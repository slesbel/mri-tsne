fMRIVis.vis package
===================

Submodules
----------

fMRIVis.vis.arcball module
--------------------------

.. automodule:: fMRIVis.vis.arcball
    :members:
    :undoc-members:
    :show-inheritance:

fMRIVis.vis.glutils module
--------------------------

.. automodule:: fMRIVis.vis.glutils
    :members:
    :undoc-members:
    :show-inheritance:

fMRIVis.vis.shader module
-------------------------

.. automodule:: fMRIVis.vis.shader
    :members:
    :undoc-members:
    :show-inheritance:

fMRIVis.vis.surface\_viewer module
----------------------------------

.. automodule:: fMRIVis.vis.surface_viewer
    :members:
    :undoc-members:
    :show-inheritance:

fMRIVis.vis.tsne module
-----------------------

.. automodule:: fMRIVis.vis.tsne
    :members:
    :undoc-members:
    :show-inheritance:

fMRIVis.vis.vector module
-------------------------

.. automodule:: fMRIVis.vis.vector
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: fMRIVis.vis
    :members:
    :undoc-members:
    :show-inheritance:
