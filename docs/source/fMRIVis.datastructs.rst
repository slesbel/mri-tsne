fMRIVis.datastructs package
===========================

Submodules
----------

fMRIVis.datastructs.mri\_slices module
--------------------------------------

.. automodule:: fMRIVis.datastructs.mri_slices
    :members:
    :undoc-members:
    :show-inheritance:

fMRIVis.datastructs.mtc module
------------------------------

.. automodule:: fMRIVis.datastructs.mtc
    :members:
    :undoc-members:
    :show-inheritance:

fMRIVis.datastructs.poi module
------------------------------

.. automodule:: fMRIVis.datastructs.poi
    :members:
    :undoc-members:
    :show-inheritance:

fMRIVis.datastructs.protocol module
-----------------------------------

.. automodule:: fMRIVis.datastructs.protocol
    :members:
    :undoc-members:
    :show-inheritance:

fMRIVis.datastructs.smp module
------------------------------

.. automodule:: fMRIVis.datastructs.smp
    :members:
    :undoc-members:
    :show-inheritance:

fMRIVis.datastructs.srf module
------------------------------

.. automodule:: fMRIVis.datastructs.srf
    :members:
    :undoc-members:
    :show-inheritance:

fMRIVis.datastructs.ssm module
------------------------------

.. automodule:: fMRIVis.datastructs.ssm
    :members:
    :undoc-members:
    :show-inheritance:

fMRIVis.datastructs.time\_course module
---------------------------------------

.. automodule:: fMRIVis.datastructs.time_course
    :members:
    :undoc-members:
    :show-inheritance:

fMRIVis.datastructs.vtc module
------------------------------

.. automodule:: fMRIVis.datastructs.vtc
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: fMRIVis.datastructs
    :members:
    :undoc-members:
    :show-inheritance:
