fMRIVis.plots package
=====================

Submodules
----------

fMRIVis.plots.fit\_analysis\_plot module
----------------------------------------

.. automodule:: fMRIVis.plots.fit_analysis_plot
    :members:
    :undoc-members:
    :show-inheritance:

fMRIVis.plots.fit\_plot module
------------------------------

.. automodule:: fMRIVis.plots.fit_plot
    :members:
    :undoc-members:
    :show-inheritance:

fMRIVis.plots.hog\_plot module
------------------------------

.. automodule:: fMRIVis.plots.hog_plot
    :members:
    :undoc-members:
    :show-inheritance:

fMRIVis.plots.patch\_contours module
------------------------------------

.. automodule:: fMRIVis.plots.patch_contours
    :members:
    :undoc-members:
    :show-inheritance:

fMRIVis.plots.protocol\_plot module
-----------------------------------

.. automodule:: fMRIVis.plots.protocol_plot
    :members:
    :undoc-members:
    :show-inheritance:

fMRIVis.plots.tsne\_plot module
-------------------------------

.. automodule:: fMRIVis.plots.tsne_plot
    :members:
    :undoc-members:
    :show-inheritance:

fMRIVis.plots.volume\_plot module
---------------------------------

.. automodule:: fMRIVis.plots.volume_plot
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: fMRIVis.plots
    :members:
    :undoc-members:
    :show-inheritance:
