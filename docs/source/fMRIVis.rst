fMRIVis package
===============

Subpackages
-----------

.. toctree::

    fMRIVis.datastructs
    fMRIVis.filters
    fMRIVis.models
    fMRIVis.plots
    fMRIVis.vis

Module contents
---------------

.. automodule:: fMRIVis
    :members:
    :undoc-members:
    :show-inheritance:
