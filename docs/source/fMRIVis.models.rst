fMRIVis.models package
======================

Submodules
----------

fMRIVis.models.glm module
-------------------------

.. automodule:: fMRIVis.models.glm
    :members:
    :undoc-members:
    :show-inheritance:

fMRIVis.models.hrf module
-------------------------

.. automodule:: fMRIVis.models.hrf
    :members:
    :undoc-members:
    :show-inheritance:

fMRIVis.models.transformations module
-------------------------------------

.. automodule:: fMRIVis.models.transformations
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: fMRIVis.models
    :members:
    :undoc-members:
    :show-inheritance:
