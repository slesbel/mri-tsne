#
# glm_subcondition.py
#
# perform GLM on fMRI data by searching subintervals inside conditions
#
# Created by Ricardo Marroquim on 23-04-2018
# Copyright (c) 2017 Federal University of Rio de Janeiro. All rights reserved
#

import sys
import numpy as np

sys.path.append('../')
from config import MriConfig

fmri_config = MriConfig('../mri.cfg')

sys.path.append(fmri_config.fmri_va_path())
from fMRIVis.datastructs import protocol, mtc, vtc, contrast, sdm
from fMRIVis.models import hrf, glm
from fMRIVis.filters import ccl_filter


def run_glm(subject, taste):
    """
        Run GLM considering split protocol (subconditions)
    """
    fmri_config.subject = subject
    vtc_glm = glm.GLM()
    vtc_glm.set_out_dir(fmri_config.subject_data_path(subject=subject))
    vtc_glm.set_out_filename(subject + '_' + taste[0:3] + '_zmap_')

    vtc_glm.load_vtc(fmri_config.vtc_file(taste=taste))

    protocol_subject = protocol.Protocol(fmri_config.protocol_sub_file(taste=taste))

    protocol_subject.delete_intervals_containing('h2o')
    protocol_subject.delete_intervals_containing('Picture')

    protocol_subject.export_prt(fmri_config.protocol_sub_file(taste=taste))

    vtc_glm.load_protocol(fmri_config.protocol_sub_file(taste=taste))

    sdm_subject = sdm.SDM(fmri_config.sdm_file(taste=taste))
    sdm_subject.print_header()
    vtc_glm.create_design_matrix(sdm_subject)

    contrasts = contrast.ContrastList(fmri_config.contrasts_file(taste=taste))
    contrasts.print_contrasts()

    vtc_glm.specify_contrasts(contrasts)

    vtc_glm.glm_analysis()


def main():
    fmri_config.parse_arguments(sys.argv)
    print('Subject : ', fmri_config.subject)
    print('Taste : ', fmri_config.taste)

    run_glm(fmri_config.subject, fmri_config.taste)
    return 0


if __name__ == "__main__":
    main()
