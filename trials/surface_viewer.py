#
# surface_viewer.py
#
# Surface viewer 
#
# Created by Ricardo Marroquim on 14-02-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import gi
gi.require_version('Gtk', '3.0')

from gi.repository import Gtk

import sys
from matplotlib.backends.backend_gtk3cairo import FigureCanvasGTK3Cairo as FigureCanvas
import matplotlib as mpl
from matplotlib.figure import Figure
from matplotlib.widgets import LassoSelector
from matplotlib.widgets import Cursor
from matplotlib import colors as mcolors
from matplotlib.path import Path
from nipy.labs.viz import cm
import numpy as np
from scipy import stats

sys.path.append('../')
from config import MriConfig
fmri_config = MriConfig('../mri.cfg')
sys.path.append(fmri_config.fmri_va_path())

from fMRIVis.vis import tsne
from fMRIVis.plots import tsne_plot
from fMRIVis.datastructs import time_course as tc
from fMRIVis.datastructs import mtc, poi, smp, protocol, srf
from fMRIVis.vis import surface_viewer
from windows import tsne_window, surface_viewer_window

def preprocess_patch(patch, protocol):
    """
        Remove 3 values from the start and end of each condition
    """
    # create a list with tuples (start, end, condition name)
    sort = []
    for c in protocol.conditions:
        for i in c.intervals:
            sort.append([i[0], i[1], c.name])

    # sort list by start time
    sort = sorted(sort, key=lambda x: x[0]) 

    reduced_patch = np.zeros(shape=patch.shape)
    zscore = np.zeros(shape=patch.shape)

    for i, p in enumerate(patch):
        zscore[i] = stats.zscore(p)

    for interval, s in enumerate(sort):
        for i, p in enumerate(zscore):
            if s[1]-s[0] == 13:
                reduced_patch[i][s[0]+3:s[1]-2] = np.mean(p[s[0]+3:s[1]-2])

    return reduced_patch

if len(sys.argv) > 1:
    subject = sys.argv[1]
    fmri_config.subject = subject
else:
    subject = fmri_config.subject

print ('Subject : ', subject)


# load data

poi = poi.POI(fmri_config.poi_file())

#srf = srf.SRF (fmri_config.srf_SPH_file())
srf = srf.SRF (fmri_config.data_path + 'All20Subjects_MeanRHmesh_Inf02.srf')

smp_sweet = smp.SMP(fmri_config.smp_file(taste='sweet'))
smp_bitter = smp.SMP(fmri_config.smp_file(taste='bitter'))

mtc_sweet = mtc.MTC (fmri_config.mtc_file(taste='sweet'))
mtc_bitter = mtc.MTC (fmri_config.mtc_file(taste='bitter'))

protocol_sweet = protocol.Protocol(fmri_config.protocol_file(taste='sweet'))
protocol_bitter = protocol.Protocol(fmri_config.protocol_file(taste='bitter'))

# create MTC with verts inside POI
mtc_patch = mtc_sweet.patch(poi)
# keep only center values for each condition
mtc_patch.data = preprocess_patch(mtc_patch.data, protocol_sweet)
smp_patch = smp_sweet.patch(poi)

# t-SNE class, plot, and window
tsne = tsne.TSNE()
tsne_win = tsne_window.TsneWindow(tsne)
tsne.set_ndpoints(mtc_patch.data)
tsne_win.plot.set_labels(smp_patch.maps[0].data)
tsne.method = 'barnes_hut'
tsne.angle = 0.5
tsne.verbose = 2

# time course plot (one for sweet other for bitter)
time_course_sweet_plt = tc.TimeCoursePlot()
time_course = tc.TimeCourse()
time_course_sweet_plt.set_protocol (protocol_sweet)

time_course_bitter_plt = tc.TimeCoursePlot()
time_course = tc.TimeCourse()
time_course_bitter_plt.set_protocol (protocol_bitter)

# reduce thickness (matplotlib seems thicker than brainvoyager)
protocol_sweet.time_course_thick = 1
protocol_bitter.time_course_thick = 1

#setup protocol window
protocol_win = Gtk.Window()
protocol_win.connect("destroy", lambda x: Gtk.main_quit())
protocol_win.set_default_size(1200, 600)
protocol_win.set_title("Protocol")
protocol_figure = Figure(figsize=(12, 12), dpi=100)
protocol_canvas = FigureCanvas(protocol_figure)
protocol_win.add(protocol_canvas)

# window to render 3D surface
srf_window = surface_viewer_window.SurfaceViewerWindow(srf=srf)

print ('matplotlib backend : ', mpl.get_backend())

# callback for key press event
def on_key_event(event):
    if event.key == 'escape':
        sys.exit(0)
    protocol_canvas.draw()
    tsne_win.canvas.draw()

def on_plot_pick (event):
    """
        Pick data point on tsne plot
    """
    artist = event.artist
    # check if the event is associated with the selection layer of the scatter plot (otherwise this callback might get called twice)
    if (artist != tsne_win.plot.selection_2d_scatter):
        return
    for i in event.ind:
        # change mask value and selected color
        if (tsne_win.plot.selection[i,3] == 1.0): #unselect
            tsne_win.plot.selection[i,3] = 0.0
            srf_window.surface_viewer.vert_selection[poi.verts[i]] = 0.0
        else: #select
            tsne_win.plot.selection[i,3] = 1.0
            time_course_sweet.load_time_course (mtc_patch.data[i])
            time_course_plt.plot(time_course)
            srf_window.surface_viewer.vert_selection[poi.verts[i]] = 1.0

    artist.set_facecolors(tsne_win.plot.selection)

    srf_window.surface_viewer.update_selection()

    canvas.draw()

def on_plot_select_lasso (verts):
    """
        Lasso selctor for scatter plot
        Check which points are inside lasso polygon and mark them as selected
    """
    path = Path(verts)
    ind = np.nonzero([path.contains_point(xy) for xy in tsne.points_2d])[0]
    for i in ind:
        tsne_win.plot.selection[i,3] = 1.0
        srf_window.surface_viewer.vert_selection[poi.verts[i]] = 1.0

    tsne_win.plot.selection_2d_scatter.set_facecolors(tsne_win.plot.selection)
    srf_window.surface_viewer.update_selection()
    canvas.draw()


def init_protocol_subplots (fig):
    """
        Create two protocol plots, one for sweet and the other for bitter
    """

    time_course_sweet_plt.init_axes(fig.add_axes([0.15, 0.1, 0.7, 0.35]),
                                    fig.add_axes([0.9, 0.1, 0.02, 0.35]),
                                    fig.add_axes([0.02, 0.1, 0.08, 0.15]))

    time_course_bitter_plt.init_axes(fig.add_axes([0.15, 0.6, 0.7, 0.35]),
                                     fig.add_axes([0.9, 0.6, 0.02, 0.35]),
                                     fig.add_axes([0.02, 0.6, 0.08, 0.15]))


# register callbacks
protocol_canvas.mpl_connect('key_press_event', on_key_event)
tsne_win.canvas.mpl_connect('pick_event', on_plot_pick)

#cursor = Cursor(scatter_fig, horizOn=True, vertOn=True)

def run_tsne(event):
    tsne_win.plot.run_tsne(event)
    srf_window.load_tsne(tsne_win.plot,poi)
    
def clear_selection(event):
    tsne_win.plot.clear_selection()
    srf_window.surface_viewer.clear_selection()
    tsne_win.plot.update_plot()
    canvas.draw()

# initialize axes (plots)
init_protocol_subplots(protocol_figure)

tsne_win.init_subplots(run_tsne, clear_selection)

srf_window.load_tsne (tsne_win.plot, poi)
srf_window.load_smp (smp_sweet)
srf_window.load_mtc (mtc_sweet)

lasso = LassoSelector(tsne_win.plot.plot_ax, onselect=on_plot_select_lasso, useblit=False)

def main():
    Gtk.main()
    return 0

if __name__ == "__main__":
    protocol_win.show_all()
    tsne_win.window.show_all()
    protocol_win.iconify()
    main()
