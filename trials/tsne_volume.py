#
# tsne_volume.py
#
# TSNE for inspect volumes from VTC data
#
# Created by Ricardo Marroquim on 26-04-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import gi
gi.require_version('Gtk', '3.0')

from gi.repository import Gtk

import sys
from matplotlib.backends.backend_gtk3cairo import FigureCanvasGTK3Cairo as FigureCanvas
import matplotlib as mpl
from matplotlib.figure import Figure
from matplotlib.widgets import LassoSelector
from matplotlib.widgets import Cursor
from matplotlib.widgets import RadioButtons, Slider, Button
from matplotlib import colors as mcolors
from matplotlib.path import Path
from scipy import stats
import numpy as np
import math

sys.path.append('../')
sys.path.append('../../')
from config import MriConfig
fmri_config = MriConfig('../mri.cfg')
sys.path.append(fmri_config.fmri_va_path())
from fMRIVis.vis import tsne
from fMRIVis.plots import volume_time_course_plot, protocol_plot, tsne_plot
from fMRIVis.datastructs import vtc, protocol, time_course
from fMRIVis.gtk_windows import tsne_window, protocol_window, volume_time_course_window

def average_conditions(patch, protocol):
    """
    """
    
    print ('original Patch size', patch.shape)
    reduced_patch = []
    annotations = []

    for b in protocol.blocks:
        if b.condition.name == 'Fixation':
            mean = stats.zscore(patch[b.start,:])
        else:
            print ('CONDITION NAME', b.condition.name)
            # reduce range to volumes near the center of the interval by adjusting offsets below
            # in this configuration selects only 7 central scans
            # original is +3 / -4
            start = b.start
            end = b.end
            
#           mean = np.mean ( stats.zscore( patch[start:end+1,:] ), 0) 
            mean = stats.zscore( np.mean(patch[start:end+1,:], 0 ) ) 

            #if (condition in b.condition.name):
            reduced_patch.append(mean)
            annotations.append(b.condition.name)
                
    reduced_patch = np.asarray(reduced_patch)
    
    print ('reduced roi size', reduced_patch.shape)

    if (False):
        for vol in range(reduced_patch.shape[0]):
            reduced_patch[vol:] = stats.zscore ( reduced_patch[vol:] )
            print (vol, reduced_patch[vol:], '\n')
        reduced_patch = np.nan_to_num (reduced_patch)

    return reduced_patch, annotations

def compute_labels (tsne_plt, protocol):
    """
        Compute tsne labels
        Compute corresponding colormap for tsne
    """
    labels, colors = protocol.block_labels_from_conditions()


    # remove Fixation if not in use
    labels = [l for l in labels if l!=0]


    # create lists with conditions colors (only one per condition, not per interval (block))
    labels_cmap = mpl.colors.ListedColormap(colors)
    tsne_plt.colormap = labels_cmap
    tsne_plt.set_labels(labels)
    tsne_plt.vmin = 0
    tsne_plt.vmax = 4
    tsne_plt.colors = colors

    print ('LABELS ', labels)
    print ('LABELS_CMAP ', labels_cmap)
    print ('COLORS ', colors)


# load data
mask_region = []

condition = 'both'
if len(sys.argv) > 1:
    condition = sys.argv[1]


protocol_filename = fmri_config.protocol_file()
vtc_filename = fmri_config.vtc_file()
vmr_filename = fmri_config.vmr_file()
zmap_filename = fmri_config.zmap_file()
zmap_highres_filename = fmri_config.zmap_mask_highres(mapname='')
mask_filename = fmri_config.mask_file()
mask_highres_filename = fmri_config.mask_highres(mapname='')

print ('loading data ...')
mask = np.load( mask_filename )

# if region selected, zero other regions
if len(mask_region) > 0:
    mask [np.logical_not(np.isin(mask, mask_region))] = 0

vtc_subject = vtc.VTC ( vtc_filename )

protocol_subject = protocol.Protocol( protocol_filename )

print ('computing patch and labels ...')
# create VTC patch with verts inside mask
vtc_patch = vtc_subject.patch_mask(mask)
# transpose, since we want vols x vert
vtc_patch = vtc_patch.transpose()
vtc_patch, annotations = average_conditions(vtc_patch, protocol_subject)

# t-SNE window
tsne = tsne.TSNE()
tsne.verbose = 2
tsne_win = tsne_window.TsneWindow(tsne, title='TSNE VOLUME')
tsne.set_ndpoints(vtc_patch)
compute_labels(tsne_win.plot, protocol_subject)
tsne_win.plot.annotations = annotations
tsne_win.plot.show_annotations = True

# protocol window
protocol_win = protocol_window.ProtocolWindow()
protocol_win.plot.set_protocol (protocol_subject)
protocol_win.plot.set_num_scans (vtc_subject.data.shape[3])

# volume time course window
volume_tc_win = volume_time_course_window.VolumeTimeCourseWindow()
volume_tc_win.plot.set_protocol (protocol_subject)


# callback for key press event
def on_key_event(event):
    """
        Keyboard callback
    """
    if event.key == 'escape':
        sys.exit(0)
    canvas.draw()


def get_block_id (point_id):
    """
        Get the block id given a plot point        
    """
    # count valid intervals (skip unflagged ones, ex. Picture, h2o ...)
    block_id = -1
    for cnt, b in enumerate(protocol_subject.blocks):
        if not b.condition.name == 'Fixation':
            block_id += 1
            if block_id == point_id:
                block_id = cnt
                break

    return block_id


def on_plot_pick (event):
    """
        Pick data point on tsne plot
    """
    artist = event.artist
    # check if the event is associated with the selection layer of the scatter plot (otherwise this callback might get called twice)
    if (artist != tsne_win.plot.selection_2d_scatter):
        return

    # for every selected point change the toggle mask
    # i is the id of the selected point on plot [0, num points]
    # block id: each event is one block, all scans inside a block counts as one
    # time is the actual scan time (the center scan time of the block)
    for i in event.ind:

        block_id = get_block_id(i)
        block = protocol_subject.blocks[block_id]
        time = int(block.start + (block.end - block.start)*0.5)

        # if selected, mark as unselected and vice-versa
        if (tsne_win.plot.selection[i,3] == 1.0):
            tsne_win.plot.selection[i,3] = 0.0
            protocol_win.plot.selection[time] = 0
        else:
            tsne_win.plot.selection[i,3] = 1.0
            protocol_win.plot.selection[time] = 1
            vol_data = volume_time_course_plot.VolumeData(vtc_patch[i], time)
            volume_tc_win.plot.plot(vol_data)

    artist.set_facecolors(tsne_win.plot.selection)
    protocol_win.plot.plot()
    tsne_win.plot.plot()

def on_plot_select_lasso (verts):
    """
        Lasso selctor for scatter plot
        Check which points are inside lasso polygon and mark them as selected
    """
    path = Path(verts)
    ind = np.nonzero([path.contains_point(xy) for xy in tsne.points_2d])[0]
    # for every point in lasso
    for i in ind:
        block_id = get_block_id(i)       
        block = protocol_subject.blocks[block_id]
        time = int(block.start + (block.end - block.start)*0.5)
        tsne_win.plot.selection[i,3] = 1.0
        protocol_win.plot.selection[time] = 1

    tsne_win.plot.selection_2d_scatter.set_facecolors(tsne_win.plot.selection)
    protocol_win.plot.plot()
    tsne_win.plot.plot()

def clear_selection(event):
    """
        Clears all selections
    """
    tsne_win.plot.clear_selection()
    protocol_win.plot.clear_selection()
    tsne_win.plot.plot()
    protocol_win.plot.plot()


#tsne_canvas.mpl_connect('key_press_event', on_key_event)
tsne_win.canvas.mpl_connect('pick_event', on_plot_pick)

# initialize axes (plots)
tsne_win.init_subplots(clear_selection_cb=clear_selection)
protocol_win.init_subplots()
volume_tc_win.init_subplots()

# initialize plots
tsne_win.plot.plot()
protocol_win.plot.plot()
volume_tc_win.plot.plot()

# Lasso selector for TSNE plot
lasso = LassoSelector(tsne_win.plot.plot_ax, onselect=on_plot_select_lasso, useblit=True)

def main():
    Gtk.main()
    return 0

if __name__ == "__main__":
    protocol_win.window.show_all()
    volume_tc_win.window.show_all()
    tsne_win.window.show_all()
    main()

