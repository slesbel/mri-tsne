#
# volume_slice_viewer.py
#
# Inspect Volume Slices Trial
# Uses BrainVoyager tutorial data
#
# Created by Ricardo Marroquim on 24-04-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

import sys
import numpy as np

sys.path.append('./')
sys.path.append( '../')
from config import MriConfig
fmri_config = MriConfig('../mri.cfg')
sys.path.append(fmri_config.fmri_va_path())
from fMRIVis.datastructs import vtc, vmr
from fMRIVis.gtk_windows import volume_slices_window


def slice_viewer (condition='faces', resolution='low', region=[]):
    print(fmri_config.vtc_file())

    vtc_filename = fmri_config.vtc_file()
    vmr_filename = fmri_config.vmr_file()
    zmap_filename = fmri_config.zmap_file()
    zmap_highres_filename = fmri_config.zmap_mask_highres(mapname='')
    mask_filename = fmri_config.mask_file()
    mask_highres_filename = fmri_config.mask_highres(mapname='')
 

    vtc_subject = vtc.VTC ( vtc_filename )
    vmr_subject = vmr.VMR ( vmr_filename )

    if resolution == 'low':
        zmap = np.load( zmap_filename )
        mask = np.load( mask_filename )
        return volume_slices_window.VolumeSlicesWindow(vtc_subject.data[:,:,:,0], zmap, mask, region=region)

    if resolution == 'high':
        zmap_highres = np.load( zmap_highres_filename )
        mask_highres = np.load( mask_highres_filename )
        return volume_slices_window.VolumeSlicesWindow(vmr_subject.data, zmap_highres, mask_highres, region=region)

def main():
    condition = 'faces'
    if len(sys.argv) > 1:
        condition = sys.argv[1]

    vol_slice_window = slice_viewer (condition, resolution='high')

    Gtk.main()
    return 0

if __name__ == "__main__":
    main()

