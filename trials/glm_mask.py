#
# glm_mask.py
#
# creates a mask for the glm result
#
# Created by Ricardo Marroquim on 23-04-2018
# Copyright (c) 2017 Federal University of Rio de Janeiro. All rights reserved
#

import sys
import numpy as np

sys.path.append('../')
from config import MriConfig
fmri_config = MriConfig('../mri.cfg')

sys.path.append(fmri_config.fmri_va_path())
from fMRIVis.datastructs import protocol, vtc, vmr
from fMRIVis.models import hrf, glm
from fMRIVis.filters import ccl_filter, data_transform


def compute_mask (subject, taste, condition, zmap_threshold = 2.0, min_vol = 50):
    """
        Compute mask from zmap data
    """

    fmri_config.subject = subject

    # load resulting zmap
    zmap = np.load( fmri_config.zmap_file(taste=taste, contrast=condition) )

    # create a binary mask given a threshold
    mask = np.zeros(shape=zmap.shape)
    mask[zmap >= zmap_threshold] = 1.0

    # spatial mask to remove border activation
    #mask[0:20,:,:] = 0.0
    #mask[55:,:,:] = 0.0
#    mask[:,:20,:] = 0.0

    # filter components
    filtered_mask = ccl_filter.filter_components(mask, min_vol=min_vol)

    mask_filename = fmri_config.zmap_mask_file(taste=taste, contrast=condition)
    np.save(mask_filename, filtered_mask)

    # create highres data

    vtc_subject = vtc.VTC ( fmri_config.vtc_file (taste=taste) )
    vmr_subject = vmr.VMR ( fmri_config.vmr_file (taste=taste) )

    highres_dim = [vmr_subject.dim_x, vmr_subject.dim_y, vmr_subject.dim_z]


    print ('creating high resolution mask...')

    highres_mask = data_transform.to_highres (filtered_mask, vtc_subject, highres_dim, interpolation='nearest')
    np.save(fmri_config.zmap_mask_file(taste=taste, contrast=condition, highres=True), highres_mask)
    print ('lowres mask : ', len(filtered_mask [filtered_mask != 0]))
    print ('highres mask : ', len(highres_mask [highres_mask != 0]))


    print ('creating high resolution zmap...')
    highres_zmap = data_transform.to_highres (zmap, vtc_subject, highres_dim, interpolation='trilinear')
    np.save(fmri_config.zmap_file(taste=taste, contrast=condition, highres=True), highres_mask)


def main():

    fmri_config.parse_arguments(sys.argv)
    print ('Subject : ', fmri_config.subject)
    print ('Taste : ', fmri_config.taste)
    print ('Condition : ', fmri_config.condition)
    print ('Zmap Threshold : ', fmri_config.zmap_threshold)
    print ('Min vol : ', fmri_config.min_cluster_vol)

    compute_mask (fmri_config.subject, fmri_config.taste, fmri_config.condition, fmri_config.zmap_threshold, fmri_config.min_cluster_vol)
    return 0

if __name__ == "__main__":
    main()

