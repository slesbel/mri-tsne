#
# hog_window.py
#
# Gtk window containg HoG plot
#
# Created by Ricardo Marroquim on 26-03-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import gi
gi.require_version('Gtk', '3.0')

import sys
import os
from matplotlib.backends.backend_gtk3cairo import FigureCanvasGTK3Cairo as FigureCanvas
import matplotlib as mpl
from matplotlib.figure import Figure
from matplotlib.widgets import RadioButtons, Slider, Button
import numpy as np
import math
from gi.repository import Gtk

sys.path.append('../')
from config import MriConfig
fmri_config = MriConfig('../mri.cfg')
sys.path.append(fmri_config.fmri_va_path())

from fMRIVis.plots import hog_plot

class HoGWindow:
    """
        HoG Viewer GTK window
    """
    def on_key_event(self, event):
        """
            Callback for key press event inside canvas
        """
        if event.key == 'escape':
            sys.exit(0)

        self.hog_plot.plot()
        self.canvas.draw()


    def __init__(self, size = [800, 800], name=''):
        """
            Initializes HoG window class
        """

        self.size = size

        # create GTK window
        self.window = Gtk.Window()
        self.window.resize(self.size[0], self.size[1])
        self.window.set_title("HoG Viewer " + str(name))
        self.window.connect('delete_event', Gtk.main_quit)
        self.window.connect('destroy', lambda quit: Gtk.main_quit())

        self.figure = Figure(figsize=(12, 12), dpi=100)
        self.canvas = FigureCanvas(self.figure)
        self.window.add(self.canvas)

        self.canvas.mpl_connect("key_press_event", self.on_key_event)

        self.hog_plot = hog_plot.HoGPlot(size=self.size, name=name)


    def init_subplots (self):
        """
            Create axes for patch contour plot (contour, colorbar, buttons ...)
        """
        #self.contour_plot.init_contour_plot(self.figure.add_axes([0.08, 0.05, 0.8, 0.8], projection='3d')) 
        hog_ax = self.figure.add_axes([0.05, 0.05, 0.8, 0.8], polar=True)
        self.hog_plot.init_hog_plot(hog_ax)


    def compute_hog (self, smap):
        """
        """
        return True
        
