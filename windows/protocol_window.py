#
# protocol_window.py
#
# Gtk window containg protocol plot
#
# Created by Ricardo Marroquim on 03-05-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

import sys
import os
from matplotlib.backends.backend_gtk3cairo import FigureCanvasGTK3Cairo as FigureCanvas
import matplotlib as mpl
from matplotlib.figure import Figure
from matplotlib.widgets import RadioButtons, Slider, Button
import numpy as np
import math

sys.path.append('../')
from config import MriConfig
fmri_config = MriConfig('../mri.cfg')
sys.path.append(fmri_config.fmri_va_path())

from fMRIVis.plots import protocol_plot

class ProtocolWindow:
    """
        Protocol GTK window
    """
    def on_key_event(self, event):
        """
            Callback for key press event inside canvas
        """
        if event.key == 'escape':
            sys.exit(0)

        self.plot.plot()
        self.canvas.draw()


    def __init__(self, size = [1000, 300], name=''):
        """
            Initializes Protocol window class
        """

        self.size = size

        # create GTK window
        self.window = Gtk.Window()
        self.window.resize(self.size[0], self.size[1])
        self.window.set_title("Protocol Viewer " + str(name))
        self.window.connect('delete_event', Gtk.main_quit)
        self.window.connect('destroy', lambda quit: Gtk.main_quit())

        self.figure = Figure(figsize=(12, 12), dpi=100)
        self.canvas = FigureCanvas(self.figure)
        self.window.add(self.canvas)

        self.canvas.mpl_connect("key_press_event", self.on_key_event)

        self.plot = protocol_plot.ProtocolPlot()


    def init_subplots (self):
        """
            Create axes for protocol plot and colorbar
        """
        self.plot.init_scatter_plot (self.figure.add_axes([0.03, 0.15, 0.7, 0.75]))
        self.plot.init_colorbar (self.figure.add_axes([0.8, 0.05, 0.02, 0.9]))

       
