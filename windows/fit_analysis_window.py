#
# fit_analysis_window.py
#
# Gtk window for fit analysis plots
#
# Created by Ricardo Marroquim on 14-03-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import gi
gi.require_version('Gtk', '3.0')

import sys
import os
from matplotlib.backends.backend_gtk3cairo import FigureCanvasGTK3Cairo as FigureCanvas
import matplotlib as mpl
from matplotlib.figure import Figure
from matplotlib.widgets import RadioButtons, Slider, Button
import numpy as np
import math
from gi.repository import Gtk

sys.path.append('../')
from config import MriConfig
fmri_config = MriConfig('../mri.cfg')
sys.path.append(fmri_config.fmri_va_path())

from fMRIVis.datastructs import srf, smp, mtc, poi
from fMRIVis.plots import fit_analysis_plot, fit_plot

class FitAnalysisWindow:
    """
        Patch Viewer GTK window
    """
    def on_key_event(self, event):
        """
            Callback for key press event inside canvas
        """
        if event.key == 'escape':
            sys.exit(0)
        elif event.key == '+':
            self.fit_analysis_plot.number_contours += 1
            print ('num contours:', self.fit_analysis_plot.number_contours)
        elif event.key == '-':
            self.fit_analysis_plot.number_contours -= 1
            print ('num contours:', self.fit_analysis_plot.number_contours)


        self.fit_analysis_plot.plot()
        self.canvas.draw()

    def __init__(self, srf, smp, size = [1500, 800], name='sweet'):
        """
            Initializes Fit Analysis window class
        """

        self.size = size

        # create GTK window
        self.window = Gtk.Window()
        self.window.resize(self.size[0], self.size[1])
        self.window.set_title("Fit Analysis " + str(name))
        self.window.connect('delete_event', Gtk.main_quit)
        self.window.connect('destroy', lambda quit: Gtk.main_quit())

        self.figure = Figure(figsize=(100, 100), dpi=100)
        self.canvas = FigureCanvas(self.figure)
        self.window.add(self.canvas)

        self.canvas.mpl_connect("key_press_event", self.on_key_event)
#        self.canvas.mpl_connect('pick_event', self.on_plot_pick)

        self.fit_analysis_plot = fit_analysis_plot.FitAnalysisPlot(srf=srf, smp=smp, size=self.size, name=name)

        self.fit_analysis_plot.init_analysis_ax(self.figure)
       


