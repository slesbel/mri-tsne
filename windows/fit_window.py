#
# fit_window.py
#
# Gtk window containg fit viewer
#
# Created by Ricardo Marroquim on 13-03-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import gi
gi.require_version('Gtk', '3.0')

import sys
import os
from matplotlib.backends.backend_gtk3cairo import FigureCanvasGTK3Cairo as FigureCanvas
import matplotlib as mpl
from matplotlib.figure import Figure
from matplotlib.widgets import RadioButtons, Slider, Button
import numpy as np
import math
from gi.repository import Gtk

sys.path.append('../../fmri-visual-analytics/src')
from fMRIVis.plots import fit_plot

class FitWindow:
    """
        Sample data fit Viewer GTK window
    """
    def on_key_event(self, event):
        """
            Callback for key press event inside canvas
        """
        if event.key == 'escape':
            sys.exit(0)

        self.fit_plot.plot()
        self.canvas.draw()


    def __init__(self, size = [400, 800], name=''):
        """
            Initializes Fit window class
        """

        self.size = size

        # create GTK window
        self.window = Gtk.Window()
        self.window.resize(self.size[0], self.size[1])
        self.window.set_title("Fit Viewer " + str(name))
        self.window.connect('delete_event', Gtk.main_quit)
        self.window.connect('destroy', lambda quit: Gtk.main_quit())

        self.figure = Figure(figsize=(12, 12), dpi=100)
        self.canvas = FigureCanvas(self.figure)
        self.window.add(self.canvas)

        self.canvas.mpl_connect("key_press_event", self.on_key_event)

        self.fit_plot = fit_plot.FitPlot(size=self.size, name=name)


    def init_subplots (self):
        """
            Create axes for patch contour plot (contour, colorbar, buttons ...)
        """
        #self.contour_plot.init_contour_plot(self.figure.add_axes([0.08, 0.05, 0.8, 0.8], projection='3d')) 
        fit_ax = [self.figure.add_axes([0.1, 0.05, 0.8, 0.40]),
                  self.figure.add_axes([0.1, 0.50, 0.8, 0.40])]
       
        self.fit_plot.init_fit_plot(fit_ax)


