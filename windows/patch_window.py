#
# patch_window.py
#
# Gtk window containg pacth viewer and buttons
#
# Created by Ricardo Marroquim on 12-03-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import gi
gi.require_version('Gtk', '3.0')

import sys
import os
from matplotlib.backends.backend_gtk3cairo import FigureCanvasGTK3Cairo as FigureCanvas
import matplotlib as mpl
from matplotlib.figure import Figure
from matplotlib.widgets import RadioButtons, Slider, Button
import numpy as np
import math
from gi.repository import Gtk

sys.path.append('../../fmri-visual-analytics/src')
from fMRIVis.datastructs import srf, smp, mtc, poi
from fMRIVis.plots import patch_contours, fit_plot


class PatchWindow:
    """
        Patch Viewer GTK window
    """
    def on_key_event(self, event):
        """
            Callback for key press event inside canvas
        """
        if event.key == 'escape':
            sys.exit(0)
        elif event.key == '+':
            self.contour_plot.number_contours += 1
            print ('num contours:', self.contour_plot.number_contours)
        elif event.key == '-':
            self.contour_plot.number_contours -= 1
            print ('num contours:', self.contour_plot.number_contours)


        self.contour_plot.plot()
        self.canvas.draw()

    def __init__(self, srf, smp, size = [1500, 800], name='sweet'):
        """
            Initializes Patch window class
        """

        self.size = size

        # create GTK window
        self.window = Gtk.Window()
        self.window.resize(self.size[0], self.size[1])
        self.window.set_title("Patch Viewer " + str(name))
        self.window.connect('delete_event', Gtk.main_quit)
        self.window.connect('destroy', lambda quit: Gtk.main_quit())

        self.figure = Figure(figsize=(12, 12), dpi=100)
        self.canvas = FigureCanvas(self.figure)
        self.window.add(self.canvas)

        self.canvas.mpl_connect("key_press_event", self.on_key_event)
#        self.canvas.mpl_connect('pick_event', self.on_plot_pick)

        self.contour_plot = patch_contours.PatchContours(srf=srf, smp=smp, size=self.size, name=name)

       

    def init_subplots (self):
        """
            Create axes for patch contour plot (contour, colorbar, buttons ...)
        """
        #self.contour_plot.init_contour_plot(self.figure.add_axes([0.08, 0.05, 0.8, 0.8], projection='3d')) 
        conditions_ax = [self.figure.add_axes([0.02, 0.05, 0.15, 0.25]),
                         self.figure.add_axes([0.20, 0.05, 0.15, 0.25]),
                         self.figure.add_axes([0.38, 0.05, 0.15, 0.25]),
                         self.figure.add_axes([0.56, 0.05, 0.15, 0.25]),
                         self.figure.add_axes([0.74, 0.05, 0.15, 0.25])]
        
        analysis_ax = [self.figure.add_axes([0.02, 0.35, 0.15, 0.25]),
                       self.figure.add_axes([0.20, 0.35, 0.15, 0.25]),
                       self.figure.add_axes([0.38, 0.35, 0.15, 0.25]),
                       self.figure.add_axes([0.56, 0.35, 0.15, 0.25]),
                       self.figure.add_axes([0.74, 0.35, 0.15, 0.25]),
                       self.figure.add_axes([0.02, 0.70, 0.15, 0.25]),
                       self.figure.add_axes([0.20, 0.70, 0.15, 0.25]),
                       self.figure.add_axes([0.38, 0.70, 0.15, 0.25]),
                       self.figure.add_axes([0.56, 0.70, 0.15, 0.25]),
                       self.figure.add_axes([0.74, 0.70, 0.15, 0.25])]

        colorbar_ax = self.figure.add_axes([0.92, 0.05, 0.02, 0.85])

        self.contour_plot.init_contours_plot(conditions_ax)
        self.contour_plot.init_colorbar_ax(colorbar_ax)
        self.contour_plot.init_analysis_ax(analysis_ax)
#
#        self.contour_plot.init_3d_plot(self.figure.add_axes([0.08, 0.05, 0.8, 0.8], projection='3d')) 
      

