#
# volume_slices_window.py
#
# Gtk window containing volume slices viewer
#
# Created by Ricardo Marroquim on 24-04-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import gi
gi.require_version('Gtk', '3.0')

from gi.repository import Gtk

import sys
import os
from matplotlib.figure import Figure
from matplotlib.backends.backend_gtk3cairo import FigureCanvasGTK3Cairo as FigureCanvas
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import LinearSegmentedColormap
import nibabel as nib
from matplotlib.widgets import LassoSelector
from matplotlib.widgets import Cursor
from matplotlib.widgets import RadioButtons
from matplotlib.path import Path
import matplotlib as mpl

sys.path.append('../')
from config import MriConfig
fmri_config = MriConfig('../mri.cfg')
sys.path.append(fmri_config.fmri_va_path())

from fMRIVis.datastructs import vmr
from fMRIVis.plots import slice_plot

class VolumeSlicesWindow:
    """
        Volume Slices Viewer GTK window
    """

    def __init__(self, vol_data, zmap, mask, size=[800, 400], name=''):
        """
            Initializes Volume Slices Viewer class
        """

        self.size = size

        self.slice_plot = slice_plot.SlicePlot(vol_data, zmap, mask)

        # create GTK window
        self.window = Gtk.Window()
        self.window.resize(self.size[0], self.size[1])
        self.window.set_title("Slice Viewer" + str(name))
        self.window.connect('delete_event', Gtk.main_quit)
        self.window.connect('destroy', lambda quit: Gtk.main_quit())

        self.figure = Figure(figsize=(100, 100), dpi=100)
        self.canvas = FigureCanvas(self.figure)

        self.canvas.mpl_connect('key_press_event', self.on_key_event)
        self.window.connect('button-press-event', self.on_image_click)
        self.canvas.mpl_connect('motion_notify_event', self.on_motion)

        # Add the grid to the window
        self.window.add(self.canvas)
        self.window.show_all()

        self.slice_plot.init_slices_ax(self.figure)

    def on_key_event(self, event):
        """
            Handles keyboard input
        """

        if event.key == 'escape':
            sys.exit(0)
        elif event.key == 'j':
            self.slice_plot.inc_slice_num(2)
            self.slice_plot.plot('sag')
        elif event.key == 'J':
            self.slice_plot.dec_slice_num(2)
            self.slice_plot.plot('sag')
        elif event.key == 'k':
            self.slice_plot.inc_slice_num(0)
            self.slice_plot.plot('cor')
        elif event.key == 'K':
            self.slice_plot.dec_slice_num(0)
            self.slice_plot.plot('cor')
        elif event.key == 'l':
            self.slice_plot.inc_slice_num(1)
            self.slice_plot.plot('tra')
        elif event.key == 'L':
            self.slice_plot.dec_slice_num(1)
            self.slice_plot.plot('tra')
        self.canvas.draw()

    def check_image_click(self, win_coords, img_id):
        """
            Check if clicked in one the subplots containing a slice image

            :param win_coords: the window coordinates of the mouse click (with origin at bottom left)
            :param img_id: the slice id (0, 1, 2) for (left, center, right)
            :returns: the voxel value if clicked inside the image
        """
        # transformation from display coords to axes
        inv = self.figure.get_axes()[img_id].transData.inverted()
        coords = inv.transform((win_coords[0], win_coords[1]))

        # depending on the slice converts to appropriate voxel coordinates
        if img_id == 1:
            # Coronal slice, note that the image was rotate 270o
            voxel_coords = [self.slice_plot.slice_ids[0], self.slice_plot.vol_data.shape[1] - 1 - int(coords[1]),
                            self.slice_plot.vol_data.shape[2] - 1 - int(coords[0])]
        elif img_id == 2:
            # Transverse slice, note that the image is displayed transposed
            voxel_coords = [self.slice_plot.vol_data.shape[2] - 1 - int(coords[1]), self.slice_plot.slice_ids[1],
                            self.slice_plot.vol_data.shape[0] - 1 - int(coords[0])]
        elif img_id == 0:
            # Sagittal slice
            voxel_coords = [int(coords[0]), self.slice_plot.vol_data.shape[1] - 1 - int(coords[1]),
                            self.slice_plot.slice_ids[2]]

        value = self.slice_plot.voxel_value(voxel_coords)

        if value != None:
            print('clicked ', voxel_coords)
            print('mask ', self.slice_plot.mask[tuple(voxel_coords)])
            self.slice_plot.goto_slices(voxel_coords)
            self.canvas.draw()
            #        plot_voxel (voxel_coords)
            #        canvas.draw()

        return value

    def on_motion(self, event):
        """
            Callback for mouse motion
        """
        pos = [event.x, event.y]

        sag_coords = []
        cor_coords = []
        tra_coords = []
        dim = self.slice_plot.vol_data.shape[0:3]

        # sag slice
        tpos = self.slice_plot.sag_ax.transAxes.inverted().transform((pos[0], pos[1])) * [dim[0], dim[1]]
        if tpos[0] >= 0.0 and tpos[1] >= 0.0 and tpos[0] <= dim[0] and tpos[1] <= dim[1]:
            sag_coords = [tpos[0], tpos[1]]
            cor_coords = [self.slice_plot.slice_ids[2], tpos[1]]
            tra_coords = [self.slice_plot.slice_ids[2], dim[2] - tpos[0]]

        # cor slice
        else:
            tpos = self.slice_plot.cor_ax.transAxes.inverted().transform((pos[0], pos[1])) * [dim[1], dim[2]]
            if tpos[0] >= 0.0 and tpos[1] >= 0.0 and tpos[0] <= dim[1] and tpos[1] <= dim[2]:
                sag_coords = [self.slice_plot.slice_ids[0], tpos[1]]
                cor_coords = [tpos[0], tpos[1]]
                tra_coords = [tpos[0], dim[0] - self.slice_plot.slice_ids[0]]

            # tra slice
            else:
                tpos = self.slice_plot.tra_ax.transAxes.inverted().transform((pos[0], pos[1])) * [dim[0], dim[2]]
                if tpos[0] >= 0.0 and tpos[1] >= 0.0 and tpos[0] <= dim[1] and tpos[1] <= dim[2]:
                    sag_coords = [dim[1] - tpos[1], dim[1] - self.slice_plot.slice_ids[1]]
                    cor_coords = [tpos[0], dim[1] - self.slice_plot.slice_ids[1]]
                    tra_coords = [tpos[0], tpos[1]]

        if len(sag_coords) > 0:
            self.slice_plot.draw_cursor(sag_coords, cor_coords, tra_coords)
            self.canvas.draw()

    def on_image_click(self, win, event):
        """
            Callback for mouse click, check if clicked on one of the subplots
        """

        # window coords with origin at bottom left
        win_coords = [event.x, win.get_size()[1] - event.y - 1]

        print('win coords ', win_coords)
        # check if clicked in one of the subplots containing slice images
        self.check_image_click(win_coords, 0)
        self.check_image_click(win_coords, 1)
        self.check_image_click(win_coords, 2)

        
