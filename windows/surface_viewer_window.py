#
# surface_viewer_window.py
#
# Gtk window containg surface viewer and buttons
#
# Created by Ricardo Marroquim on 07-03-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import gi
gi.require_version('Gtk', '3.0')

import sys
import os
from matplotlib.backends.backend_gtk3cairo import FigureCanvasGTK3Cairo as FigureCanvas
import matplotlib as mpl
from matplotlib.figure import Figure
from matplotlib.widgets import RadioButtons, Slider, Button
import numpy as np
import math
from mpmath import *
from scipy import stats
from sklearn.preprocessing import MinMaxScaler

from threading import Timer

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL import GLX
from OpenGL import GL
from OpenGL.arrays import vbo

from gi.repository import Gtk, GdkPixbuf, Gdk

sys.path.append('../')
from config import MriConfig
fmri_config = MriConfig('../mri.cfg')
sys.path.append(fmri_config.fmri_va_path())

from fMRIVis.datastructs import srf, smp, mtc, poi
from fMRIVis.vis import surface_viewer, arcball, glutils


class SurfaceViewerWindow:
    """
        OpenGL based Surface Viewer GTK window
    """

    def __init__(self, srf, size = [800, 800], name= ''):
        """
            Initializes Surface Viewer class
        """
        self.render_type = 'mtc'
        self.animate_mtc = False

        self.enable_render = False
        self.size = size

        # create GTK window
        self.window = Gtk.Window()
        self.window.realize()
        self.window.resize(self.size[0], self.size[1])
        self.window.set_resizable(True)
        self.window.set_reallocate_redraws(True)
        self.window.set_title("SRF - Surface Viewer - " + name)
        self.window.add_events( Gdk.EventMask.SCROLL_MASK )
        self.window.connect('delete_event', Gtk.main_quit)
        self.window.connect('destroy', lambda quit: Gtk.main_quit())
        self.window.connect("key_press_event", self.on_key_pressed)

        self.surface_viewer = surface_viewer.SurfaceViewer(srf)

        self.window.connect('button-press-event', self.surface_viewer.on_button_pressed)
        self.window.connect('button-release-event', self.surface_viewer.on_button_released)
        self.window.connect('motion_notify_event', self.surface_viewer.motion_notify_event)
        self.window.connect('scroll-event', self.surface_viewer.mouse_scroll)

        self.grid = Gtk.Grid()
        self.grid.set_border_width(10)
        self.grid.set_row_spacing(5)
        self.grid.set_column_spacing(5)

        # Buttons
        self.animation_button = Gtk.ToggleButton(label="Animation On/Off")
        self.animation_button.connect("clicked", self.toggle_animation)

        self.render_type_button = Gtk.Button(label=str(self.render_type))
        self.render_type_button.connect("clicked", self.cycle_render_type)

        self.cycle_smp_button = Gtk.Button(label=('SMP 1'))
        self.cycle_smp_button.connect("clicked", self.cycle_smps)

        self.cycle_generic_button = Gtk.Button(label=('Generic 1'))
        self.cycle_generic_button.connect("clicked", self.cycle_generics)


        self.grid.attach(self.animation_button, 0, 0, 1, 1)
        self.grid.attach(self.render_type_button, 1, 0, 1, 1)
        self.grid.attach(self.cycle_smp_button, 2, 0, 1, 1)
        self.grid.attach(self.cycle_generic_button, 3, 0, 1, 1)
        self.grid.attach(self.surface_viewer.drawing_area, 0, 1, 8, 1)
        
        # Add the grid to the window
        self.window.add(self.grid)
        self.window.show_all()

        # initialize other possible buffers        
        self.smp_buffer = []
        self.selected_smp = 0
        self.mtc_buffer = []
        self.selected_mtc = 0
        self.tsne_buffer = []
        self.selected_tsne = 0

        self.generic_buffer = []
        self.selected_generic = 0

        self.generic_names = []


        # lists of attributes and types, each attribute is a vertex buffer id
        self.attributes = []

    def load_generic_map (self, map_data, name=''):
 
        self.surface_viewer.drawing_area.make_current()
        scaled = (map_data-min(map_data))/(max(map_data)-min(map_data))

        bufid = self.surface_viewer.load_attrib_buffer(scaled)
        self.generic_buffer.append(bufid)

        if name == '':
            name = 'generic'
        self.generic_names.append(name)
        self.cycle_generic_button.set_label(name + ' ' + str(self.selected_generic+1) + ' / ' + str(len(self.generic_buffer)))


    def load_generic_poi_map (self, map_data, poi):
 
        self.surface_viewer.drawing_area.make_current()
        data = np.zeros(poi.num_mesh_verts)        
        scaled = (map_data-min(map_data))/(max(map_data)-min(map_data))

        for i in range(poi.num_verts):
            data[poi.verts[i]] = scaled[i]

        bufid = self.surface_viewer.load_attrib_buffer(data)
        self.generic_buffer.append(bufid)


    def load_smp (self, smp):

        self.surface_viewer.drawing_area.make_current()
        # one VBO for each surface map (scale in range[0,1])
        for i in range(len(smp.maps)):
            x = np.asarray(smp.maps[i].data)
            scaled_data = (x-min(x))/(max(x)-min(x))
            bufid = self.surface_viewer.load_attrib_buffer(scaled_data)
            self.smp_buffer.append (bufid)
        self.cycle_smp_button.set_label('SMP ' + str(self.selected_smp+1) + ' / ' + str(len(self.smp_buffer)))

    def load_mtc (self, mtc):

        self.surface_viewer.drawing_area.make_current()
        # one VBO for each time step
        #for i in range(self.mtc.num_time_pts):
        for i in range(50):
            x = np.asarray(mtc.data[:,i])
            scaled_data = (x-min(x))/(max(x)-min(x))
            bufid = self.surface_viewer.load_attrib_buffer(scaled_data)
            self.mtc_buffer.append (bufid)

    def load_tsne (self, tsne_plt, poi):
        
        self.surface_viewer.drawing_area.make_current()
        data = np.zeros(poi.num_mesh_verts)        
        scaled = (tsne_plt.tsne.labels-min(tsne_plt.tsne.labels))/(max(tsne_plt.tsne.labels)-min(tsne_plt.tsne.labels))

        for i in range(poi.num_verts):
            data[poi.verts[i]] = scaled[i]

        bufid = self.surface_viewer.load_attrib_buffer(data)
        self.tsne_buffer.append(bufid)


    def timeout (self):
        if (self.animate_mtc): 
            self.selected_mtc += 1
            if self.selected_mtc >= len(self.mtc_buffer):
                self.selected_mtc = 0
            self.animation_timer = Timer(0.5, self.timeout)
            self.animation_timer.start()

            self.surface_viewer.selected_attrib = self.mtc_buffer[self.selected_mtc]
            self.surface_viewer.drawing_area.queue_render()

        print ('timer', self.selected_mtc)

    def toggle_animation (self, widget):
        if (self.animate_mtc):
            self.animation_timer.cancel()
        else:
            self.animation_timer = Timer(0.5, self.timeout)
            self.animation_timer.start()
        self.animate_mtc = not self.animate_mtc

    def cycle_render_type(self, widget):
        bufid = -1
        if self.render_type == 'smp':
            self.render_type = 'mtc'
            if self.mtc_buffer:
                bufid = self.mtc_buffer[self.selected_mtc]
        elif self.render_type == 'mtc':
            self.render_type = 'tsne'
            if self.tsne_buffer:
                bufid = self.tsne_buffer[self.selected_tsne]
        elif self.render_type == 'tsne':
            self.render_type = 'generic'
            if self.generic_buffer:
                bufid = self.generic_buffer[self.selected_generic]
        elif self.render_type == 'generic':
            self.render_type = 'smp'
            if self.smp_buffer:
                bufid = self.smp_buffer[self.selected_smp]

        self.surface_viewer.selected_attrib = bufid

        self.render_type_button.set_label(str(self.render_type))
        self.surface_viewer.drawing_area.queue_render()

    def toggle_light (self, widget):
        self.use_light = not self.use_light
        self.surface_viewer.drawing_area.queue_render()

    def cycle_smps (self, widget):
        self.selected_smp += 1
        if self.selected_smp >= len(self.smp_buffer):
            self.selected_smp = 0

        self.cycle_smp_button.set_label('SMP ' + str(self.selected_smp+1) + ' / ' + str(len(self.smp_buffer)))
        if self.render_type == 'smp':
            self.surface_viewer.selected_attrib = self.smp_buffer[self.selected_smp]


    def cycle_generics (self, widget):
        self.selected_generic += 1
        if self.selected_generic >= len(self.generic_buffer):
            self.selected_generic = 0
        self.cycle_generic_button.set_label(self.generic_names[self.selected_generic] + ' ' + str(self.selected_generic+1) + ' / ' + str(len(self.generic_buffer)))
        if self.render_type == 'generic':
            self.surface_viewer.selected_attrib = self.generic_buffer[self.selected_generic]

        self.surface_viewer.drawing_area.queue_render()

    def on_key_pressed (self, win, event):
        """
        Handles keyboard input
        """
        key = Gdk.keyval_name(event.keyval)
        if key == 'Escape':
            if (self.animate_mtc):
                self.animation_timer.cancel()
            quit()
        elif key == '+':
            self.cycle_smps(win)
        elif key == 'a':            
            self.toggle_animation(win)
        elif key == 'i':
            self.surface_viewer.toggle_light(win)
        elif key == 't':
            self.cycle_render_type(win)
 


data_path = '../../mri-data/achbot-np/'
srf = srf.SRF(data_path + 'achbot_mean_TAL_WM_RH_BL2_RECOSMx_D80k_INFL.srf')
smp = smp.SMP(data_path + 'achbot_sweet_glmdec_D3-D7_beta.smp')
mtc = mtc.MTC(data_path + 'achbot_mbepi_swe_pp_nii_TAL2xs_RH.mtc', data_path + 'achbot_mbepi_swe_pp_nii_TAL2xs_RH.mtc.npy')


def main():
    g = SurfaceViewerWindow(srf=srf)
    g.load_smp(smp)
    g.load_mtc(mtc)
    
#    g.load_tsne(tsne)

    Gtk.main()

if __name__ == '__main__':
    main()
          

