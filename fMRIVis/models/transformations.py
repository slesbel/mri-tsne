#
# transformations..py
#
# Transforms data from one mesh to another mesh
#
# Created by Ricardo Marroquim on 19-03-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import sys
import os
import numpy as np
import math
from tqdm import tqdm, trange
from sklearn import manifold


sys.path.append('../')
from fMRIVis.datastructs import smp, ssm
from fMRIVis.vis import glutils

def map_mesh_smp_to (ssm, source_smp):
    """
        Maps SMP maps to another surface using a SSM (Sphere to Sphere Mapping)
    """

    target_smp = smp.SMP()
    target_smp.version = source_smp.version
    target_smp.number_verts = ssm.number_target_verts
    target_smp.number_maps = source_smp.number_maps
   
    for m in source_smp.maps:
        target_map = smp.SurfaceMap()
        target_smp.maps.append(target_map)
        
    for v in ssm.map:
        for i, m in enumerate(target_smp.maps):
            if v > 0 and v < ssm.number_target_verts:
                 m.data.append( source_smp.maps[i].data[int(v)]  )
            else:
                 m.data.append( 0.0  )


    return target_smp

def map_mesh_data_to (ssm, source_data):
    """
        Maps data from source to target mesh using SSM
    """

    target_data = np.full(shape=ssm.map.shape[0], fill_value=0.0)

    for i, v in enumerate(ssm.map):
        target_data[i] = source_data[int(v)]

    return target_data


def map_mesh_poi_data_to (ssm, source_data, poi):
    """
        Maps data from poi of source mesh to target mesh using SSM
    """

    target_data = np.full(shape=ssm.number_target_verts, fill_value=0.0)
    v_list = poi.verts.tolist()
    for i in range(target_data.shape[0]):
        # source vertex id
        source_vertex = int(ssm.map[i])
        # checks if source vertex is in POI
        poi_id = v_list.index(source_vertex) if source_vertex in v_list else -1
        if poi_id != -1:
            # get data from poi region and maps to target mesh
            target_data[i] = source_data[poi_id]

    return target_data

def map_mesh_data_to_poi (ssm, source_data, poi):
    """
        Maps data from source mesh to poi of target mesh using SSM
    """

    target_data = np.full(shape=ssm.number_target_verts, fill_value=0.0)
    v_list = poi.verts.tolist()
    for i in range(target_data.shape[0]):
        # source vertex id
        source_vertex = int(ssm.map[i])
        # checks if target vertex is in POI
        poi_id = v_list.index(i) if i in v_list else -1
        if poi_id != -1:
            # get data from poi region and maps to target mesh
            target_data[i] = source_data[source_vertex]

    return target_data

def map_mesh_poi_data_to_poi (ssm, source_data, source_poi, target_poi):
    """
        Maps data from poi of source mesh to poi of target mesh using SSM
    """

    target_data = np.full(shape=ssm.number_target_verts, fill_value=0.0)
    source_list = source_poi.verts.tolist()
    target_list = target_poi.verts.tolist()
    for i in range(target_data.shape[0]):
        # source vertex id
        source_vertex = int(ssm.map[i])
        # checks if target vertex is in POI
        source_poi_id = source_list.index(source_vertex) if source_vertex in source_list else -1
        target_poi_id = target_list.index(i) if i in target_list else -1
        if target_poi_id != -1:
            if source_poi_id != -1:

                # get data from poi region and maps to target mesh
                target_data[i] = source_data[source_poi_id]
            else:
                target_data[i] = 1.0

    return target_data


def scale_matrix4 (matrix, scale):
    scaled_matrix = matrix
    scaled_matrix[:3,:3] = scaled_matrix[:3,:3] * scale
    return scaled_matrix


def rotation_matrix_from_axis_angle(axis, theta, size=4):
    """
        Return the rotation matrix associated with counterclockwise rotation about the given axis by theta radians.
    """
    axis = np.asarray(axis)
    norm = axis.dot(axis)
    axis = axis/math.sqrt(norm)
    a = math.cos(theta/2.0)
    b, c, d = -axis*math.sin(theta/2.0)
    aa, bb, cc, dd = a*a, b*b, c*c, d*d
    bc, ad, ac, ab, bd, cd = b*c, a*d, a*c, a*b, b*d, c*d

    if size == 4:
        return np.array([[aa+bb-cc-dd, 2*(bc+ad), 2*(bd-ac), 0.0],
                        [2*(bc-ad), aa+cc-bb-dd, 2*(cd+ab), 0.0],
                        [2*(bd+ac), 2*(cd-ab), aa+dd-bb-cc, 0.0],
                        [0.0, 0.0, 0.0, 1.0]], dtype=np.float32)
    elif size == 3:
        return np.array([[aa+bb-cc-dd, 2*(bc+ad), 2*(bd-ac)],
                        [2*(bc-ad), aa+cc-bb-dd, 2*(cd+ab)],
                        [2*(bd+ac), 2*(cd-ab), aa+dd-bb-cc]], dtype=np.float32)


def isomap_projection (points3d, n_neighbors=10, n_components=2):
    """
        Projects 3d points to 2d using isomap method
    """
    points2d = manifold.Isomap(n_neighbors, n_components).fit_transform(points3d)
    return points2d

def spherical_projection (points3d):
    """
        Projects 3d points to sphere at origin with radius 1
    """

    centroid = np.average(points3d, axis=0)

    sphere_pts = points3d
    for i, p in enumerate(points3d):
        v = p - centroid
        sphere_pts[i] = (v/np.linalg.norm(v))

    return sphere_pts

def stereographic_projection (points3d):
    """
        Stereographic projection
    """

    #project to sphere
    sphere_pts = spherical_projection (points3d)

    # stereographic projection
    planar_pts = np.zeros(shape=(sphere_pts.shape[0],2))
    for i, p in enumerate(sphere_pts):
       planar_pts[i] = [p[0]/(1.0-p[2]), p[1]/(1.0-p[2])]
    return planar_pts



def planar_projection (points3d):
    """
        Projects 3d points to plane
        OBS: assumes points come from a surface patch that can be properly projected without intersections
    """

    centroid = np.average(points3d, axis=0)

    # translate all points to put center of mass at origin
    pts = points3d - centroid

    # build scatter matrix
    scatter_matrix = np.transpose(pts).dot(pts)

    # compute eigenvalues/vectors of scatter matrix and extract plane normal (min eigenvalue)
    w, v = np.linalg.eig(scatter_matrix)
    normal_vector = v[ np.argmin(w) ]
    normal_vector = normal_vector / np.linalg.norm(normal_vector)

    # compute rotation
    axis = np.cross ([0, 0, 1], normal_vector)
    axis = axis / np.linalg.norm(axis)
    angle = math.acos(np.dot (normal_vector, [0, 0, 1]))
    rot_matrix = rotation_matrix_from_axis_angle (axis, angle, size=3)
    rot_matrix = np.transpose(rot_matrix)

    # project patch points to surface and rotate to align with XY plane
    planar_points = pts
    for i, p in enumerate(pts):
        d = np.dot(p, normal_vector) * normal_vector
        proj = p - d
        proj = rot_matrix.dot(proj)
        proj[2] = 0.0
        planar_points[i] = proj

    return planar_points


def laplacian_smooth (srf, iterations=1):
    """
        Apply the laplacian operator on a surface mesh
    """

    smooth_srf = srf
    print ('laplacian smooth')
    for it in trange(iterations):
        for i in trange(smooth_srf.number_verts):
            laplace = np.zeros(3)
            for n in smooth_srf.nearest_neighbors[i]:
                laplace += smooth_srf.get_vertex(n)
            laplace = laplace / float(len(smooth_srf.nearest_neighbors[i]))
            smooth_srf.set_vertex(i, laplace)

    return smooth_srf
 
def patch2plane (srf):
    """
        Projects a surface patch to a plane and export new planar srf
    """
    # original surface points
    srf_points = np.zeros(shape=(srf.number_verts,3))
    for i in range (srf.number_verts):
        srf_points[i] = srf.get_vertex(i)

    planar_points = isomap_projection(srf_points)

    planar_srf = srf
    p3 = np.zeros(3)
    p3[2] = 0.0
    for i, p in enumerate(planar_points):
        p3[:2] = p
        planar_srf.set_vertex(i, p3)

    return planar_srf

