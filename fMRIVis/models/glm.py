#
# glm.py
#
# perform GLM on fMRI data
#
# Created by Ricardo Marroquim on 17-01-2018
# Copyright (c) 2017 Federal University of Rio de Janeiro. All rights reserved
#

import sys
import numpy as np
from nipy.core.api import Image, vox2mni
from nipy.modalities.fmri import glm, design_matrix, experimental_paradigm
import nibabel as nib
import matplotlib.pyplot as plt
from nipy.labs.viz import plot_map, cm

sys.path.append('../')
from fMRIVis.datastructs import protocol, vtc, contrast, sdm
from fMRIVis.models import hrf


class GLM:
    """
        Encapsulates GLM methods for VTC files
    """

    def __init__(self):
        """
            Initializes the GLM class
        """
        self.frametimes = []
        self.condition = []
        self.onset = []
        self.duration = []
        self.tr = 0
        self.hrf_model = 'canonical'
        self.drift_model = 'cosine'
        self.hfcut = 256
        self.out_dir = '../../../glm/'
        self.out_filename = 'zmap'
        self.protocol = protocol.Protocol()
        self.contrasts = {}

    def set_out_dir(self, out_dir):
        """
            Sets the directory to output result of glm

            :param out_dir: output directory
        """
        self.out_dir = out_dir

    def set_out_filename(self, out_file):
        """
            Sets the output filename result of glm

            :param out_file: output filename
        """
        self.out_filename = out_file

    def load_vtc(self, vtc_filename, np_filename=''):
        """
            Load necessary data from VTC file (Volume Time Course)

            :param vtc_filename: VTC file to read 4D data
            :param np_filename: optional numpy file with VTC data to speed up read from disk
        """
        voltc = vtc.VTC(vtc_filename, np_filename)
        voltc.print_header()
        self.tr = voltc.tr / 1000.0  # glm expects tr in secs, vtc is in ms
        self.vols = voltc.number_volumes
        self.nifti = voltc.toNIfTI()

        # glm expects 4x4 affine and not 5x5
        self.nifti.affine = voltc.affine

    def load_protocol(self, protocol_filename):
        """
            Loads the protocol and computes respective variables for design matrix

            :param protocol_filename: PRT file
        """

        self.protocol = protocol.Protocol(protocol_filename)
        print("Vols")
        print(self.vols)
        print("TR")
        print(self.tr)
        self.frametimes = np.arange(0, self.vols * self.tr, self.tr )

        # create a list with tuples (start, end, condition name)
        sort = []
        print("self.protocol.conditions")
        print(self.protocol.conditions)
        for c in self.protocol.conditions:
            for i in c.intervals:
                sort.append([i[0], i[1], c.name])

        # sort list by start time
        sort = sorted(sort, key=lambda x: x[0])

        # create arrays for glm
        # note: onset for glm starts from 0, not 1
        # note: times must be multiplied by tr in secs
        # note: duration is how many scans in interval, thats why there is a +1
        for s in sort:
            self.onset.append((s[0] - 1) * self.tr)
            self.duration.append((s[1] - s[0] + 1) * self.tr)
            self.condition.append(s[2])

        print(self.condition)
        print(len(self.condition))
        print(self.onset)
        print(self.duration)
        print(np.sum(self.duration))

    def create_design_matrix(self, sdm=None):
        """
            Creates a Design Matrix for GLM
        """
        print('Loading design matrix...')
        paradigm = experimental_paradigm.BlockParadigm(self.condition, self.onset, self.duration)
        # paradigm = experimental_paradigm.load_paradigm_from_csv_file(paradigm_file)['0']
        print(sdm)
        #paradigm.write_to_csv(self.out_dir + 'paradigm.csv')
        if sdm == None:
            print("####################################")
            print(self.frametimes)
            print(paradigm)
            print(self.hrf_model)
            print(self.drift_model)
            print(self.hfcut)
            print("####################################")
            self.design_matrix = design_matrix.make_dmtx(self.frametimes, paradigm, hrf_model=self.hrf_model
                                                         ,drift_model=self.drift_model, hfcut=self.hfcut)
            print(self.design_matrix)
            print(self.design_matrix.matrix.shape)
        else:
            self.design_matrix = design_matrix.make_dmtx(self.frametimes, paradigm, hrf_model=self.hrf_model,
                                                         drift_model=self.drift_model, hfcut=self.hfcut,
                                                         add_regs=sdm.data, add_reg_names=sdm.predictors_names)

        print(self.design_matrix.matrix.shape)

        # fmri_glm = glm.GeneralLinearModel(self.nifti, self.design_matrix.matrix, mask='compute')

        print(self.design_matrix.names)

        ax = self.design_matrix.show()
        ax.set_position([.05, .25, .9, .65])
        ax.set_title('Design matrix')
        plt.savefig('../tmp/design_matrix.png')
        plt.show()




    def specify_contrasts(self, contrast_list):
        """
            Specifies the contrasts for GLM

            First appends zeros to fill contrast with drift and constant predictors
        """

        num_predictors = len(self.design_matrix.names)

        for c in contrast_list.contrasts:
            missing_ctr = num_predictors - c.number_values
            c.data = np.append(c.data, np.zeros(missing_ctr))
            c.number_values = num_predictors
            print(c.data)

        self.contrast_list = contrast_list

    def glm_analysis(self):
        print('Fitting a GLM (this takes time)...')
        fmri_glm = glm.FMRILinearModel(self.nifti, self.design_matrix.matrix, mask='compute')
        fmri_glm.fit(do_scaling=True, model='ar1')

        #########################################
        # Estimate the contrasts
        #########################################

        print('Computing contrasts...')
        for index, c in enumerate(self.contrast_list.contrasts):
            print('  Contrast % 2i out of %i: %s' % (index + 1, self.contrast_list.number_contrasts, c.name))
            print('  id and val', c.name, c.data)
            # save the z_image
            image_path = self.out_dir + self.out_filename + c.name + '.nii'
            z_map, = fmri_glm.contrast(c.data, con_id=c.name, output_z=True)
            nib.save(z_map, image_path)
            data_path = self.out_dir + self.out_filename + c.name + '.npy'
            np.save(data_path, z_map.get_data())

            # Create snapshots of the contrasts
            vmax = max(-z_map.get_data().min(), z_map.get_data().max())
            plot_map(z_map.get_data(), z_map.get_affine(),
            #cmap= cm.cold_hot, vmin=-vmax, vmax=vmax, slicer='z', black_bg=True, threshold=2.5, title=c.name)
            cmap = cm.cold_hot, vmin = -vmax, vmax = vmax, slicer = 'z', black_bg = True, threshold = 2.6)
            plt.savefig ('../tmp/zmap.png')

plt.show()


########################################
# Perform a GLM analysis
########################################
# vtc_glm = GLM()
# vtc_glm.load_vtc(in_vtc_filename, in_vtc_np_filename)
# vtc_glm.load_protocol(in_protocol_filename)
# vtc_glm.create_design_matrix()
# contrasts = {}
# contrasts['LvsR'] = np.array([0,0.0,1.0,-1.0,0,0,0,0,0,0,0])
# vtc_glm.specify_contrasts(contrasts)
# vtc_glm.glm_analysis()
