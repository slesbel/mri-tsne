#
# hrf.py
#
# Hemodynamic response function
#

import sys
from scipy import stats
import numpy as np
import nibabel as nib
import math
import matplotlib.pyplot as plt
from nipy.modalities.fmri import hrf, utils, hemodynamic_models
import peakutils
from scipy import signal

class HRF_match:
    """
        HRF match class tries to find HRF patterns in voxel signals
    """

    def __init__ (self, show_plot=False):

        # create a single impulse HRF
        time = 16.0
        hrf_model = hemodynamic_models.spm_hrf(tr=1.0, oversampling=1.0, time_length=time, onset=0)
        t = np.linspace(0.0, 8.0, int(time))
        t = np.arange(0.0, time, 1.0)

        # box filter to represent stimulus duration
        box_filter = np.zeros( int(time) )
        box_filter [ (t>=1.0) & (t < time-1) ] = 1

        # convolve both signals to obtain the predicted HRF response during the stimulus
        self.hrf_func = np.convolve(hrf_model, box_filter)
#        self.hrf_func = hrf_model

        # normalize HRF
        #self.hrf_func = (self.hrf_func - np.mean(self.hrf_func)) /  (np.std(self.hrf_func))
        #self.hrf_func = self.hrf_func - np.min(self.hrf_func)

        #hrf_func = np.pad(hrf_func, (0,250-len(hrf_func)), mode='constant', constant_values=(0.0))

        # scale to fit HRF function in condition (stimulus) period

        #plot function
        if show_plot:
            plt.plot(box_filter)
            plt.show()
            a=plt.gca()
            a.set_xlabel(r'$t$')
            a.set_ylabel(r'$h_{can}(t)$')
            plt.plot(self.hrf_model)
            plt.show()

    def draw_conditions(self):
        fixation_color = 'gray'
        rvf_color = 'red'
        lvf_color = 'green'
        bvf_color = 'blue'
        condition_alpha = 0.3
        a=plt.gca()
        a.axvspan(0, 6, alpha=condition_alpha, color=fixation_color) #fixation
        a.axvspan(7, 24, alpha=condition_alpha, color=rvf_color) #RVF
        a.axvspan(25, 33, alpha=condition_alpha, color=fixation_color) #fixation
        a.axvspan(34, 51, alpha=condition_alpha, color=lvf_color) #LVF
        a.axvspan(52, 60, alpha=condition_alpha, color=fixation_color) #fixation
        a.axvspan(61, 78, alpha=condition_alpha, color=bvf_color) #BVF
        a.axvspan(79, 87, alpha=condition_alpha, color=fixation_color) #fixation
        a.axvspan(88, 105, alpha=condition_alpha, color=rvf_color) #RVF
        a.axvspan(106, 114, alpha=condition_alpha, color=fixation_color) #fixation
        a.axvspan(115, 132, alpha=condition_alpha, color=lvf_color) #LVF
        a.axvspan(133, 141, alpha=condition_alpha, color=fixation_color) #fixation
        a.axvspan(142, 159, alpha=condition_alpha, color=bvf_color) #BVF
        a.axvspan(160, 168, alpha=condition_alpha, color=fixation_color) #fixation
        a.axvspan(169, 186, alpha=condition_alpha, color=rvf_color) #RVF
        a.axvspan(187, 195, alpha=condition_alpha, color=fixation_color) #fixation
        a.axvspan(196, 213, alpha=condition_alpha, color=lvf_color) #LVF
        a.axvspan(214, 222, alpha=condition_alpha, color=fixation_color) #fixation
        a.axvspan(223, 240, alpha=condition_alpha, color=bvf_color) #BVF
        a.axvspan(241, 249, alpha=condition_alpha, color=fixation_color) #fixation


    def filter_peaks (self, sig):
        """
            Remove isolated peaks from the signal
            Substitute isolated peaks by mean between adjacent values
        """
        epsilon = 1.5

        for x in range(1, len(sig)-1):
            if sig[x]-sig[x-1] > epsilon and sig[x] - sig[x+1] > epsilon:
                sig[x] = (sig[x+1]+sig[x-1])*0.5

        return sig


    def smooth_signal (self, sig, kernel=3):
        """
            Spatial smoothing of 1D signal using Gaussian kernel
        """

        window = signal.gaussian(7, std=1)
        norm = np.linalg.norm(window)
        window = window / np.sum(window)
        return np.convolve (sig, window, mode='same')

    def convert_to_HRF (self, input_signal, show_plot=False):
        """
            Converts a signal to a clean HRF fit
        """

        smooth_signal = input_signal
        #smooth_signal = self.smooth_signal(smooth_signal)
        smooth_signal = signal.medfilt(input_signal, 3)
        ### convolve voxel with HRF function

        # zscore transform of signal
        zscore = stats.zscore(smooth_signal)

        zscore = (zscore - np.mean(zscore)) / (np.std(zscore))

        corr = np.convolve (zscore, np.flipud(self.hrf_func), mode='same')

        # find peaks in cross correlation
        peak_ind = peakutils.indexes(corr, thres=0.0, min_dist=10)

        # normalize cross correlation result
        corr = (corr - np.mean(corr)) / np.std(corr)

        #peak_ind = peakutils.interpolate(range(0, len(corr)), corr, ind=peak_ind)
        #peak_ind = signal.find_peaks_cwt(corr, np.arange(1,30))
       
        valid_peaks = 0
        peak_threshold = 0.75
       
        clean_signal = np.zeros(zscore.shape)
                
        hrf_hsize = int(len(self.hrf_func)*0.5)
        center = int(len(clean_signal)*0.5)

        for ind in peak_ind:
            if corr[ind] > peak_threshold:
                #print str(ind) + ': ' + str(ind-hrf_hsize) + ' , ' + str(ind+hrf_hsize)
                if ind+hrf_hsize > len(clean_signal):
                    clean_signal = np.pad(clean_signal, (0, 1+ind+hrf_hsize-len(clean_signal)), 'constant')
                inds = np.arange(ind-hrf_hsize, ind+hrf_hsize)
                np.put (clean_signal, inds, self.hrf_func*corr[ind])
                valid_peaks += 1

        clean_signal = clean_signal[:250]

        if show_plot:
            self.draw_conditions()
            plt.plot(input_signal)
            plt.plot(smooth_signal)
            plt.show()
            a=plt.gca()
            self.draw_conditions()
            plt.plot(corr)
            plt.plot(zscore)
            for ind in peak_ind:
                if corr[ind] > peak_threshold:
                    a.axvspan(ind-1, ind+1, alpha=0.7, color='red') #fixation
            plt.show()
            self.draw_conditions()
            plt.plot(zscore)
            plt.plot(clean_signal)
            plt.show()

        return valid_peaks, clean_signal, zscore
