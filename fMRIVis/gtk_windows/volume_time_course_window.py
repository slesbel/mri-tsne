#
# volume_time_course_window.py
#
# Gtk window containg volume time course plot (selected voxels for a given volume)
#
# Created by Ricardo Marroquim on 03-05-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

import sys
import os
from matplotlib.backends.backend_gtk3cairo import FigureCanvasGTK3Cairo as FigureCanvas
import matplotlib as mpl
from matplotlib.figure import Figure
from matplotlib.widgets import RadioButtons, Slider, Button
import numpy as np
import math


sys.path.append('../')
sys.path.append('../../')

from fMRIVis.plots import volume_time_course_plot

class VolumeTimeCourseWindow:
    """
        Volume Time Course GTK window
    """
    def on_key_event(self, event):
        """
            Callback for key press event inside canvas
        """
        if event.key == 'escape':
            sys.exit(0)

        self.plot.plot()
        self.canvas.draw()


    def __init__(self, size = [600, 200], name=''):
        """
            Initializes Volume Time Course window class
        """

        self.size = size

        # create GTK window
        self.window = Gtk.Window()
        self.window.resize(self.size[0], self.size[1])
        self.window.set_title("Volume Time Course Viewer " + str(name))
        self.window.connect('delete_event', Gtk.main_quit)
        self.window.connect('destroy', lambda quit: Gtk.main_quit())

        self.figure = Figure(figsize=(12, 12), dpi=100)
        self.canvas = FigureCanvas(self.figure)
        self.window.add(self.canvas)

        self.canvas.mpl_connect("key_press_event", self.on_key_event)

        self.plot = volume_time_course_plot.VolumeTimeCoursePlot()


    def init_subplots (self):
        """
            Create axes for volume time course plot and type button
        """
        self.plot.init_plot (self.figure.add_axes([0.05, 0.15, 0.85, 0.75]))
        self.plot.init_type_button (self.figure.add_axes([0.9, 0.5, 0.06, 0.2]))


       
