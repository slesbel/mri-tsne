#
# tsne_window.py
#
# Gtk window for plotting t-SNE results
#
# Created by Ricardo Marroquim on 12-03-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import gi
gi.require_version('Gtk', '3.0')

from gi.repository import Gtk

import sys
import os
from matplotlib.backends.backend_gtk3cairo import FigureCanvasGTK3Cairo as FigureCanvas
import matplotlib as mpl
from matplotlib.figure import Figure
from matplotlib.widgets import RadioButtons, Slider, Button
import numpy as np

sys.path.append('../')
sys.path.append('../../')

from fMRIVis.vis import tsne
from fMRIVis.plots import tsne_plot



class TsneWindow:
    """
        GTK window for plotting t-SNE results
        This is just the window shell to speed up building visualizations, for the actual plot use tsne_plot.py
    """
    def on_key_event(self, event):
        """
            Callback for key press event inside canvas
        """
        if event.key == 'x':
            sys.exit(0)
        self.canvas.draw()


    def __init__(self, tsne, title="TSNE", size = [600, 600]):
        """
            Initializes t-SNE window class
        """
        self.window = Gtk.Window()
        self.window.connect("destroy", lambda x: Gtk.main_quit())
        self.window.set_default_size(size[0], size[1])
        self.window.set_title(title)
        self.figure = Figure(figsize=(12, 12), dpi=100)
        self.canvas = FigureCanvas(self.figure)
        self.window.add(self.canvas)

        self.plot = tsne_plot.TsnePlot(tsne)

        self.canvas.mpl_connect('key_press_event', self.on_key_event)


    def init_subplots (self, run_tsne_cb=None, clear_selection_cb=None, colorbar_discrete=False, colorbar_text='conditions'):
        """
            Create axes for TSNE plot (scatter, colorbar, buttons ...)
        """
        self.plot.init_scatter_plot(self.figure.add_axes([0.08, 0.05, 0.8, 0.8])) 
        self.plot.init_colorbar(self.figure.add_axes([0.9, 0.05, 0.02, 0.8]), colorbar_text, discrete=colorbar_discrete)
        self.plot.init_perplexity_slider(self.figure.add_axes([0.1, 0.88, 0.8, 0.01]))
        self.plot.init_run_button(self.figure.add_axes([0.05, 0.95, 0.14, 0.02]), run_tsne_cb)
        self.plot.init_save_button(self.figure.add_axes([0.2, 0.95, 0.14, 0.02]))
        self.plot.init_clear_button(self.figure.add_axes([0.35, 0.95, 0.14, 0.02]), clear_selection_cb)
        self.plot.init_annotate_button(self.figure.add_axes([0.5, 0.95, 0.14, 0.02]))
        self.plot.init_show_centers_button(self.figure.add_axes([0.35, 0.92, 0.14, 0.02]))
        self.plot.init_show_points_button(self.figure.add_axes([0.5, 0.92, 0.14, 0.02]))
        self.plot.init_annotation_type_button(self.figure.add_axes([0.65, 0.95, 0.14, 0.02]))



