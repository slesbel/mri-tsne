#
# voxel_selection.py
#
# select voxels of interest and create a binary mask
#
# Created by Ricardo Marroquim on 19-12-2017
# Copyright (c) 2017 Federal University of Rio de Janeiro. All rights reserved
#

import sys
from scipy import stats
import numpy as np
import nibabel as nib
import math
from sklearn import preprocessing
import pandas as pd
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
from nipy.modalities.fmri import hrf, utils, hemodynamic_models
from scipy import signal
import peakutils
from tqdm import trange

sys.path.append('../')
from fMRIVis.models import hrf
from fMRIVis.filters import ccl_filter

has_labels = False
in_MRI_filename = '../../../mri-data/CG_VTC_pp.npy'
in_zmap_filename = '../mri-data/glm_result/LvsR_z_map.npy'
in_labels_filename = '../mri-data/CG_VTC_pp_labels.txt'
out_coords_filename = '../mri-data/voxel_coords.npy'
out_zmap_filename = '../mri-data/voxel_zmap.npy'
out_mask_filename = '../mri-data/voxel_mask.npy'
out_voxels_filename = '../mri-data/CG_voxels.txt'
out_volumes_filename = '../mri-data/CG_volumes.txt'
out_HRF_filename = '../mri-data/CG_HRF_fit.npy'
has_labels = True

# read data
mri_data = np.load(in_MRI_filename)

if mri_data.ndim == 3:
    mri_data = mri_data[..., np.newaxis]

zmap = np.load(in_zmap_filename)

print ('input file dimensions : %s' % (str(mri_data.shape)))

# read labels if available
if (has_labels):
    with open(in_labels_filename) as f:
        labels = f.readlines()
    labels = [x.strip() for x in labels]


def mask_from_zmap (zmap, threshold=2.5):
    """
        Compute a voxel mask from an input z-score mask (result from GLM)
        
        :param zmap: Z-score volume map
        :param threshold: Z-score threshold to create mask
    """
    # create empty mask to select valid voxels
    mask = np.zeros(shape=(zmap.shape[0:3]))
    print ('mask dim : ', mask.shape)
 
    mask[:] = [x>threshold for x in zmap[:,:,:]]
    return mask
    

def fit_hrf (data):
    """
        Fit a HRF model to the time course of the voxels

        :params data: MRI data
        :returns : clean HRF fit, zscore normalized data
    """
    clean_data = np.zeros(shape=(data.shape))
    zscore_data = np.zeros(shape=(data.shape))
    hrf_ = hrf.HRF_match()
    
    for z in trange(data.shape[2], desc='HRF model'):
        for y in range(data.shape[1]):
            for x in range(data.shape[0]):
                voxel = data[x, y, z, :]

                # convert the time series into HRFs, and count peaks
                valid_peaks, clean_signal, zscore = hrf_.convert_to_HRF(voxel)
                clean_data[x,y,z,:] = clean_signal
                
                if np.isnan(zscore).any():                    
                    zscore = np.nan_to_num(zscore)

                zscore_data[x,y,z,:] = zscore

    return clean_data, zscore_data

def select_voxels (data, mean_threshold=90, zscore_threshold=8.0):
    """
        Compute voxel stats to select only significant ones
        Construct a binary mask with selected voxels

        Criteria per voxel are:
            max zscore value threshold
            minimum number of peaks
            mean value threshold

        :param data: MRI data
        :param mean_threshold: minimum mean value per voxel threshold
        :param zscore_threshold: minimum max zscore threshold
        :returns : binary mask, clean HRF version of MRI data
    """
    # create empty mask to select valid voxels
    mask = np.zeros(shape=(data.shape[0:3]))
    clean_data = np.zeros(shape=(data.shape))
    zscore_data = np.zeros(shape=(data.shape))

    hrf_ = hrf.HRF_match()
    
    print ('mean threshold %f', mean_threshold)
    for z in trange(data.shape[2], desc='select voxels'):
        for y in range(data.shape[1]):
            for x in range(data.shape[0]):
                voxel = data[x, y, z, 7:]

                # stats for voxel
                mean_value = np.mean(voxel)
                zscore = stats.zscore(voxel)
                dif_zscore = max(zscore) - min(zscore)

                voxel = data[x, y, z, :]
                voxel[:7] = mean_value
                
                # convert the time series into HRFs, and count peaks
                valid_peaks, clean_signal = hrf_.convert_to_HRF(voxel)
                clean_data[x,y,z,:] = clean_signal
                zscore_data[x,y,z,:] = zscore

                # if meets all criteria, update mask
                if valid_peaks >= 3 and (not(math.isnan( min(zscore) ))) and dif_zscore > zscore_threshold and mean_value > mean_threshold:
                    mask[x,y,z] = 1.0

    return mask, clean_data, zscore_data
 
#voxel = mri_data[46, 6, 27, :]
#voxel = mri_data[1, 17, 16, :]
#hrf_ = hrf.HRF_match(True)
#hrf_.convert_to_HRF(voxel, True)
#sys.exit(0)

#mask, clean_data, zscore_data = select_voxels(mri_data, 100, 7.5)
clean_data, zscore_data = fit_hrf (mri_data)
mask = mask_from_zmap(zmap, 4.5)

dims = mri_data.shape[0] * mri_data.shape[1] * mri_data.shape[2]
print ('valid voxels %i / %i' % (np.count_nonzero(mask == 1.0), dims))

# eliminate isolated voxels from mask
mask = ccl_filter.filter_components(mask)
print ('valid voxels after cc filter %i / %i' % (np.count_nonzero(mask == 1.0), dims))

# save mask and HRF data as numpy arrays
np.save(out_mask_filename, mask)
np.save(out_HRF_filename, clean_data)

# what do we want to analyse? time courses or HRF models?
#mri_data = clean_data
mri_data = zscore_data

# normalize the data, TSNE needs values in range[0,1]
mri_data = (mri_data - np.min(mri_data)) / (np.max(mri_data) - np.min(mri_data))

## create two different files output files with selected voxels

# First output : voxels vs volumes
# also save point coordinates for each selected voxel in different file
out_voxels = open(out_voxels_filename,'w')
pts_coords = []
pts_zmap = []
for z in trange(mri_data.shape[2], desc='writing voxels file'):
    for y in range(mri_data.shape[1]):
        for x in range(mri_data.shape[0]):
            if mask[x,y,z] == 1:
                for v in range(mri_data.shape[3]):
                    value = mri_data[x, y, z, v]
                    out_voxels.write('   ' + str(np.float64(value)))
                out_voxels.write('\n')
                pts_coords.append( [x,y,z] )
                pts_zmap.append ( zmap[x,y,z] )
out_voxels.close()
np.save(out_coords_filename, pts_coords)
np.save(out_zmap_filename, pts_zmap)

# Second output : volumes vs voxels
out_volumes = open(out_volumes_filename,'w')
for v in trange(mri_data.shape[3], desc='writing volumes file'):
    for z in range(mri_data.shape[2]):
        for y in range(mri_data.shape[1]):
            for x in range(mri_data.shape[0]):
               if mask[x,y,z] == 1.0:
                    value = mri_data[x, y, z, v]
                    out_volumes.write('   ' + str(np.float64(value)))
    out_volumes.write('\n')
out_volumes.close()

