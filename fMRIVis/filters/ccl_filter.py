#
# ccl_filter.py
#
# Connected Component Labelling Filter
# Creates a mask with voxels inside connectec components with at least n voxels, where n is given

# Created by Ricardo Marroquim on 17-01-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import sys
from scipy import stats
import numpy as np
import math
from collections import Counter

def search_neighbors(coords, label, vol, labels):
    """
        Returns a list of valid neighbors that have no label yet
    """
    labels[tuple(coords)] = label
    to_visit = []
    for zi in range(coords[2]-1,coords[2]+2):
        if zi >= 0 and zi < vol.shape[2]:
            for yi in range(coords[1]-1,coords[1]+2):
                if yi >=0 and yi < vol.shape[1]:
                    for xi in range(coords[0]-1,coords[0]+2):
                        if xi >= 0 and xi < vol.shape[0]:
                            if labels[xi,yi,zi] == 0 and vol[xi,yi,zi] == 1:
                                to_visit.append([xi,yi,zi])
    return to_visit

def filter_components (vol, min_vol=10):
    """
        CCL filter for creating a mask for regions with at least n voxels

        :param vol: numpy 3D binary array
        :param min_vol: minimum number of voxels to keep component in mask
        :returns: the new mask with filtered components
    """

    mask = np.zeros( vol.shape )
    visited = np.zeros( vol.shape )
    labels = np.zeros( vol.shape )

    # start with 2, leave 0 and 1 for the final mask value
    label = 2

    to_visit = []

    for z in range(0, vol.shape[2]):
        for y in range(0, vol.shape[1]):
            for x in range(0, vol.shape[0]):

                # if valid voxel with empty label start a new component
                if labels[x,y,z] == 0 and vol[x,y,z] == 1:
                    to_visit = search_neighbors([x,y,z], label, vol, labels)
                    while (len(to_visit) > 0):
                        neighbor = to_visit.pop()
                        if labels[tuple(neighbor)] == 0:
                            to_visit.extend(search_neighbors(neighbor, label, vol, labels))
                    label = label + 1 # update label
                    
    print ('labels : %s' % (label))
    flat_labels = labels.flatten()
    counter = Counter(flat_labels)
    #sorted(counter.items(), key=lambda i: i[1], reverse=True)

    # for every component with less than n voxels, remove voxels from mask
    # mark remaining regions with unique labels


    final_labels = np.zeros (vol.shape)

    num_regions = 0
    for l in counter:
        if counter[l] < min_vol:
            final_labels [ labels == l ] = 0
        elif l != 0.0:
            num_regions += 1
            final_labels [ labels == l ] = num_regions
            print ('component / size : ', num_regions, ' / ', counter[l])

    print ('number of regions left', num_regions)

    return final_labels
       
