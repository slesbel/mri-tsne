#
# data_transfrm.py
#
# Transforms data from low/high resolution to high/low resolution

# Created by Ricardo Marroquim on 07-05-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import sys
from scipy import stats
import numpy as np
import math
from collections import Counter
from tqdm import tqdm, trange

def max_operator (data, coord):
    """
        Apply max operator on closest voxels
    """
    coord = np.asarray(coord)
    integral = ((np.modf (coord))[1]).astype(int)

    values = [data[tuple(integral + [0,0,0])],
            data[tuple(integral + [1,0,0])],
            data[tuple(integral + [0,0,1])],
            data[tuple(integral + [1,0,1])],
            data[tuple(integral + [0,1,0])],
            data[tuple(integral + [1,1,0])],
            data[tuple(integral + [0,1,1])],
            data[tuple(integral + [1,1,1])]]

    return max(values)

 
def trilinear (data, coord):
    """
        Apply trilinear interpolation
    """

    coord = np.asarray(coord)
    frac = (np.modf (coord))[0]
    integral = ((np.modf (coord))[1]).astype(int)

    c000 = data[tuple(integral + [0,0,0])]
    c100 = data[tuple(integral + [1,0,0])]
    c001 = data[tuple(integral + [0,0,1])]
    c101 = data[tuple(integral + [1,0,1])]
    c010 = data[tuple(integral + [0,1,0])]
    c110 = data[tuple(integral + [1,1,0])]
    c011 = data[tuple(integral + [0,1,1])]
    c111 = data[tuple(integral + [1,1,1])]

    # interpolate along x direction
    c00 = c000*(1.0-frac[0]) + c100*frac[0]
    c01 = c001*(1.0-frac[0]) + c101*frac[0]
    c10 = c010*(1.0-frac[0]) + c110*frac[0]
    c11 = c011*(1.0-frac[0]) + c111*frac[0]

    # interpolate along y direction
    c0 = c00*(1.0-frac[1]) + c10*frac[1]
    c1 = c01*(1.0-frac[1]) + c11*frac[1]
    
    # interpolate along z direction
    c = c0*(1.0-frac[2]) + c1*frac[2]

    return c

def to_highres (lowres_data, vtc, highres_dim, interpolation='nearest'):
    """
        Transform data from functional resolution to structural resolution
    """

    # create empty highres data
    highres_data = np.zeros( highres_dim ).astype(float)

    # for each highres voxel interpolate from lowres data

    res = vtc.voxel_resolution
    print ('to highres')
    for z in trange(highres_data.shape[2]):
        if z >= vtc.zstart and z < vtc.zend - res:
            for y in range(highres_data.shape[1]):
                if y >= vtc.ystart and y < vtc.yend - res:
                    for x in range(highres_data.shape[0]):
                        if x >= vtc.xstart and x < vtc.xend - res:
                            hr_coord = [x, y ,z]
                            lr_coord = [(x-vtc.xstart)/float(res), (y-vtc.ystart)/float(res), (z-vtc.zstart)/float(res)]
                            if interpolation == 'nearest':
                                lr_coord = np.asarray(lr_coord)
                                lr_coord = tuple(lr_coord.astype(int))
                                #print (hr_coord, ' --> ', lr_coord)
                                highres_data[tuple(hr_coord)] = lowres_data[lr_coord]
                            elif interpolation == 'trilinear':
                                #print (hr_coord, ' --> ', lr_coord)
                                highres_data[tuple(hr_coord)] = trilinear(lowres_data, lr_coord)

    return highres_data

def to_lowres (highres_data, vtc, lowres_dim, interpolation='nearest'):
    """
        Transform data from structural resolution (vmr) to functional resolution
    """

    # create empty lowres data
    lowres_data = np.zeros( lowres_dim ).astype(float)

    # for each lowres voxel interpolate from highres data

    res = vtc.voxel_resolution

    for z in range(lowres_data.shape[2]):
        for y in range(lowres_data.shape[1]):
                for x in range(lowres_data.shape[0]):
                    lr_coord = [x, y, z]
                    hr_coord = [x*float(res)+vtc.xstart, y*float(res)+vtc.ystart, z*float(res)+vtc.zstart]
                    if interpolation == 'nearest':
                        hr_coord = np.asarray(hr_coord)
                        hr_coord = tuple(hr_coord.astype(int))
                        lowres_data[tuple(lr_coord)] = highres_data[hr_coord]
                    elif interpolation == 'trilinear':
                        lowres_data[tuple(lr_coord)] = trilinear(highres_data, hr_coord)
                    elif interpolation == 'max':
                        lowres_data[tuple(lr_coord)] = max_operator(highres_data, hr_coord)

    return lowres_data

