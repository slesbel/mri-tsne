#
# poi.py
#
# Patches of Interest for Surface files
#
# Created by Ricardo Marroquim on 15-02-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import numpy as np
import struct
import os.path

class POI:
    """
        Patches of Interest for Surface mesh from Brainvoyager
    """

    def __init__(self, poi_filename='', np_filename=''):
        """
            Inits the POI class, if given filename imports the data
        """
        self.file_version = 0
        self.mesh_file = ''
        self.num_mesh_verts = 0
        self.num_pois = 0
        self.name = ''
        self.info_file = ''
        self.color = [0, 0, 0]
        self.label_vertex = 0
        self.num_verts = 0
        self.num_poi_mtc = 0
        self.verts = [] # only indices, not the coordinates
        self.mask = []

        # checks no numpy filename passed, checks if exists one with same name and npy extension
        if not np_filename or not os.path.isfile(np_filename):            
            if os.path.isfile(poi_filename + '.npy'):
                np_filename = poi_filename + '.npy'

        if poi_filename:
            self.import_poi (poi_filename, np_filename)

    def print_header(self):
        """
            Print header information
        """

        print ('\n################################################')
        print ('Header info for file ', self.poi_filename)
        print ('Version Number ', self.file_version)
        print ('Mesh File ', self.mesh_file)
        print ('Number of Mesh Vertices ', self.num_mesh_verts)
        print ('Number of POIs ', self.num_pois)
        print ('POI Name ', self.name)
        print ('Info Text Filename ', self.info_file)
        print ('Color ', self.color)
        print ('Label Vertex ', self.label_vertex)
        print ('Number of Vertices in POI ', self.num_verts)
        print ('Number of POI MTCs', self.num_poi_mtc)
#        print ('Verts shape ', self.verts.shape)
        print ('################################################\n')

    def load_np (self, filename):
        """
            Loads the MTC data from a numpy array directly
        """
        self.verts = np.load(filename)

    def create_mask (self):
        """
            Creates a binary mask with same dimension of mesh verts
        """
        self.mask = np.zeros(self.num_mesh_verts)
        for v in self.verts:
            self.mask[v] = 1

    def read_poi_header (self, line_list):
        """
            read a header value from POI file represented as a line list
            
        """
        # searches for first non empty line
        line = line_list.pop(0)
        while not line:
            line = line_list.pop(0)
        # splits line where there are spaces
        line = line.split()
        # removes header
        header = line.pop(0)
        # returns everything else but header
        return line

    def import_poi (self, poi_filename, np_filename=''):
        """
            Imports POI file (Patches of Interest)
            :param poi_filename: name (with path) of POI file
            :param np_filename: name (with path) of optional numpy file with verts (in this case does not import the verts, only header info)
        """

        self.poi_filename = poi_filename
        with open(self.poi_filename, "r") as fin:
            line_list = fin.read().splitlines() 

            self.file_version = self.read_poi_header(line_list)[0]
            self.mesh_file = self.read_poi_header(line_list)[0]
            self.num_mesh_verts = int(self.read_poi_header(line_list)[0])
            self.num_pois = int(self.read_poi_header(line_list)[0])
            self.name = self.read_poi_header(line_list)[0]
            self.info_file = self.read_poi_header(line_list)[0]
            self.color = list(map(int, self.read_poi_header(line_list)))
            self.label_vertex = int(self.read_poi_header(line_list)[0])
            self.num_verts = int(self.read_poi_header(line_list)[0])

            # normalize color attributes in range [0,1] instead of [0,255]
            self.color = [c / 255.0 for c in self.color]
 
            self.verts = []
            if np_filename and os.path.isfile(np_filename):
                self.load_np (np_filename)              
                for v in range(self.num_verts):
                    line_list.pop(0)
            else:
                for v in range(self.num_verts):
                    vert = int(line_list.pop(0))
                    self.verts.append(vert)
                self.verts = np.asarray(self.verts)
                np.save(self.poi_filename, self.verts)

            self.num_poi_mtc = int(self.read_poi_header(line_list)[0])

            self.create_mask()
