    #
# vtc.py
#
# Volume Time Course
#
# Created by Ricardo Marroquim on 15-02-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import numpy as np
from nipy.core.api import Image, vox2mni
import struct
import os.path

class VTC:
    """
        Volume Time Course data imported from Brainvoyager
    """

    def __init__(self, vtc_filename='', np_filename=''):
        """
            Inits the VTC class, if given filename imports the data
        """

        self.version = 2

        if self.version == 2:
            self.version = 2
            self.fmr_file = ''
            self.prt_files = []
            self.number_volumes = 0
            self.voxel_resolution = 3
            self.xstart = 57
            self.xend = 231
            self.ystart = 52
            self.yend = 172
            self.zstart = 59
            self.zend = 197
            self.left_right=0
            self.tr = 0
            self.hemodynamic_delay = 0
            self.delta_parameter = 0
            self.tau_parameter = 0
            self.segment_size = 0
            self.segment_offset = 0
            # 4x4 transformation (does not include vol coordinate, only x,y,z)
            self.affine = np.zeros([4,4])
            # this is the matrix produced by the nifti conversion (includes vol coordinate)
            self.affine5 = np.zeros([5,5])
            self.data = [] # data is organized as [x,y,z,t]
        elif self.version == 3:
            self.version = 3
            self.fmr_file = ''
            self.number_protocols = 1
            self.prt_files = []
            self.current_prt = 0
            self.datatype = 0
            self.number_volumes = 0
            self.voxel_resolution = 3
            self.xstart = 57
            self.xend = 231
            self.ystart = 52
            self.yend = 172
            self.zstart = 59
            self.zend = 197
            self.left_right_convention = 0
            self.reference_space = 0
            self.tr = 0
            # 4x4 transformation (does not include vol coordinate, only x,y,z)
            self.affine = np.zeros([4, 4])
            # this is the matrix produced by the nifti conversion (includes vol coordinate)
            self.affine5 = np.zeros([5, 5])
            self.data = []  # data is organized as [x,y,z,t]
        else:
            pass

        # if no numpy filename passed, checks if exists one with same name and npy extension
        if not np_filename or not os.path.isfile(np_filename):            
            if os.path.isfile(vtc_filename + '.npy'):
                np_filename = vtc_filename + '.npy'

        if vtc_filename:
            self.import_vtc (vtc_filename, np_filename)

    def print_header(self):
        """
            Prints header information
        """

        if self.version == 2:
            print ('\n################################################')
            print ('Header info for file ', self.vtc_filename)
            print ('Version Number ', self.version)
            print ('FRM file ', self.frm_file)
            print ('Protocol Files ', self.prt_files)
            print ('Number of Volumes ', self.number_volumes)
            print ('Voxel Resolution ', self.voxel_resolution)
            print ('XStart ', self.xstart)
            print ('XEnd ', self.xend)
            print ('YStart ', self.ystart)
            print ('YEnd ', self.yend)
            print ('ZStart ', self.zstart)
            print ('ZEnd ', self.zend)
            print ('Left-Right Convention (1=radiological, 2=neurological, 0=unknown)', self.left_right_convention)
            print ('Hemodynamic delay, simple shift value ', self.hemodynamic_delay)
            print ('Hemodynamic function, delta parameter ', self.delta_parameter)
            print ('Hemodynamic function, tau parameter ', self.tau_parameter)
            print ('Segment size ', self.segment_size)
            print ('Segment offset ', self.segment_offset)
            print ('TR - Repetion Time ', self.tr)
            print ('Computed Affine Matrix4x4\n', self.affine)
            print ('Computed Affine Matrix5x5\n', self.affine5)
            print ('VTC data dimensions ', self.data.shape)
            print ('################################################\n')
        elif self.version == 3:
            print('\n################################################')
            print('Header info for file ', self.vtc_filename)
            print('Version Number ', self.version)
            print('FRM file ', self.frm_file)
            print('Number of Protocols ', self.number_protocols)
            print('Protocol Files ', self.prt_files)
            print('Current Protocol ', self.current_prt)
            print('Datatype of MTC data (1=short int, 2=float) ', self.datatype)
            print('Number of Volumes ', self.number_volumes)
            print('Voxel Resolution ', self.voxel_resolution)
            print('XStart ', self.xstart)
            print('XEnd ', self.xend)
            print('YStart ', self.ystart)
            print('YEnd ', self.yend)
            print('ZStart ', self.zstart)
            print('ZEnd ', self.zend)
            print('Left-Right Convention (1=radiological, 2=neurological, 0=unknown)', self.left_right_convention)
            print('Reference Space (1=native, 2=ACPC, 3=Talairach, 0=unknown)', self.reference_space)
            print('TR - Repetion Time ', self.tr)
            print('Computed Affine Matrix4x4\n', self.affine)
            print('Computed Affine Matrix5x5\n', self.affine5)
            print('VTC data dimensions ', self.data.shape)
            print('################################################\n')
        else:
            pass

    def load_np (self, filename):
        """
            Loads the VTC data from a numpy array directly
        """
        self.data = np.load(filename)

    def compute_affine_matrix (self):
        """
            Compute affine matrix from start and end coordinates
        """
        scale = self.voxel_resolution
        if self.left_right_convention == 1:
            scale = scale * -1
        self.affine[0,2] = scale
        self.affine[1,0] = scale
        self.affine[2,1] = scale
        self.affine[0,3] = 128-self.zstart
        self.affine[1,3] = 128-self.xstart
        self.affine[2,3] = 128-self.ystart
        self.affine[3,3] = 1.0

        self.affine5[0,2] = scale
        self.affine5[1,0] = scale
        self.affine5[2,1] = scale
        self.affine5[0,4] = 128-self.zstart
        self.affine5[1,4] = 128-self.xstart
        self.affine5[2,4] = 128-self.ystart
        self.affine5[3,3] = 1.0
        self.affine5[4,4] = 1.0

    def voxel2talairach (self, coords):
        """
            Transform from voxel space to Talairach space
        """
        #remember we inverted the y axis after loading the data
        coord4d = [coords[0], coords[1], coords[2], 1]
        return self.affine.dot( coord4d )

    def talairach2voxel (self, coords):
        """
            Transform from Talairach space to voxel space
        """
        #remember we inverted the y axis after loading the data
        tal4d = [coords[0], coords[1], coords[2], 1.0]
        voxel = np.linalg.inv(self.affine).dot(tal4d)
#        voxel[1] = self.vol_data.shape[1]-2-voxel[1]

        #print ('4dvoxel from tal %s' % (voxel))
#        return np.rint(voxel[0:3]).astype(int)
        return (voxel[0:3]).astype(int)


    def patch_mask (self, mask):
        """
            Given a mask, returns a new patch structure with voxels inside mask

            :param mask: Given binary mask
            :returns : Voxels time course inside mask
        """
        vtc_patch = []

        for z in range(mask.shape[2]):
            for y in range(mask.shape[1]):
                for x in range(mask.shape[0]):
                    if mask[x,y,z] > 0.0:
                        vtc_patch.append (self.data[x,y,z,:])

        return np.asarray(vtc_patch)


    def toNIfTI (self):
        """
            Returns a NIfTI structure as defined in nipy
        """
        nifti_image = Image(self.data, vox2mni(np.eye(5)))
        nifti_image.affine = self.affine5
        return nifti_image


    def import_vtc (self, vtc_filename, np_filename=''):
        """
            Imports VTC file (Volume Time Course)

            :param vtc_filename: name (with path) of VTC file
            :param np_filename: name (with path) of optional numpy file with data (in this case does not import the data, only header info)
        """

        self.vtc_filename = vtc_filename
        with open(vtc_filename, "rb") as fin:

            self.version = struct.unpack('H', fin.read(2))[0]

            self.frm_file = ''
            byte = fin.read(1)
            while byte != b'\x00':
                self.frm_file = self.frm_file + byte.decode()
                byte = fin.read(1)

            if self.version == 2:

                self.number_protocols = 1
                for i in range(self.number_protocols):
                    prt_file = ''
                    byte = fin.read(1)
                    while byte != b'\x00':
                        prt_file = prt_file + byte.decode()
                        byte = fin.read(1)
                    self.prt_files.append(prt_file)

                self.number_volumes = struct.unpack('H', fin.read(2))[0]
                self.voxel_resolution = struct.unpack('H', fin.read(2))[0]
                #print(1+self.voxel_resolution)

                self.datatype = 1

                self.xstart = struct.unpack('H', fin.read(2))[0]
                self.xend = struct.unpack('H', fin.read(2))[0]
                self.ystart = struct.unpack('H', fin.read(2))[0]
                self.yend = struct.unpack('H', fin.read(2))[0]
                self.zstart = struct.unpack('H', fin.read(2))[0]
                
                self.zend = struct.unpack('H', fin.read(2))[0]

                self.left_right_convention = 1#ord(fin.read(1))

                self.hemodynamic_delay = struct.unpack('H', fin.read(2))[0]
                self.tr = struct.unpack('f', fin.read(4))[0]
                self.delta_parameter = struct.unpack('f', fin.read(4))[0]
                self.tau_parameter = struct.unpack('f', fin.read(4))[0]
                self.segment_size = struct.unpack('H', fin.read(2))[0]
                self.segment_offset = struct.unpack('H', fin.read(2))[0]
                
                self.compute_affine_matrix()
            elif self.version == 3:
                self.number_protocols = struct.unpack('H', fin.read(2))[0]

                for i in range(self.number_protocols):
                    prt_file = ''
                    byte = fin.read(1)
                    while byte != b'\x00':
                        prt_file = prt_file + byte.decode()
                        byte = fin.read(1)
                    self.prt_files.append(prt_file)

                self.current_protocol = struct.unpack('H', fin.read(2))[0]
                self.datatype = struct.unpack('H', fin.read(2))[0]
                self.number_volumes = struct.unpack('H', fin.read(2))[0]
                self.voxel_resolution = struct.unpack('H', fin.read(2))[0]
                self.xstart = struct.unpack('H', fin.read(2))[0]
                self.xend = struct.unpack('H', fin.read(2))[0]
                self.ystart = struct.unpack('H', fin.read(2))[0]
                self.yend = struct.unpack('H', fin.read(2))[0]
                self.zstart = struct.unpack('H', fin.read(2))[0]
                self.zend = struct.unpack('H', fin.read(2))[0]
                self.left_right_convention = ord(fin.read(1))
                self.reference_space = ord(fin.read(1))
                self.tr = struct.unpack('f', fin.read(4))[0]
                self.compute_affine_matrix()
            else:
                pass

            dimensions = [int((self.xend-self.xstart)/self.voxel_resolution),
                            int((self.yend-self.ystart)/self.voxel_resolution),
                            int((self.zend-self.zstart)/self.voxel_resolution),
                            self.number_volumes]

            print( dimensions )

            if np_filename:
                self.load_np (np_filename)
            else:
                self.data = np.zeros(dimensions)
                for z in range(dimensions[2]):
                    for y in range(dimensions[1]):
                        for x in range(dimensions[0]):
                            for t in range(dimensions[3]):
                                if self.datatype == 1:
                                    value = struct.unpack('H', fin.read(2))[0]
                                elif self.datatype == 2:
                                    value = struct.unpack('f', fin.read(4))[0]
                                self.data[x,y,z,t] = value

                np.save(vtc_filename, self.data)
                        

