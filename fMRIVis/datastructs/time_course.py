#
# time_course.py
#
# Time course of a voxel or surface vertex
# included matplotlib methods to render time course
#
# Created by Ricardo Marroquim on 14-02-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import sys
import matplotlib as mpl
from matplotlib.widgets import RadioButtons
from scipy import stats
import numpy as np

sys.path.append('../')
from fMRIVis.datastructs import protocol


class TimeCourse():

    def __init__ (self):
        """
            Initializes the time course
        """
        self.data = []
        self.zscore = []

    def load_time_course (self, d):
        """
            Load time course data from numpy array
        """
        self.data = d
        self.zscore = stats.zscore(d)


class TimeCoursePlot():
    """
        Methods for plotting a time course using matplotlib
    """

    def __init__(self):
        """
        """
        self.tc = TimeCourse()
        self.protocol = protocol.Protocol()

    def set_time_course (self, tc):
        """
            Sets the time course data for plotting
        """
        self.tc = tc

    def set_protocol (self, prt):
        """
            Sets the associate protocol for the time course
        """
        self.protocol = prt

    def init_axes (self, iax, cax, bax):
        """
            Initializes axes for plotting time course and radio button
            If there is an associated protocol, create colobar from conditions
        """

        # voxel time course inspection window
        self.inspection_ax = iax
        self.colorbar_ax = cax
        self.button_ax = bax

        # create colobar for labels based on protocol
        if self.protocol.number_conditions > 0:
            labels_colors = []
            labels_names = []
            # create lists with conditions colors and names
            for c in self.protocol.conditions:
                labels_colors.append(c.color)
                labels_names.append(c.name) 

            labels_cmap = mpl.colors.ListedColormap(labels_colors)
            bounds = list(np.arange(1, len(labels_colors)+2))
            norm = mpl.colors.BoundaryNorm(bounds, labels_cmap.N)
            labels_colorbar= mpl.colorbar.ColorbarBase(self.colorbar_ax, cmap=labels_cmap, norm=norm,
                      boundaries=bounds,
                      spacing='proportional',
                      orientation='vertical')
            self.colorbar_ax.yaxis.set_label_position('left')
            self.colorbar_ax.set_ylabel('conditions', rotation=90)
            self.colorbar_ax.yaxis.set_major_formatter(mpl.ticker.NullFormatter())

            # ticks positions for labels
            ticks = []
            for t in range(self.protocol.number_conditions):
                ticks.append (t*(1.0/self.protocol.number_conditions)+0.5/self.protocol.number_conditions
    )
            self.colorbar_ax.yaxis.set_major_locator(mpl.ticker.FixedLocator(ticks))

            # condition names
            self.colorbar_ax.yaxis.set_major_formatter(mpl.ticker.FixedFormatter(labels_names))

        # radio button to change scale and mode
        self.type_button = RadioButtons(self.button_ax, ('value', 'zscore'))
        self.type_button.activecolor = 'red'
        self.type_button.on_clicked(self.change_type)

    def change_type (self, connection_id):
        """
            Change time course visualization type
        """
        self.plot ()

    
    def plot (self, tc = []):
        """
            Plot time course for voxel/vertex
            Draw conditions on plot
        """
        if tc:
            self.tc = tc

        self.inspection_ax.clear()

        self.inspection_ax.set_facecolor(self.protocol.bg_color)

        thick = self.protocol.time_course_thick
        color = self.protocol.time_course_color
        if self.type_button.value_selected == 'value':
            self.inspection_ax.plot(self.tc.data, linewidth=thick, color=color, zorder=2)
        elif self.type_button.value_selected == 'zscore':
            self.inspection_ax.plot(self.tc.zscore, linewidth=thick, color=color, zorder=2)
#        elif self.type_button.value_selected == 'HRF':
#            self.inspection_ax.plot(mri_slices.mri_data[coords[0], coords[1], coords[2],:])

        self.plot_conditions()

        self.inspection_ax.figure.canvas.draw()


    def plot_conditions (self):
        """
            Plot condition (label) stripes
        """
        alpha = 1.0
        #for each condition
        for condition in self.protocol.conditions:
            #plots its intervals with given color
            for interval in condition.intervals:
                self.inspection_ax.axvspan(interval[0]-1, interval[1], alpha=alpha, color=condition.color, zorder=1)

