#
# smp.py
#
# Surface Map
#
# Created by Ricardo Marroquim on 16-02-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import numpy as np
from nipy.core.api import Image, vox2mni
import struct
import copy

class SurfaceMap:
    """
        A Surface Map containing statistical results per vertex
    """

    def __init__ (self):
        """
            Inits the surface map class
        """
        self.map_type = 0
        self.number_lags = 0
        self.min_lag = 0
        self.max_lag = 0
        self.cc_overlay = 0
        self.cluster_size = 0
        self.enable_cluster_check = 1
        self.use_values_above = 0
        self.stat_critical_threshold = 0.0
        self.stat_max_threshold = 0.0
        self.dof_f_test_nominator = 0
        self.dof_f_test_denominator = 0
        self.bonferroni_correction = 0
        self.show_positive_negative_flag = 0
        self.critical_value_color_pos = [0, 0, 0]
        self.max_value_color_pos = [0, 0, 0]
        self.critical_value_color_neg = [0, 0, 0]
        self.max_value_color_neg = [0, 0, 0]
        self.enable_smp_color = 0
        self.map_alpha = 0.0
        self.name = ''
        self.data = []

    def print_header (self):
        """
            Prints header information
        """
        print ('Map Name ', self.name)
        print ('Map Type ', self.map_type)
        print ('Number of Lags ', self.number_lags)
        print ('Min Lag ', self.min_lag)
        print ('Max Lag ', self.max_lag)
        print ('CCOverlay ', self.cc_overlay)
        print ('Cluster Size ', self.cluster_size)
        print ('Enable Cluster Check ', self.enable_cluster_check)
        print ('Statistical Threshold, Critical Value ', self.stat_critical_threshold)
        print ('Statistical Threshold, Max Value ', self.stat_max_threshold)
        print ('DOF nominator F-test ', self.dof_f_test_nominator)
        print ('DOF denominator F-test ', self.dof_f_test_denominator)
        print ('Cortex-based Bonferroni Correction Value ', self.bonferroni_correction)
        print ('Critical Value Color Positive ', self.critical_value_color_pos)
        print ('Critical Value Color Negative ', self.critical_value_color_neg)
        print ('Max Value Color Positive ', self.max_value_color_pos)
        print ('Max Value Color Negative ', self.max_value_color_neg)
        print ('Enable SMP Specific Color ', self.enable_smp_color)
        print ('Transparent Color Factor ', self.map_alpha)

class SMP:
    """
        Surface Map data imported from Brainvoyager
    """

    def __init__ (self, smp_filename='', np_filename=''):
        """
            Inits the smp class, if given filename imports the data
        """
        self.version = 3
        self.number_verts = 0
        self.number_maps = 0
        self.srf_file = ''
        self.maps = []

        if smp_filename:
            self.import_smp (smp_filename, np_filename)

    def print_header(self):
        """
            Prints header information
        """

        print ('\n################################################')
        print ('Header info for file ', self.smp_filename)
        print ('Version Number ', self.version)
        print ('Number of Verts ', self.number_verts)
        print ('Number of Maps ', self.number_maps)
        print ('SRF file ', self.srf_file)

        cnt = 1
        for m in self.maps:
            print ('\nMap ', cnt)
            m.print_header()
            cnt += 1

        print ('################################################\n')

    def patch (self, poi):
        """
            Given a Patch of Interest, returns MTC with vertices inside POI
            :param poi: Patch of Interest
            :returns : Mesh Time Course inside poi
        """
        smp_patch = copy.deepcopy(self)
        smp_patch.num_verts = poi.num_verts

        # for each map, creates a reduced list with only POI's verts
        for m in smp_patch.maps:
            d = []
            for v in poi.verts:
                d.append( m.data[v] )
            m.data = d
            m.data = np.asarray(m.data)

        return smp_patch


    def load_np (self, filename):
        """
            Loads the MTC data from a numpy array directly
        """
        self.data = np.load(filename)


    def import_smp (self, smp_filename, np_filename=''):
        """
            Imports SMP file (Surface Maps)
            :param smp_filename: name (with path) of SMP file
            :param np_filename: name (with path) of optional numpy file with data per map (in this case does not import the map data, only header info)            
        """

        self.smp_filename = smp_filename
        with open(smp_filename, "rb") as fin:

            self.version = struct.unpack('H', fin.read(2))[0]

            self.number_verts = struct.unpack('i', fin.read(4))[0]
            self.number_maps = struct.unpack('H', fin.read(2))[0]

            self.srf_file = ''
            byte = fin.read(1)
            while byte != b'\x00':
                self.srf_file = self.srf_file + byte.decode()
                byte = fin.read(1)

            self.maps = []
            for i in range(self.number_maps):
                smap = SurfaceMap()
                smap.map_type = struct.unpack('i', fin.read(4))[0]
                smap.number_lags = struct.unpack('i', fin.read(4))[0]
                smap.min_lags = struct.unpack('i', fin.read(4))[0]
                smap.max_lags = struct.unpack('i', fin.read(4))[0]
                smap.cc_overlay = struct.unpack('i', fin.read(4))[0]
                smap.cluster_size = struct.unpack('i', fin.read(4))[0]
                smap.enable_cluster_check = ord(fin.read(1))
                smap.stat_critical_threshold = struct.unpack('f', fin.read(4))[0]
                smap.stat_max_threshold = struct.unpack('f', fin.read(4))[0]
                smap.use_values_above = ord(fin.read(1)) 
                smap.dof_f_test_nominator = struct.unpack('i', fin.read(4))[0]
                smap.dof_f_test_denominator = struct.unpack('i', fin.read(4))[0]
                smap.show_positive_negative_flag = ord(fin.read(1))
                smap.bonferroni_correction = struct.unpack('i', fin.read(4))[0]
                smap.critical_value_color_pos[0] = ord(fin.read(1))/255.0
                smap.critical_value_color_pos[1] = ord(fin.read(1))/255.0
                smap.critical_value_color_pos[2] = ord(fin.read(1))/255.0
                smap.max_value_color_pos[0] = ord(fin.read(1))/255.0
                smap.max_value_color_pos[1] = ord(fin.read(1))/255.0
                smap.max_value_color_pos[2] = ord(fin.read(1))/255.0
                smap.critical_value_color_neg[0] = ord(fin.read(1))/255.0
                smap.critical_value_color_neg[1] = ord(fin.read(1))/255.0
                smap.critical_value_color_neg[2] = ord(fin.read(1))/255.0
                smap.max_value_color_neg[0] = ord(fin.read(1))/255.0
                smap.max_value_color_neg[1] = ord(fin.read(1))/255.0
                smap.max_value_color_neg[2] = ord(fin.read(1))/255.0
                smap.enable_smp_color = ord(fin.read(1))
                smap.map_alpha = struct.unpack('f', fin.read(4))[0]
                
                byte = fin.read(1)
                while byte != b'\x00':
                    smap.name = smap.name + byte.decode()
                    byte = fin.read(1)

                for i in range(self.number_verts):
                    smap.data.append( struct.unpack('f', fin.read(4))[0] )

                self.maps.append(smap)


