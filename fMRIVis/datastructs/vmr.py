#
# vmr.py
#
# Volume High Resolution Anatomic Data
#
# Created by Ricardo Marroquim on 03-05-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import numpy as np
from nipy.core.api import Image, vox2mni
import struct
import os.path

class SpatialTransformation:
    """
        Spatial transformations for VMR
    """

    def __init__ (self):
        """
            Inits the Spatial Transform class
        """

        self.type = 0 # 1-rigid :: 2-affine :: 3-Talairach :: 5-Un-Talairach
        self.description = ''
        self.filename = ''
        self.number_values = 0
        self.values = []

class VMR:
    """
        Volume HighRes Data imported from Brainvoyager
    """

    def __init__(self, vmr_filename='', np_filename=''):
        """
            Inits the VMRC class, if given filename imports the data
        """
        self.version = 4
        self.vmr_filename = ''
        self.dim_x = 0
        self.dim_y = 0
        self.dim_z = 0
        self.x_offset = 0
        self.y_offset = 0
        self.z_offset = 0
        self.framing_cube_dim = 256
        self.pos_infos_verified = 0
        self.coordinate_system = 0 # 1 = DICOM
        self.x_center_first_slice = 0
        self.y_center_first_slice = 0
        self.z_center_first_slice = 0
        self.x_center_last_slice = 0
        self.y_center_last_slice = 0
        self.z_center_last_slice = 0
        self.slice_row_dir_x = 0
        self.slice_row_dir_y = 0
        self.slice_row_dir_z = 0
        self.slice_col_dir_x = 0
        self.slice_col_dir_y = 0
        self.slice_col_dir_z = 0
        self.number_rows_img_matrix = 0
        self.number_cols_img_matrix = 0
        self.fov_rows = 0
        self.fov_cols = 0
        self.slice_thickness = 0
        self.gap_thickness = 0
        self.number_spatial_transformations = 0
        self.past_transformations = []
        self.left_right_convention = 0
        self.ref_space_flag = 0
        self.voxel_res_x = 0
        self.voxel_res_y = 0
        self.voxel_res_z = 0
        self.flag_voxel_res_verified = 0
        self.flag_talairach_space = 0
        #self.min_intensity = 0
        #self.mean_intensity = 0
        #self.max_intensity = 0
        self.data = []

        # if no numpy filename passed, checks if exists one with same name and npy extension
        if not np_filename or not os.path.isfile(np_filename):            
            if os.path.isfile(vmr_filename + '.npy'):
                np_filename = vmr_filename + '.npy'

        if vmr_filename:
            self.import_vmr (vmr_filename, np_filename)

    def talairach2voxel (self, coords):
        """
            Transform from Talairach space to voxel space
        """
        voxel = [128 - coords[0], 128 - coords[1], 128 - coords[2]]
        voxel = np.asarray(voxel)
        return voxel.astype(int)



    def print_header(self):
        """
            Prints header information
        """

        print ('\n################################################')
        print ('Header info for file ', self.vmr_filename)
        print ('Version Number ', self.version)
        print ('DimX ', self.dim_x)
        print ('DimY ', self.dim_y)
        print ('DimZ ', self.dim_z)
        print ('X offset ', self.x_offset)
        print ('Y offset ', self.y_offset)
        print ('Z offset ', self.z_offset)
        print ('Framing Cube dimensions ', self.framing_cube_dim)
        print ('Pos Infos Verified ', self.pos_infos_verified)
        print ('Coordinate system ', self.coordinate_system)
        print ('X center first slice ', self.x_center_first_slice)
        print ('Y center first slice ', self.y_center_first_slice)
        print ('Z center first slice ', self.z_center_first_slice)
        print ('X center last slice ', self.x_center_last_slice)
        print ('Y center last slice ', self.y_center_last_slice)
        print ('Z center last slice ', self.z_center_last_slice)
        print ('X slice col dir ', self.slice_col_dir_x)
        print ('Y slice col dir ', self.slice_col_dir_y)
        print ('Z slice col dir ', self.slice_col_dir_z)
        print ('Number rows slice image matrix ', self.number_rows_img_matrix)
        print ('Number cols slice image matrix ', self.number_cols_img_matrix)
        print ('FOV rows direction ', self.fov_rows)
        print ('FOV cols direction ', self.fov_cols)
        print ('Slice thickness ', self.slice_thickness)
        print ('Gap thickness ', self.gap_thickness)
        print ('Number past spatial transformations ', self.number_spatial_transformations)

        for i, tr in enumerate(self.past_transformations):
            print ('Past transformation ', i)
            print ('  Type ', tr.type)
            print ('  Description ', tr.description)
            print ('  Filename ', tr.filename)
            print ('  Number values ', tr.number_values)
            print ('  Values ', tr.values)

        print ('Left-Right convention ', self.left_right_convention)
        print ('Reference space flag ', self.ref_space_flag)
        print ('Voxel resolution X ', self.voxel_res_x)
        print ('Voxel resolution Y ', self.voxel_res_y)
        print ('Voxel resolution Z ', self.voxel_res_z)
        print ('Flag voxel resolution verified ', self.flag_voxel_res_verified)
        print ('Flag Talairach space ', self.flag_talairach_space)
        #print ('Min intensity in original 16-bit data ', self.min_intensity)
        #print ('Mean intensity in original 16-bit data ', self.mean_intensity)
        #print ('Max intensity in original 16-bit data ', self.max_intensity)
 
        print ('################################################\n')

    def load_np (self, filename):
        """
            Loads the VMR data from a numpy array directly
        """
        self.data = np.load(filename)


    def import_vmr (self, vmr_filename, np_filename=''):
        """
            Imports VMR file (Volume High Res Data)

            :param vmr_filename: name (with path) of VMR file
            :param np_filename: name (with path) of optional numpy file with data (in this case does not import the data, only header info)
        """

        self.vmr_filename = vmr_filename
        with open(vmr_filename, "rb") as fin:

            self.version = struct.unpack('H', fin.read(2))[0]
            self.dim_x = struct.unpack('H', fin.read(2))[0]
            self.dim_y = struct.unpack('H', fin.read(2))[0]
            self.dim_z = struct.unpack('H', fin.read(2))[0]

            if np_filename:
                self.load_np (np_filename)
                fin.read(self.dim_z*self.dim_y*self.dim_x)
            else:
                self.data = np.zeros((self.dim_x, self.dim_y, self.dim_z))
                for z in range(self.dim_z):
                    for y in range(self.dim_y):
                        for x in range(self.dim_x):
                            value = ord(fin.read(1))
                            self.data[x,y,z,] = value

                np.save(vmr_filename, self.data)
                        

            self.x_offset = struct.unpack('h', fin.read(2))[0]
            self.y_offset = struct.unpack('h', fin.read(2))[0]
            self.z_offset = struct.unpack('h', fin.read(2))[0]
            self.framing_cube_dim = struct.unpack('h', fin.read(2))[0]
            self.pos_infos_verified = struct.unpack('i', fin.read(4))[0]
            self.coordinate_system = struct.unpack('i', fin.read(4))[0]
            self.x_center_first_slice = struct.unpack('f', fin.read(4))[0]
            self.y_center_first_slice = struct.unpack('f', fin.read(4))[0]
            self.z_center_first_slice = struct.unpack('f', fin.read(4))[0]
            self.x_center_last_slice = struct.unpack('f', fin.read(4))[0]
            self.y_center_last_slice = struct.unpack('f', fin.read(4))[0]
            self.z_center_last_slice = struct.unpack('f', fin.read(4))[0]
            self.slice_row_dir_x = struct.unpack('f', fin.read(4))[0]
            self.slice_row_dir_y = struct.unpack('f', fin.read(4))[0]
            self.slice_row_dir_z = struct.unpack('f', fin.read(4))[0]
            self.slice_col_dir_x = struct.unpack('f', fin.read(4))[0]
            self.slice_col_dir_y = struct.unpack('f', fin.read(4))[0]
            self.slice_col_dir_z = struct.unpack('f', fin.read(4))[0]
            self.number_rows_img_matrix = struct.unpack('i', fin.read(4))[0]
            self.number_cols_img_matrix = struct.unpack('i', fin.read(4))[0]
            self.fov_rows = struct.unpack('f', fin.read(4))[0]
            self.fov_cols = struct.unpack('f', fin.read(4))[0]
            self.slice_thickness = struct.unpack('f', fin.read(4))[0]
            self.gap_thickness = struct.unpack('f', fin.read(4))[0]
            self.number_spatial_transformations = struct.unpack('i', fin.read(4))[0]

            for tr in range(self.number_spatial_transformations):
                transform = SpatialTransformation()
                transform.description = ''
                byte = struct.unpack('c', fin.read(1))[0]
                while byte != b'\x00':
                    transform.description = transform.description + byte.decode()
                    byte = struct.unpack('c', fin.read(1))[0]
     
                transform.type = struct.unpack('i', fin.read(4))[0]

                transform.filename = ''
                byte = struct.unpack('c', fin.read(1))[0]
                while byte != b'\x00':
                    transform.filename = transform.filename + byte.decode()
                    byte = struct.unpack('c', fin.read(1))[0]
 
                transform.number_values = struct.unpack('i', fin.read(4))[0]

                for v in range(transform.number_values):
                    transform.values.append ( struct.unpack('f', fin.read(4))[0] )
                self.past_transformations.append(transform)


            self.left_right_convetion = struct.unpack('b', fin.read(1))[0]
            self.ref_space_flag = struct.unpack('b', fin.read(1))[0]
            self.voxel_res_x = struct.unpack('f', fin.read(4))[0]
            self.voxel_res_y = struct.unpack('f', fin.read(4))[0]
            self.voxel_res_z = struct.unpack('f', fin.read(4))[0]
            self.flag_voxel_res_verified = struct.unpack('b', fin.read(1))[0]
            self.flag_talairach_space = struct.unpack('b', fin.read(1))[0]
            
            #self.min_intensity = struct.unpack('i', fin.read(4))[0]
            #self.mean_intensity = struct.unpack('i', fin.read(4))[0]
            #self.max_intensity = struct.unpack('i', fin.read(4))[0]

 

