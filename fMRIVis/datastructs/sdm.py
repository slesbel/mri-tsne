#
# sdm.py
#
# Design Matrix for GLM
#
# Created by Ricardo Marroquim on 04-05-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import matplotlib as mpl
import numpy as np


class SDM():
    """
        A Design Matrix for Time Courses
    """

    def __init__ (self, filename=''):
        """
            Initializes SDM class
        """
        self.version = 0
        self.sdm_filename = ''
        self.number_predictors = 0
        self.number_data_points = 0
        self.includes_constant = 0
        self.first_confound_predictor = 1
        self.predictors_colors = []
        self.predictors_names = []
        self.data = []

        if filename:
            self.import_sdm (filename)


    def print_header (self):
        """
            Print SDM info
        """
        print ('\n################################################')
        print ('Header info for file ', self.sdm_filename)
        print ('Version Number ', self.version)
        print ('Number predictors ', self.number_predictors)
        print ('Number data points ', self.number_data_points)
        print ('Includes constant ', self.includes_constant)
        print ('First confound predictor ', self.first_confound_predictor)
        print ('Predictors colors ', self.predictors_colors)
        print ('Predictors names ', self.predictors_names)
        print ('Matrix shape ', self.data.shape)
        print (self.data)
        print ('################################################\n')



    def read_sdm_header (self, line_list):
        """
            read a header value from SDM file represented as a line list
            
        """
        # searches for first non empty line
        line = line_list.pop(0)
        while not line:
            line = line_list.pop(0)
        # splits line where there are spaces
        line = line.split()
        # removes header
        header = line.pop(0)
        # returns everything else but header
        return line



    def read_sdm_values (self, line_list):
        """
            reads the values from the SDM matrix
            
        """
        # searches for first non empty line
        line = line_list.pop(0)
        while not line:
            line = line_list.pop(0)

        # splits line where there are spaces
        line = line.split()
        # returns everything else but header
        return line

     
    def import_sdm (self, filename):
        """
            Imports SDM file (Design Matrix)

            :param filename: name (with path) of SDM file
        """

        self.sdm_filename = filename
        with open(filename, "r") as fin:
            line_list = fin.read().splitlines() 
            #line_list = list(filter(None, line_list))

            self.version = int(self.read_sdm_header(line_list)[0])
            self.number_predictors = int(self.read_sdm_header(line_list)[0])
            self.number_data_points = int(self.read_sdm_header(line_list)[0])
            self.includes_constant = int(self.read_sdm_header(line_list)[0])
            self.first_confound_predictor = int(self.read_sdm_header(line_list)[0])

            
            self.predictors_colors = list(map(int, self.read_sdm_header(line_list)))
            self.predictors_names = line_list.pop(0)
            self.predictors_names = self.predictors_names.split('"')[1::2]

            for l in range(self.number_data_points):
                self.data.append ( list(map(float, self.read_sdm_values(line_list)) ))

            self.data = np.asarray(self.data)


    def export_sdm (self, out_filename):
        """
            Exports SDM file (Contrasts)

            :param filename: name (with path) of SDM file
        """

        with open(out_filename, "w") as fout:

            fout.write ('\n')
            fout.write ('NumberContrasts:    %s' % self.number_contrasts)

            for c in self.conditions:
                fout.write ('\n\n')
                fout.write ('%s\n' % c.name)
                fout.write ('%d\n' % c.number_values)
                for d in c.data:
                    fout.write (' %d' % d)

