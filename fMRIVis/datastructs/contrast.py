#
# contrast.py
#
# Contrast for GLM
#
# Created by Ricardo Marroquim on 26-04-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import matplotlib as mpl
import numpy as np

class Contrast():
    """
        A GLM contrast
    """

    def __init__ (self, name='', data= []):
        """
            Initializes Contrast
        """
        self.name = name
        self.number_values = len(data)
        self.data = data
        self.data = np.asarray(self.data)

    def print_contrast (self):
        """
            Prints the contrast info
        """
        print ('Name:', self.name)
        print ('Number values:', self.number_values)
        print ('Values:', self.data)

class ContrastList():
    """
        A list of contrasts for a GLM session
    """

    def __init__ (self, filename=''):
        """
            Initializes ContrastList class
        """
        self.number_contrasts = 0
        self.contrasts = []

        if filename:
            self.import_ctr (filename)


    def print_contrasts (self):
        """
            Print condition info
        """
        for c in self.contrasts:
            c.print_contrast()
            print ('\n')

    def read_ctr_header (self, line_list):
        """
            read a header value from CTR file represented as a line list
            
        """
        # searches for first non empty line
        line = line_list.pop(0)
        while not line:
            line = line_list.pop(0)
        # splits line where there are spaces
        line = line.split()
        # removes header
        header = line.pop(0)
        # returns everything else but header
        return line



    def read_ctr_contrast (self, line_list):
        """
            read a contrast from CTR file
            
        """
        # searches for first non empty line
        line = line_list.pop(0)
        while not line:
            line = line_list.pop(0)
 
        name = line
        number_values = int(line_list.pop(0))
        values = line_list.pop(0).split()
        data = []
        for v in values:        
            data.append( float(v) )

        contrast = Contrast(name, data)
        return contrast
     
    def import_ctr (self, filename):
        """
            Imports CTR file (Contrasts)

            :param filename: name (with path) of CTR file
        """

        self.filename = filename
        with open(filename, "r") as fin:
            line_list = fin.read().splitlines() 
            #line_list = list(filter(None, line_list))

            self.number_contrasts = int(self.read_ctr_header(line_list)[0])

            self.contrasts = []
            for c in range(self.number_contrasts):
                contrast = self.read_ctr_contrast(line_list)
                self.contrasts.append(contrast)


    def export_ctr (self, out_filename):
        """
            Exports CTR file (Contrasts)

            :param filename: name (with path) of CTR file
        """

        with open(out_filename, "w") as fout:

            fout.write ('\n')
            fout.write ('NumberContrasts:    %s' % self.number_contrasts)

            for c in self.conditions:
                fout.write ('\n\n')
                fout.write ('%s\n' % c.name)
                fout.write ('%d\n' % c.number_values)
                for d in c.data:
                    fout.write (' %d' % d)

