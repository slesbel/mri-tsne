#
# ssm.py
#
# Sphere to Sphere Mapping
#
# Created by Ricardo Marroquim on 14-03-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import numpy as np
import struct
import copy
import sys

class SSM:
    """
        Surface to Surface Mapping imported from Brainvoyager
    """

    def __init__(self, ssm_filename=''):
        """
            Inits the SSM class, if given filename imports the data
        """
        self.version = 0
        self.number_target_verts = 0
        self.number_source_verts = 0
        self.map = []

        if ssm_filename:
            self.import_ssm (ssm_filename)

    def print_header(self):
        """
            Prints header information
        """

        print ('\n################################################')
        print ('header info for file ', self.ssm_filename)
        print ('Version number ', self.version)
        print ('Number of Target Vertices ', self.number_target_verts)
        print ('Number of Source Vertices ', self.number_source_verts)
        print ('Map shape', self.map.shape)
        print ('################################################\n')

    def import_ssm (self, ssm_filename):
        """
            Imports SSM file
            :param ssm_filename: name (with path) of SSM file
        """

        self.ssm_filename = ssm_filename
        with open(ssm_filename, "rb") as fin:

            self.version = struct.unpack('H', fin.read(2))[0]
            self.number_target_verts = struct.unpack('i', fin.read(4))[0]
            self.number_source_verts = struct.unpack('i', fin.read(4))[0]

            self.map = np.zeros(self.number_target_verts)
            for i in range(self.number_target_verts):
                self.map[i] = struct.unpack('i', fin.read(4))[0]

