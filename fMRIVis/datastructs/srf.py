#
# srf.py
#
# Surface representation from Brainvoyager (SRF file)
# Contains the actual geometrical representation of the surface (vertices, normals and triangles)
#
# Created by Ricardo Marroquim on 22-02-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import numpy as np
from nipy.core.api import Image, vox2mni
import struct
import copy
import sys
from tqdm import tqdm, trange

class SRF:
    """
        Surface data imported from Brainvoyager
    """

    def __init__(self, srf_filename=''):
        """
            Inits the SRF class, if given filename imports the data
        """
        self.version = 0
        self.reserved = 0
        self.number_verts = 0
        self.number_triangles = 0
        self.mesh_center = [128.0, 128.0, 128.0]
        self.verts = []
        self.normals = []
        self.convex_curvature_color = [0.322, 0.733, 0.980, 1.000]
        self.concave_curvature_color = [0.100, 0.240, 0.320, 1.000]
        self.color_indices = []
        self.nearest_neighbors = []
        self.triangles = []
        self.number_strip_elements = 0
        self.strip_elements = []
        self.srf_filename = ''
        self.mtc_file = ''

        if srf_filename:
            self.import_srf (srf_filename)

    def get_vertex (self, index):
        p = np.zeros(3)
        for i in range(3):
            p[i] = self.verts[int(index)*3+i]
        return p

    def set_vertex (self, index, p):
        for i in range(3):
            self.verts[int(index)*3+i] = p[i]

    def compute_normalization (self):
        """
        Computes the center of the model, and normalization scale
        Center is the center of bounding box of model
        Scale is transformation to put vertices in range [0,1]
        """
        center = [0, 0, 0, 1.0]
        max_dim = [sys.float_info.min, sys.float_info.min, sys.float_info.min]
        min_dim = [sys.float_info.max, sys.float_info.max, sys.float_info.max]
        max_dim = np.array(max_dim)
        min_dim = np.array(min_dim)
        for v in range(self.number_verts):
            for i in range(3):
                max_dim[i] = max(max_dim[i], self.verts[v*3+i])
                min_dim[i] = min(min_dim[i], self.verts[v*3+i])

        # center of bounding box
        center = (max_dim + min_dim)/2.0
        center = np.append(center, 1.0)
    
        # compute normalization scale
        radius = 0.0
        for v in range(self.number_verts):
            vert = self.verts[v*3:v*3+3] - center[:3]
            radius = max (radius, np.linalg.norm(vert))
        radius = 1.0/radius

        return center, radius

    def patch(self, poi):
        """
            Given a Patch of Interest, returns SRF with vertices inside POI
            :param poi: Patch of Interest
            :returns : Surface patch inside poi
        """
        srf_patch = copy.deepcopy(self)
        srf_patch.number_verts = poi.num_verts
        srf_patch.number_triangles = 0
        srf_patch.verts = []
        srf_patch.normals = []
        srf_patch.triangles = []
        srf_patch.nearest_neighbors = []
        srf_patch.number_strip_elements = 0
        srf_patch.strip_elements = []
        srf_patch.color_indices = [0] * poi.num_verts

        for v in poi.verts:
            srf_patch.verts.append( self.verts[v*3+0] )
            srf_patch.verts.append( self.verts[v*3+1] )
            srf_patch.verts.append( self.verts[v*3+2] )
            srf_patch.normals.append( self.normals[v*3+0] )
            srf_patch.normals.append( self.normals[v*3+1] )
            srf_patch.normals.append( self.normals[v*3+2] )

        # include only triangles that have all 3 vertices in poi
        ids = poi.verts.tolist()

        for t in trange(self.number_triangles):
            tris = [self.triangles[t*3+0], self.triangles[t*3+1], self.triangles[t*3+2]]
            if np.all(np.in1d(tris, poi.verts)):
                for i in tris:
                    srf_patch.triangles.append( ids.index(i) )
                srf_patch.number_triangles += 1


        # nearest neighbors
        for v in poi.verts:
            neighbors = []
            for n in self.nearest_neighbors[v]:
                if np.in1d(n, poi.verts):
                    neighbors.append( ids.index(n) )
            srf_patch.nearest_neighbors.append(neighbors)

        return srf_patch

 
    def print_header(self):
        """
            Prints header information
        """

        print ('\n################################################')
        print ('header info for file ', self.srf_filename)
        print ('Version number ', self.version)
        print ('Number of Vertices ', self.number_verts)
        print ('Number of Triangles', self.number_triangles)
        print ('Mesh Center', self.mesh_center)
        print ('Convex Curvature Color', self.convex_curvature_color)
        print ('Concave Curvature Color', self.concave_curvature_color)
        print ('Number of Triangle Strip Elements', self.number_strip_elements)
        print ('Associated MTC filename', self.mtc_file)
        print ('################################################\n')


    def export_srf (self, out_filename):
        """
            Exports to SRF file
            :param out_filename: name (with path) of output SRF file
        """

        with open(out_filename, "wb") as fout:
            
            fout.write(struct.pack('f', self.version))
            fout.write(struct.pack('i', self.reserved))
            fout.write(struct.pack('i', self.number_verts))
            fout.write(struct.pack('i', self.number_triangles))
            fout.write(struct.pack('f', self.mesh_center[0]))
            fout.write(struct.pack('f', self.mesh_center[1]))
            fout.write(struct.pack('f', self.mesh_center[2]))

            # write vertices interleaved
            for i in range(3):
                for v in range(self.number_verts):
                    fout.write(struct.pack('f', self.verts[v*3+i]))

            # write normals interleaved
            for i in range(3):
                for v in range(self.number_verts):
                    fout.write(struct.pack('f', self.normals[v*3+i]*-1.0))

            fout.write(struct.pack('f', self.convex_curvature_color[0]))
            fout.write(struct.pack('f', self.convex_curvature_color[1]))
            fout.write(struct.pack('f', self.convex_curvature_color[2]))
            fout.write(struct.pack('f', self.convex_curvature_color[3]))
            fout.write(struct.pack('f', self.concave_curvature_color[0]))
            fout.write(struct.pack('f', self.concave_curvature_color[1]))
            fout.write(struct.pack('f', self.concave_curvature_color[2]))
            fout.write(struct.pack('f', self.concave_curvature_color[3]))

            # color indices
            for c in self.color_indices:
                fout.write(struct.pack('i', int(c)))

            # nearest neighbors
            for nn in self.nearest_neighbors:
                fout.write(struct.pack('i', int(len(nn))))
                for neighbor in nn:
                    fout.write(struct.pack('i', int(neighbor)))                

            # triangles
            for t in self.triangles:
                fout.write(struct.pack('i', int(t)))

            # triangle strips
            fout.write(struct.pack('i', int(self.number_strip_elements)))
            for s in self.strip_elements:
                fout.write(struct.pack('i', int(s)))

            #fout.write(chr(int(self.mtc_file, 16)))
            #file_out.write(chr(int(file_contents, 16))
            encoded = str.encode(self.mtc_file, 'utf-8')
            fout.write(encoded)
            fout.write(b'\x00')

        print ('exported', out_filename)

    def import_srf (self, srf_filename):
        """
            Imports SRF file (Surface geometry)
            :param srf_filename: name (with path) of SRF file
        """

        self.srf_filename = srf_filename
        with open(srf_filename, "rb") as fin:

            self.version = struct.unpack('f', fin.read(4))[0]
            self.reserved = struct.unpack('i', fin.read(4))[0] #reserved, must be '0'
            #print ('Reserved int = ', reserved)
            self.number_verts = struct.unpack('i', fin.read(4))[0]
            self.number_triangles = struct.unpack('i', fin.read(4))[0]
            self.mesh_center[0]= struct.unpack('f', fin.read(4))[0]
            self.mesh_center[1]= struct.unpack('f', fin.read(4))[0]
            self.mesh_center[2]= struct.unpack('f', fin.read(4))[0]

            self.verts = np.zeros(self.number_verts*3, 'f')
            # verts are interleaved in file, read first X coordinates of all vertices, and so on
            for i in range(3):
                for v in range(self.number_verts):
                    self.verts[v*3 + i] = struct.unpack('f', fin.read(4))[0]

            # Brainvoyager convetion is Radiological (so invert the x coordinate)
            # to use Neurological convention comment for below and invert front face orientation in surface viewer
            for v in range(self.number_verts):
                self.verts[v*3] *= -1.0

            self.normals = np.zeros(self.number_verts*3)
            for i in range(3):
                for v in range(self.number_verts):
                    # normals point inward, so multiply by -1 to point outward
                    self.normals[v*3 + i] = struct.unpack('f', fin.read(4))[0] * -1.0

            self.convex_curvature_color[0] = struct.unpack('f', fin.read(4))[0]
            self.convex_curvature_color[1] = struct.unpack('f', fin.read(4))[0]
            self.convex_curvature_color[2] = struct.unpack('f', fin.read(4))[0]
            self.convex_curvature_color[3] = struct.unpack('f', fin.read(4))[0]
            self.concave_curvature_color[0] = struct.unpack('f', fin.read(4))[0]
            self.concave_curvature_color[1] = struct.unpack('f', fin.read(4))[0]
            self.concave_curvature_color[2] = struct.unpack('f', fin.read(4))[0]
            self.concave_curvature_color[3] = struct.unpack('f', fin.read(4))[0]

            # color indices
            self.color_indices = np.zeros(self.number_verts)
            for v in range(self.number_verts):
                self.color_indices[v] = struct.unpack('i', fin.read(4))[0]

            # read nearest neighbors for each vertex
            for v in range(self.number_verts):
                num_neighbors = struct.unpack('i', fin.read(4))[0]
                nn = np.zeros(num_neighbors)
                for n in range(num_neighbors):
                    nn[n] = struct.unpack('i', fin.read(4))[0]
                self.nearest_neighbors.append(nn)

            # read triangles, 3 indices per triangles
            self.triangles = np.zeros(self.number_triangles*3, 'i')
            for t in range(self.number_triangles*3):
                self.triangles[t] = struct.unpack('i', fin.read(4))[0]

            # triangle strips
            self.number_strip_elements = struct.unpack('i', fin.read(4))[0]
            self.strip_elements = np.zeros(self.number_strip_elements)
            for s in range(self.number_strip_elements):
                self.strip_elements[s] = struct.unpack('i', fin.read(4))[0]

            self.mtc_file = ''
            byte = fin.read(1)
            while byte != b'\x00':
                self.mtc_file = self.mtc_file + byte.decode()
                byte = fin.read(1)
                       

