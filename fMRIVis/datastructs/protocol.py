#
# protocol.py
#
# fMRI Experiment protocol (conditions)
#
# Created by Ricardo Marroquim on 14-02-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import matplotlib as mpl
import numpy as np

class Block():
    """
        A protocol block (condition instance)
        Basically, a block corresponds to an interval of a condition
    """

    def __init__ (self, condition, start, end):
        """
            Initializes Block
        """
        self.condition = condition
        self.start = start
        self.end = end

class Condition():
    """
        Protocol condition for MRI experiment
    """

    def __init__ (self):
        """
            Initializes Condition class            
        """
        self.name = ''
        self.number_intervals = 0
        self.color = [100/255.0, 100/255.0, 100/255.0]
        self.intervals = []

    def print_condition (self):
        """
            Print condition info
        """
        print ('Name ', self.name)
        print ('Number of Intervals ', self.number_intervals)
        print ('Color ', self.color)
        print ('Intervals\n', self.intervals)

class Protocol():
    """
        Protocol information about an experiment
        Imports data from Brainvoyager prt file
    """

    def __init__ (self, filename=''):
        """
            Initializes Protocol class
            where appropriate attributes default values for PRT file format
        """

        self.filename = ''
        self.file_version = 0
        self.resolution_of_time = 0
        self.experiment = ''
        self.bg_color = [0, 0, 0]
        self.text_color = [255/255.0, 255/255.0, 217/255.0]
        self.time_course_color = [255/255.0, 255/255.0, 255/255.0]
        self.time_course_thick = 3
        self.reference_func_color = [255/255.0, 255/255.0, 51/255.0]
        self.reference_func_thick = 2
        self.number_conditions = 0
        self.conditions = []
        self.blocks = []

        if filename:
            self.import_prt (filename)


    def delete_intervals_containing (self, name):
        """
            Deletes intervals for conditions that contains given name
        """

        for c in self.conditions:
            if name in c.name:
                c.intervals = []
                c.number_intervals = 0

    def create_blocks (self):
        """
            Create the blocks structure that are sorted by start time        
        """
        # create a list with tuples (start, end, condition)
        sort = []
        for cnt, c in enumerate(self.conditions):
            for i in c.intervals:
                sort.append([i[0], i[1], c])

        # sort list by start time
        sort = sorted(sort, key=lambda x: x[0])

        for s in sort:
            block = Block(s[2], s[0], s[1])
            self.blocks.append(block)


    def condition_at_time (self, time):
        """
            Returns the condition that has interval containing given time
        """
        for c in self.conditions:
            for i in c.intervals:
                if i[0] <= time and i[1] >= time:
                    return c
        return None

    def count_scans (self):
        cnt = 0
        for c in self.conditions:
            for i in c.intervals:
                cnt += i[1] - i[0] + 1
        return cnt

    def scan_labels_from_conditions (self, num_scans=0):
        """
            Create a label array for volumes
            Each volume receives label associated with condition
        """

        if num_scans == 0:
            num_scans = self.count_scans()

        labels = [0] * num_scans
        colors = []
        cnt = 0
        for c in self.conditions:
            for i in c.intervals:
                for x in range(i[0], i[1]+1):
                    labels[x-1] = cnt
            cnt += 1
            
        for c in self.conditions:
            colors.append(c.color)


        return labels, colors


    def count_intervals (self):
        cnt = 0
        for c in self.conditions:
            for i in c.intervals:
                c += 1
        return cnt

    def block_labels_from_conditions (self):
        """
            Create a label array for blocks, only one label per condition and not per volume inside interval
        """
        # create a list with tuples (start, condition id, condition color)
        sort = []
        for cnt, c in enumerate(self.conditions):
            for i in c.intervals:
                sort.append([i[0], cnt, c.name])

        # sort list by start time
        sort = sorted(sort, key=lambda x: x[0]) 

        labels = []
        colors = []

        for s in sort:
            labels.append(s[1])

        for c in self.conditions:
            colors.append(c.color)

        return labels, colors

    def create_labels_colorbar(self, ax):
        """
            Create a colorbar for the conditions (when plotting volumes)
            :param ax: Given axes to display colorbar
        """
        labels_colors = []
        labels_names = []
        # create lists with conditions colors and names
        for c in self.conditions:
            labels_colors.append(c.color)
            labels_names.append(c.name) 

        labels_cmap = mpl.colors.ListedColormap(labels_colors)
        bounds = list(np.arange(1, len(labels_colors)+2))
        norm = mpl.colors.BoundaryNorm(bounds, labels_cmap.N)
        labels_colorbar= mpl.colorbar.ColorbarBase(ax, cmap=labels_cmap, norm=norm,
                  boundaries=bounds,
                  spacing='proportional',
                  orientation='vertical')

        return labels_colorbar


    def print_protocol (self):

        print ('\n################################################')
        print ('PRT filename ', self.filename)
        print ('File Version ', self.file_version)
        print ('Resolution of Time ', self.resolution_of_time)
        print ('Experiment ', self.experiment)
        print ('Background Color ', self.bg_color)
        print ('Text Color ', self.text_color)
        print ('Time Course Line Color ', self.time_course_color)
        print ('Time Course Line Thickness', self.time_course_thick)
        print ('Reference Function Line Color ', self.reference_func_color)
        print ('Reference Function Line Thickness ', self.reference_func_thick)
        print ('Number of Conditions ', self.number_conditions)
        
        cnt = 1
        for c in self.conditions:
            print ('\nCondition ', cnt)
            c.print_condition()
            cnt = cnt + 1

        print ('\n################################################\n')

    def read_prt_header (self, line_list):
        """
            read a header value from PRT file represented as a line list
            
        """
        # searches for first non empty line
        line = line_list.pop(0)
        while not line:
            line = line_list.pop(0)
        # splits line where there are spaces
        line = line.split()
        # removes header
        header = line.pop(0)
        # returns everything else but header
        return line



    def read_prt_condition (self, line_list):
        """
            read a condition from PRT file represented as a line list
            
        """
        # searches for first non empty line
        line = line_list.pop(0)
        while not line:
            line = line_list.pop(0)
 
        condition = Condition()
        condition.name = line
        condition.number_intervals = int(line_list.pop(0))
        for i in range(condition.number_intervals):        
            line = line_list.pop(0).split()
            condition.intervals.append([int(line[0]), int(line[1])])
        line = line_list.pop(0).split()
        line.pop(0)
        condition.color = list(map(int, line))
        condition.color = [c / 255.0 for c in condition.color]

        return condition
     
    def import_prt (self, filename):
        """
            Imports PRT file (Protocol)

            :param filename: name (with path) of PRT file
            :returns: numpy array with PRT data
        """

        self.filename = filename
        with open(filename, "r") as fin:
            line_list = fin.read().splitlines() 
            #line_list = list(filter(None, line_list))

            self.file_version = self.read_prt_header(line_list)[0]
            self.resolution_of_time = self.read_prt_header(line_list)[0]
            self.experiment = self.read_prt_header(line_list)[0]
            self.bg_color = list(map(int, self.read_prt_header(line_list)))
            self.text_color = list(map(int, self.read_prt_header(line_list)))
            self.time_course_color = list(map(int, self.read_prt_header(line_list)))
            self.time_course_thick = int(self.read_prt_header(line_list)[0])
            self.reference_func_color = list(map(int, self.read_prt_header(line_list)))
            self.reference_func_thick = int(self.read_prt_header(line_list)[0])
            self.number_conditions = int(self.read_prt_header(line_list)[0])

            # normalize color attributes in range [0,1] instead of [0,255]
            self.bg_color = [c / 255.0 for c in self.bg_color]
            self.text_color = [c / 255.0 for c in self.text_color]
            self.time_course_color = [c / 255.0 for c in self.time_course_color]
            self.reference_func_color = [c / 255.0 for c in self.reference_func_color]

            self.conditions = []
            for cnd in range(self.number_conditions):
                condition = self.read_prt_condition(line_list)
                self.conditions.append(condition)

            self.create_blocks()

    def export_prt (self, out_filename):
        """
            Exports PRT file (Protocol)

            :param filename: name (with path) of PRT file
        """

        with open(out_filename, "w") as fout:

            fout.write ('\n')
            fout.write ('FileVersion:        %s\n\n' % self.file_version)
            fout.write ('ResolutionOfTime:   %s\n\n' % self.resolution_of_time)
            fout.write ('Experiment:         %s\n\n' % self.experiment)
            fout.write ('BackgroundColor:    %d %d %d\n' % (255*self.bg_color[0], 255*self.bg_color[1], 255*self.bg_color[2]))
            fout.write ('TextColor:          %d %d %d\n' % (255*self.text_color[0], 255*self.text_color[1], 255*self.text_color[2]))
            fout.write ('TimeCourseColor:    %d %d %d\n' % (255*self.time_course_color[0], 255*self.time_course_color[1], 255*self.time_course_color[2]))
            fout.write ('TimeCourseThick:    %d\n' % self.time_course_thick)
            fout.write ('ReferenceFuncColor: %d %d %d\n' % (255*self.reference_func_color[0], 255*self.reference_func_color[1], 255*self.reference_func_color[2]))
            fout.write ('ReferenceFuncThick: %d\n\n' % self.reference_func_thick)
            fout.write ('NrOfConditions:     %d' % self.number_conditions)

            for cnd in self.conditions:
                fout.write ('\n\n')
                fout.write ('%s\n' % cnd.name)
                fout.write ('%d\n' % cnd.number_intervals)
                for i in cnd.intervals:
                    fout.write (' %d %d\n' % (i[0], i[1]))
                fout.write ('Color: %d %d %d' % (255*cnd.color[0], 255*cnd.color[1], 255*cnd.color[2]))

