#
# mtc.py
#
# Mesh Time Course representation from Brainvoyager
# Time course per vertex of surface mesh
#
# Created by Ricardo Marroquim on 14-02-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import numpy as np
from nipy.core.api import Image, vox2mni
import struct
import copy
import os.path
import sys

sys.path.append('../')
from fMRIVis.datastructs import poi

class MTC:
    """
        Mesh Time Course (MTC) data imported from Brainvoyager

        The MTC file basically contains the value for each cortex surface vertex for each scan
            
        .. _SO: http://support.brainvoyager.com/automation-aamp-development/23-file-formats/374-users-guide-23-the-format-of-mtc-files.html
    """

    def __init__(self, mtc_filename='', np_filename=''):
        """
            Inits the MTC class, if given filename imports the data 

            :param mtc_filename: optional mtc file to import the data
            :param np_filename: optional numpy file previously created from mtc to accelerate IO             """
        # mtc format version
        self.version = 0
        # number of surface vertices
        self.num_verts = 0
        # number of points in Time Course
        self.num_time_pts = 0
        # associated Volume Time Course file
        self.vtc_file = ''
        # associated Protocol file
        self.ptr_file = ''
        self.hemodynamic_delay = 0
        self.tr = 0
        self.delta = 0
        self.tau = 0
        self.seg_size = 0
        self.seg_offset = 0
        self.datatype = 0
        self.data = [] # np array of time course per vertex
        self.mtc_filename = ''

        # checks if numpy filename was passed, if not checks if exists one with same name as mtc file and npy extension
        if not np_filename or not os.path.isfile(np_filename):
            if os.path.isfile(mtc_filename + '.npy'):
                np_filename = mtc_filename + '.npy'

        if mtc_filename:
            self.import_mtc (mtc_filename, np_filename)

    def print_header(self):
        """
            Prints header information for MTC file
        """

        print ('\n################################################')
        print ('header info for file ', self.mtc_filename)
        print ('version number ', self.version)
        print ('num vertices ', self.num_verts)
        print ('num time points', self.num_time_pts)
        print ('vtc filename ', self.vtc_file)
        print ('ptr filename ', self.ptr_file)
        print ('hemodynamic delay ', self.hemodynamic_delay)
        print ('TR - Repetion Time ', self.tr)
        print ('Delta parameter for hemodynamic response function ', self.delta)
        print ('Tau parameter for hemodynamic response function ', self.tau)
        print ('Segment size (interval per stimulus block/event) ', self.seg_size)
        print ('Segment offset (first datapoint after first rest period) ', self.seg_offset)
        print ('datatype of MTC data; 1 = float ', self.datatype)
        print ('MTC data dimensions ', self.data.shape)
        print ('################################################\n')

    def patch (self, poi):
        """
            Given a Patch of Interest, returns a new MTC with vertices inside POI

            :param poi: Patch of Interest
            :returns : Mesh Time Course inside poi
        """
        mtc_patch = copy.deepcopy(self)
        mtc_patch.num_verts = poi.num_verts
        mtc_patch.data = []

        for v in poi.verts:
            mtc_patch.data.append( self.data[v] )
        mtc_patch.data = np.asarray(mtc_patch.data)
        return mtc_patch

    def average_per_scan (self, protocol):
        """
        Given a protocol, for each protocol condition averages the scans inside the interval

        :param protocol: Given protocol with conditions and intervals
        :returns : New MTC with one point per condition (average of original scans for condition)
        """

        avg_mtc = copy.deepcopy(self)
        avg_mtc.data = []

        for c in protocol.conditions:
            for i in c.intervals:
                avg = np.mean( self.data[:,i[0]:i[1]+1] )
                avg_mtc.data.append(avg)

        return avg_mtc

    def load_np (self, filename):
        """
            Loads the MTC data from a numpy array directly

            :param filename: Given filename containing MTC data in numpy format
        """

        self.data = np.load(filename)

    def toNIfTI (self):
        """
            Returns a NIfTI Image structure as defined in the nipy library
            Attention: here we return a 4x4 affine matrix, nipy creates a 5x5 matrix

            :returns : An Image NIfTI structure from the MTC
        """

        nifti_image = Image(self.data, vox2mni(np.eye(3)))
#        nifti_image.affine = self.affine
        return nifti_image


    def import_mtc (self, mtc_filename, np_filename=''):
        """
            Imports MTC file (Mesh Time Course)

            :param mtc_filename: name (with path) of MTC file
            :param np_filename: name (with path) of optional numpy file with data (in this case does not import the data from the MTC file, only header info)
        """

        self.mtc_filename = mtc_filename
        with open(mtc_filename, "rb") as fin:

            self.version = struct.unpack('i', fin.read(4))[0]
            self.num_verts = struct.unpack('i', fin.read(4))[0]
            self.num_time_pts = struct.unpack('i', fin.read(4))[0]

            self.vtc_file = ''
            byte = fin.read(1)
            while byte != b'\x00':
                self.vtc_file = self.vtc_file + byte.decode()
                byte = fin.read(1)
            
            self.ptr_file = ''
            byte = fin.read(1)
            while byte != b'\x00':
                self.ptr_file = self.ptr_file + byte.decode()
                byte = fin.read(1)

            self.hemodynamic_delay = struct.unpack('i', fin.read(4))[0]
            self.tr = struct.unpack('f', fin.read(4))[0]
            self.delta = struct.unpack('f', fin.read(4))[0]
            self.tau = struct.unpack('f', fin.read(4))[0]
            self.seg_size = struct.unpack('i', fin.read(4))[0]
            self.seg_offset = struct.unpack('i', fin.read(4))[0]
            self.datatype = ord(fin.read(1))

            self.data = []

            # either loads data from numpy structure if available (faster), or from MTC file (slower)
            if np_filename and os.path.isfile(np_filename):
                self.load_np (np_filename)
            else:
                for v in range(self.num_verts):
                    vert = []
                    for t in range(self.num_time_pts):
                        value = struct.unpack('f', fin.read(4))[0]
                        vert.append(value)
                    self.data.append(vert)
                # if the numpy file does not exist, create it for future use
                self.data = np.asarray(self.data)
                np.save(mtc_filename, self.data)
                print ('created numpy file for', mtc_filename)
