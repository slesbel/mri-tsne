#
# voi.py
#
# Regions of Interest for Volumetric files
#
# Created by Ricardo Marroquim on 21-05-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import sys
import numpy as np
import struct
import os.path

sys.path.append('../')
from fMRIVis.datastructs import vtc


class VOI:
    """
        A single Volume of Interest and its associate voxels
    """

    def __init__(self):
        """
            Inits a VOI
        """
        self.name = ''
        self.color = [0,0,0]
        self.number_voxels = 0
        self.voxels = []

    def print_header(self):
        """
            Print header information
        """

        print ('\n################################################')
        print ('VOI Name ', self.name)
        print ('VOI Color ', self.color)
        print ('Number of Voxels ', self.number_voxels)
        print ('Voxels shape ', self.voxels.shape)
        print ('\n################################################')
 

class VOIList:
    """
        Regions of Interest for Volumes from Brainvoyager
    """

    def __init__(self, voi_filename='', np_filename=''):
        """
            Inits the VOI class, if given filename imports the data
        """
        self.file_version = 0
        self.reference_space = ''
        self.original_vmr_res_x = 0
        self.original_vmr_res_y = 0
        self.original_vmr_res_z = 0
        self.original_vmr_offset_x = 0
        self.original_vmr_offset_y = 0
        self.original_vmr_offset_z = 0
        self.original_vmr_framingcube_dim = 0
        self.left_right_convention = 0
        self.subject_voi_naming_convention = ''
        self.number_vois = 0
        self.vois = []
        self.number_vois_vtcs = 0
        self.vtc_filenames = []
        

        # checks no numpy filename passed, checks if exists one with same name and npy extension
        if not np_filename or not os.path.isfile(np_filename):            
            if os.path.isfile(voi_filename + '.npy'):
                np_filename = voi_filename + '.npy'

        if voi_filename:
            self.import_voi (voi_filename, np_filename)

    def create_mask (self, original_vol, voi_id = []):
        """
            Creates a binary mask for volume with given dimensions
            :param original_vol: original volume (vtc or vmr)
            :param voi: empty to considere all vois, or pass a specific id list
            :returns : labelled mask (each voi is attributed one id > 0)
        """

        mask = np.zeros(shape=original_vol.data.shape[0:3])

        if len(voi_id) == 0:
            voi_id = np.arange(self.number_vois)

        for v_id, v in enumerate(voi_id):
            for tal_coord in self.vois[v].voxels:
                vol_coord = original_vol.talairach2voxel(tal_coord)
                mask[tuple(vol_coord)] = v_id+1

        return mask


    def print_header(self):
        """
            Print header information
        """

        print ('\n################################################')
        print ('Header info for file ', self.voi_filename)
        print ('Version Number ', self.file_version)
        print ('Reference Space (BV, NATIVE, ACPC, Tal) ', self.reference_space)
        print ('Original VMR Resolution X ', self.original_vmr_res_x)
        print ('Original VMR Resolution Y ', self.original_vmr_res_y)
        print ('Original VMR Resolution Z ', self.original_vmr_res_z)
        print ('Original VMR Offset X ', self.original_vmr_offset_x)
        print ('Original VMR Offset Y ', self.original_vmr_offset_y)
        print ('Original VMR Offset Z ', self.original_vmr_offset_z)
        print ('Original VMR Framing Cube Dimensions ', self.original_vmr_framingcube_dim)        
        print ('Left Rigt Convetion ', self.left_right_convention)
        print ('Subject VOI Naming Convention ', self.subject_voi_naming_convention)
        print ('Number of VOIs ', self.number_vois)
        for i, v in enumerate(self.vois):
            print ('VOI ', i, ' / ', self.number_vois)
            v.print_header()
        print ('Number of VOIs VTCs ', self.number_vois_vtcs)
        for i, n in enumerate(self.vtc_filenames):
            print ('VTC Filename ', i, ' / ', self.number_vois_vtcs, n)
        print ('################################################\n')

    def read_voi_voxel (self, line_list):
        """
            Read a voxel value from VOI file
            
        """
        # searches for first non empty line
        line = line_list.pop(0)
        while not line:
            line = line_list.pop(0)
        # splits line where there are spaces
        line = line.split()
        # returns everything else but header
        return line


    def read_voi_header (self, line_list):
        """
            Read a header value from VOI file represented as a line list
            
        """
        # searches for first non empty line
        line = line_list.pop(0)
        while not line:
            line = line_list.pop(0)
        # splits line where there are spaces
        line = line.split()
        # removes header
        header = line.pop(0)
        # returns everything else but header
        return line

    def import_voi (self, voi_filename, np_filename=''):
        """
            Imports VOI file (Regions of Interest)
            :param voi_filename: name (with path) of VOI file
            :param np_filename: name (with path) of optional numpy file with verts (in this case does not import the voxels, only header info)
        """

        self.voi_filename = voi_filename
        with open(self.voi_filename, "r") as fin:
            line_list = fin.read().splitlines() 

            self.file_version = self.read_voi_header(line_list)[0]
            self.reference_space = self.read_voi_header(line_list)[0]

            self.original_vmr_res_x = int(self.read_voi_header(line_list)[0])
            self.original_vmr_res_y = int(self.read_voi_header(line_list)[0])
            self.original_vmr_res_z = int(self.read_voi_header(line_list)[0])
            self.original_vmr_offset_x = int(self.read_voi_header(line_list)[0])
            self.original_vmr_offset_y = int(self.read_voi_header(line_list)[0])
            self.original_vmr_offset_z = int(self.read_voi_header(line_list)[0])
            self.original_vmr_framingcube_dim = int(self.read_voi_header(line_list)[0])
            self.left_right_convention = int(self.read_voi_header(line_list)[0])
            self.subject_voi_naming_convention = self.read_voi_header(line_list)[0]

            self.number_vois = int(self.read_voi_header(line_list)[0])

            # if there exists an associated numpy file, we skip reading the actual voxels from the text file, and read them directly from the binary numpy for performance reasons
            has_np = False
            np_data = []
            if np_filename and os.path.isfile(np_filename):
                np_data = np.load(np_filename)
                has_np = True

            for i in range(self.number_vois):
                new_voi = VOI()
                new_voi.name = self.read_voi_header(line_list)[0]
                new_voi.color = list(map(int, self.read_voi_header(line_list)))
                new_voi.color = [c / 255.0 for c in new_voi.color]
                new_voi.number_voxels = int(self.read_voi_header(line_list)[0])

                if has_np:
                    new_voi.voxels = np_data[i]
                    for v in range(new_voi.number_voxels):
                        line_list.pop(0)
                else:
                    for v in range(new_voi.number_voxels):
                        new_voxel = list(map(int, self.read_voi_voxel(line_list)))
                        new_voi.voxels.append(new_voxel)
                    new_voi.voxels = np.asarray(new_voi.voxels)
                    np_data.append(new_voi.voxels)
                self.vois.append(new_voi)

            # if no previous Numpy file, save one
            if not has_np:
                np.save(self.voi_filename, np_data)

            self.number_vois_vtcs = int(self.read_voi_header(line_list)[0])

            for i in range(self.number_vois_vtcs):
                self.vtc_filenames.append( self.read_voi_header(line_list)[0] )
     
