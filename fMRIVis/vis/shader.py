from OpenGL.GL import *
from OpenGL.GL import shaders
from OpenGL.GL import GL_VERTEX_SHADER, GL_FRAGMENT_SHADER

class Shader:

    def compile(self, vertex_filename, frag_filename):
        with open(vertex_filename, "r") as fin:
            vertex_source = str(fin.read())

        with open(frag_filename, "r") as fin:
            frag_source = fin.read()

        vertex_shader = shaders.compileShader(vertex_source, GL_VERTEX_SHADER)
        frag_shader = shaders.compileShader(frag_source, GL_FRAGMENT_SHADER)

        self.program = shaders.compileProgram(vertex_shader, frag_shader)

