#version 330

in vec4 color;
in vec3 normal;
in vec4 vert;
in float attrib_value;
in float selected;

out vec4 out_color;

uniform mat4 view_matrix;
uniform bool use_light;
uniform bool use_attrib;

float ka = 0.2;
float kd = 1.0;
float ks = 0.2;
float shininess = 25.0;

vec4 colormap(float x) {
    float r = clamp(8.0 / 3.0 * x, 0.0, 1.0);
    float g = clamp(8.0 / 3.0 * x - 1.0, 0.0, 1.0);
    float b = clamp(4.0 * x - 3.0, 0.0, 1.0);
    return vec4(r, g, b, 1.0);
}

void main()
{
	vec4 map_color = color;
	if (use_attrib)
		if (attrib_value == 0.0 || attrib_value == -1.0)
			map_color = vec4(0.3, 0.1, 0.1, 1.0);
		else
			map_color = colormap(attrib_value);
	else
		map_color = vec4(0.3, 0.1, 0.1, 1.0);

	if (selected > 0.5)
		map_color = vec4(0.2, 0.2, 1.0, 1.0);


	if (use_light)
	{
		vec3 light_direction = normalize(vec3(0.3, 0.1, 1.0));

		vec3 light_reflection = reflect(-light_direction, normal);
		vec3 eye_direction = normalize (-vert.xyz);

		vec4 ambient_light = map_color * ka;
		vec4 diffuse_light = map_color * kd * max(dot(light_direction, normal), 0.0);
		vec4 specular_light = vec4(vec3(ks), 1.0) * max(pow(dot(light_reflection, eye_direction), shininess), 0.0);

		out_color = vec4(ambient_light.xyz + diffuse_light.xyz + specular_light.xyz, 1.0);
	}
	else
	{
		out_color = map_color;
	}
	
}


