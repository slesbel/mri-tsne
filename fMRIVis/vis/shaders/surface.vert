#version 330

in vec3 in_position;
in vec3 in_normal;
in float in_attrib;
in float in_selected;

out vec4 color;
out vec3 normal;
out vec4 vert;
out float attrib_value;
out float selected;

uniform mat4 model_matrix;
uniform mat4 view_matrix;
uniform mat4 projection_matrix;

void main()
{

	gl_Position = projection_matrix * view_matrix * model_matrix * vec4(in_position, 1.0);
	vert = view_matrix * model_matrix * vec4(in_position, 1.0);

	mat4 normal_matrix = transpose( inverse(view_matrix * model_matrix) );
	normal = normalize( vec3( normal_matrix * vec4(in_normal, 0.0)).xyz);

	color = vec4(vec3(0.3), 1.0);
	attrib_value = in_attrib;
	selected = in_selected;
}


