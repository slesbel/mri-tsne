#
# glutils.py
#
# OpenGL Utils, common methods
#
# Created by Ricardo Marroquim on 01-03-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import numpy as np
import math

from OpenGL.GL import *
from OpenGL import GLX
from OpenGL import GL
from OpenGL.GL import shaders
from OpenGL.GL import GL_VERTEX_SHADER, GL_FRAGMENT_SHADER


def compute_projection_matrix (width, height, fov, near_plane, far_plane):
    """
    Computes a projection matrix
    Returns matrix in column major format (Opengl compatible)
    """

    projection_matrix = np.identity(4, np.float32)
    aspect_ratio = width/height
    frustum_length = far_plane - near_plane
    f = 1.0 / math.tan((fov/2.0)*(3.14/180.0))
    sx = f / aspect_ratio
    sy = f
    sz = -1.0 * ((far_plane + near_plane) / frustum_length)
    pz = -1.0 * ((2.0 * near_plane * far_plane) / frustum_length)

    projection_matrix[0,0] = sx
    projection_matrix[1,1] = sy
    projection_matrix[2,2] = sz
    projection_matrix[3,2] = pz
    projection_matrix[2,3] = -1.0
    projection_matrix[3,3] = 0.0

    return projection_matrix



class Shader:

    def compile(self, vertex_filename, frag_filename):
        with open(vertex_filename, "r") as fin:
            vertex_source = str(fin.read())

        with open(frag_filename, "r") as fin:
            frag_source = fin.read()

        vertex_shader = shaders.compileShader(vertex_source, GL_VERTEX_SHADER)
        frag_shader = shaders.compileShader(frag_source, GL_FRAGMENT_SHADER)

        self.program = shaders.compileProgram(vertex_shader, frag_shader)


#    def initMatrices (self):
#        #  view matrix
#        self.view_matrix = np.identity(4, np.float32)
#        self.initial_view = np.identity(4, np.float32)
#        self.model_matrix = np.identity(4, np.float32)
#
#        # model matrix normalizes vertices in range[0,1] and centers model on origin
#        self.model_matrix[:3,:3] *= self.normalization_scale
#        self.model_matrix[3,:3] = -self.center[:3] * self.normalization_scale
#
#        # initial camera translation
#        self.initial_view[3,:] = np.array([0.0, 0.0, -2, 1.0])
#
#        self.view_matrix = self.initial_view
#        self.initial_view_inv = np.linalg.inv(self.initial_view)
#
#        print ('\n model\n', self.model_matrix)
#        print ('\n view\n', self.initial_view)
#        print ('\n view inv\n', self.initial_view_inv)

