#
# surface_viewer.py
#
# OpenGL viewer for a surface mesh f
#
# Created by Ricardo Marroquim on 14-02-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import gi
gi.require_version('Gtk', '3.0')

import sys
import os
import numpy as np
import math
from mpmath import *
from scipy import stats
from sklearn.preprocessing import MinMaxScaler

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL import GLX
from OpenGL import GL
from OpenGL.arrays import vbo

from gi.repository import Gtk, GdkPixbuf, Gdk

sys.path.append('../')
from fMRIVis.datastructs import protocol, srf, smp, mtc
from fMRIVis.vis import arcball, glutils
from fMRIVis.models import transformations

class SurfaceViewer:
    """
        OpenGL based Surface Viewer GTK window
    """

    def __init__(self, srf, size = [800, 800]):
        """
            Initializes Surface Viewer class
        """
        self.use_light = True
        self.arcball_rotating = False
        self.enable_render = False
        self.size = size

        # Surface representation (geometry)
        self.srf = srf

        # Create an OpenGL drawing area
        self.drawing_area = Gtk.GLArea()
        self.drawing_area.connect('configure_event', self.on_configure_event)
        self.drawing_area.connect('render', self.on_draw)
        self.drawing_area.connect('realize', self.initGL)
        self.drawing_area.set_double_buffered(True)
        self.drawing_area.set_has_depth_buffer(True)
        self.drawing_area.set_size_request(self.size[0], self.size[1])
        # model center and normalization scale
        self.center, self.normalization_scale =  self.srf.compute_normalization()
        
      
        # arcball
        self.arcball = arcball.ArcBall ((self.size[0]/2, self.size[1]/2, 0), self.size[0]/2)
        self.zoom = 1.0

        # last mouse position with button clicked (for arcball)
        self.mouse = [0, 0]

        self.initMatrices()

    def initMatrices (self):
        #  view matrix
        self.view_matrix = np.identity(4, np.float32)
        self.initial_view = np.identity(4, np.float32)
        self.model_matrix = np.identity(4, np.float32)
        self.zoom_matrix = np.identity(4, np.float32)

        # model matrix normalizes vertices in range[0,1] and centers model on origin
        
        self.model_matrix = transformations.scale_matrix4 (self.model_matrix, self.normalization_scale)
        self.model_matrix[3,:3] = -self.center[:3] * self.normalization_scale

        # initial camera translation
        self.initial_view[3,:] = np.array([0.0, 0.0, -3.0, 1.0])

        self.view_matrix = self.initial_view
        self.initial_view_inv = np.linalg.inv(self.initial_view)

        # projection matrix
        self.projection_matrix = glutils.compute_projection_matrix(self.size[0], self.size[1], 45.0, 0.1, 100.0)

    def load_attrib_buffer (self, buf):
        """
            Loads a new vertex attribute buffer
        """
        if len(buf) != self.srf.number_verts:
            return -1

        self.drawing_area.make_current()
        self.attrib_buffers.append (vbo.VBO(np.array(buf, dtype=np.float32)))
        self.selected_attrib = len(self.attrib_buffers) - 1

        return self.selected_attrib
            

    def initGL (self, area):
        """
            Initialize GL stuff
        """
        # make current context to initialize GL stuff
        area.get_context().make_current()

        print(glGetString(GL_VENDOR))
        print(glGetString(GL_VERSION))
 
        glViewport(0, 0, self.size[0], self.size[1])
        
        glEnable(GL_CULL_FACE)
        glCullFace(GL_BACK)
        # if using Neurological convention (instead of radiological), use GL_CW
        glFrontFace(GL_CCW)

        glDisable(GL_BLEND)
        glEnable(GL_DEPTH_TEST)
        glDepthMask(GL_TRUE)
        glDepthFunc(GL_LESS)
        glDepthRange(0.0, 1.0)
        glClearDepth(1.0)
        glClearColor(0.3, 0.3, 0.3, 1.0)

        # create Vertex Array Object and leave it bind
        self.vertex_array_object = GL.glGenVertexArrays(1)
        glBindVertexArray( self.vertex_array_object )

        # setup VBOs
        self.vertex_buffer = vbo.VBO(np.array(self.srf.verts, dtype=np.float32))
        self.normal_buffer = vbo.VBO(np.array(self.srf.normals, dtype=np.float32))
        self.index_buffer = vbo.VBO(np.array(self.srf.triangles, dtype=np.int32), target=GL_ELEMENT_ARRAY_BUFFER)

        self.vert_selection = np.zeros(self.srf.number_verts, dtype=np.float32)
        self.selection_buffer = vbo.VBO(np.array(self.vert_selection, dtype=np.float32))

        # attrib buffer is initially empty
        self.attrib_buffers = [] 
        self.selected_attrib = -1

        # Load the shaders
        script_dir = os.path.dirname(__file__)
        self.shader = glutils.Shader()
        self.shader.compile(script_dir + '/shaders/surface.vert', script_dir + '/shaders/surface.frag')
 
        # obtain uniform locations
        glUseProgram(self.shader.program)
        self.uniform_vertex = glGetAttribLocation(self.shader.program, "in_position")
        self.uniform_normal = glGetAttribLocation(self.shader.program, "in_normal")
        self.uniform_attrib = glGetAttribLocation(self.shader.program, "in_attrib")
        self.uniform_selected = glGetAttribLocation(self.shader.program, "in_selected")
        self.uniform_model_matrix = glGetUniformLocation(self.shader.program, 'model_matrix')
        self.uniform_view_matrix = glGetUniformLocation(self.shader.program, 'view_matrix')
        self.uniform_projection_matrix = glGetUniformLocation(self.shader.program, 'projection_matrix')
        self.uniform_use_light = glGetUniformLocation(self.shader.program, 'use_light')
        self.uniform_use_attrib = glGetUniformLocation(self.shader.program, 'use_attrib')
        glUseProgram(0)

    def rotate_surface(self, x, y):
        """
            Rotates the surface using the Arcball
        """
        angle, axis = self.arcball.rot (self.mouse[0], self.size[1]-self.mouse[1], x, self.size[1]-y)
        self.mouse[0], self.mouse[1] = x,y        
        arcball_rotation = transformations.rotation_matrix_from_axis_angle(axis, -angle)

        rotation = self.initial_view_inv.dot(arcball_rotation.dot(self.initial_view))
        self.view_matrix = self.view_matrix.dot(rotation)


    def draw_srf (self):

        self.vertex_buffer.bind()
        glEnableVertexAttribArray(self.uniform_vertex);
        glVertexAttribPointer(self.uniform_vertex, 3, GL_FLOAT, False, 0, None)

        self.normal_buffer.bind()
        glEnableVertexAttribArray(self.uniform_normal);
        glVertexAttribPointer(self.uniform_normal, 3, GL_FLOAT, False, 0, None)

        self.selection_buffer.bind()
        glEnableVertexAttribArray(self.uniform_selected);
        glVertexAttribPointer(self.uniform_selected, 1, GL_FLOAT, False, 0, None)

        if self.selected_attrib == -1:
            glUniform1i(self.uniform_use_attrib, 0)
        else:
            glUniform1i(self.uniform_use_attrib, 1)        
            self.attrib_buffers[self.selected_attrib].bind()            
            glEnableVertexAttribArray(self.uniform_attrib);
            glVertexAttribPointer(self.uniform_attrib, 1, GL_FLOAT, False, 0, None)


        self.index_buffer.bind()
        glDrawElements(GL_TRIANGLES, self.srf.number_triangles*3, GL_UNSIGNED_INT, None)
#        glDrawArrays(GL_POINTS, 0, self.srf.number_triangles*3)

        glDisableVertexAttribArray(self.uniform_vertex)
        glDisableVertexAttribArray(self.uniform_normal)
        self.vertex_buffer.unbind()
        self.normal_buffer.unbind()
        self.index_buffer.unbind()
        if self.selected_attrib != -1:
            self.attrib_buffers[self.selected_attrib].unbind()


    def render (self):
        """
            Renders the 3D surface
        """
        self.drawing_area.make_current()
        self.drawing_area.get_context().make_current()

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        glUseProgram(self.shader.program)

        model = self.model_matrix.dot(self.zoom_matrix)
        glUniformMatrix4fv(self.uniform_model_matrix, 1, GL_FALSE, model)
        glUniformMatrix4fv(self.uniform_view_matrix, 1, GL_FALSE, self.view_matrix)
        glUniformMatrix4fv(self.uniform_projection_matrix, 1, GL_FALSE, self.projection_matrix)
        glUniform1i(self.uniform_use_light, self.use_light)

        self.draw_srf()

        glUseProgram(0)

        err = glGetError()
        if ( err != GL_NO_ERROR ):
            print ('GLERROR: ', gluErrorString( err ))

    def update_selection (self):
        self.selection_buffer.set_array(self.vert_selection)
        self.drawing_area.queue_render()

    def clear_selection (self):
        self.vert_selection[:] = 0
        self.update_selection()
 
    def on_configure_event(self, widget, event):
        self.drawing_area.queue_render()
        return True

    def on_draw(self, area, context):
        self.render()
        return True

    def motion_notify_event (self, win, event):
        """
        """
        if self.arcball_rotating:
            self.rotate_surface(event.x, event.y)
        self.drawing_area.queue_render()
        return True
 
    def on_button_released(self, win, event):
        """
        Release mouse buttons
        """
        self.arcball_rotating = False
        return True

    def on_button_pressed (self, win, event):
        """
        """
        if event.button == 1:
            self.arcball_rotating = True
            self.mouse[0], self.mouse[1] = event.x, event.y        
            self.rotate_surface(event.x, event.y)
        self.drawing_area.queue_render()
        return True

    def mouse_scroll (self, win, event):
        if event.direction == Gdk.ScrollDirection.UP:
            self.zoom = min(5.0, self.zoom+0.1)
        elif event.direction == Gdk.ScrollDirection.DOWN: 
            self.zoom = max(0.1, self.zoom-0.1)

        self.zoom_matrix = transformations.scale_matrix4(np.identity(4, np.float32), self.zoom)

        self.drawing_area.queue_render()
        return True

    def toggle_light (self, widget):
        self.use_light = not self.use_light
        self.drawing_area.queue_render()


