#
# mri_tsne.py
#
# Wrapper for TSNE from sklearn
#
# Created by Ricardo Marroquim on 15-02-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import numpy as np
import sys
from tqdm import trange
from sklearn.manifold import TSNE as sklearn_TSNE
import itertools
import math

class TSNE:
    """
        encapsulated TSNE implementation
    """

    def __init__ (self):

        # tsne parameters
        self.perplexity = 5.0
        self.iterations = 8000
        self.method = 'exact' # alternative is barnes_hut
        self.angle = 0.2 # only for barnes_hut
        self.init = 'random' # alternative is pca
        self.learning_rate = 450.0
        self.early_exaggeration = 5.0
        self.metric = 'euclidean'
        self.n_iter_without_progress = 300
        self.verbose = 1
        self.random_state = None

        self.points_2d = []
        self.points_nd = []

        self.labels = []

        # cluster centers
        self.centers = []
        self.centers_labels = []

        # sigma for Gaussian weighted clusters
        self.centers_sigma = 50.0
 
    def run(self):
        """
            Run the t-SNE method
        """
        self.points_2d = sklearn_TSNE(n_components=2, perplexity=self.perplexity, n_iter=self.iterations, verbose=self.verbose, angle=self.angle, method=self.method, init=self.init, random_state=self.random_state, learning_rate=self.learning_rate, early_exaggeration=self.early_exaggeration, metric=self.metric, n_iter_without_progress=self.n_iter_without_progress).fit_transform(self.points_nd)

        self.normalize_points()

 #       self.points_2d = self.tsne(self.points_nd, 2, self.points_nd.shape[1], self.perplexity, self.iterations)        


    def normalize_points (self):
        """
            Nomalize 2d points in range [-1, 1]            
        """
        self.points_2d = (2.0 * self.points_2d - np.min(self.points_2d)) / (np.max(self.points_2d) - np.min(self.points_2d)) - 1.0
        

    def compute_cluster_centers (self):
        """
            Computer the cluster centers
        """

        tuples = zip(self.points_2d, self.labels.tolist())
        sort = sorted(tuples, key=lambda x: x[1])
        self.centers = []
        self.centers_labels = []

        sums = itertools.groupby(sorted(sort, key = lambda i: i[1]), lambda i: i[1])
        for l, grp in sums:
            center = np.zeros(2)
            n = 0
            for p in grp:
                dist = np.linalg.norm(center-p[0])
                weight = math.exp(-0.5*dist*dist/self.centers_sigma*self.centers_sigma)
                center += p[0]*weight
                n += weight
            center = center / n
            self.centers.append(center)
            self.centers_labels.append(l)

        self.centers = np.asarray (self.centers)
        self.centers_labels = np.asarray (self.centers_labels)


    def set_labels(self, labels):
        """
            Sets labels from np array
        """
        self.labels = np.asarray(labels)
        self.selection = np.zeros(shape=[len(self.labels),4])
        cl = self.selection_color
        cl[3] = 0.0
        self.selection[:,:] = cl
        self.vmax = self.labels.max()
        self.vmin = self.labels.min()

    def set_ndpoints(self, pts):
        """
            Sets nd points from np array
        """
        self.points_nd = pts
        for p in self.points_nd:
            p = (p - np.min(p)) / (np.max(p) - np.min(p))


    def load_ndpoints(self, filename):
        """
            Load nd points from txt file
        """
        self.points_nd = np.loadtxt(filename)

    def load_2dpoints(self, filename):
        """
            Load tsne 2D points from file
        """
        self.points_2d = np.load(filename)

    def save_2dpoints(self, event):
        np.save('temp_tsne', self.points_2d)


