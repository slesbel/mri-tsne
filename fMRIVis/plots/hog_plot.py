#
# hog_plot.py
#
# Visualize Histogram of Gradients
#
# Created by Ricardo Marroquim on 26-03-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import sys
import numpy as np
import math
import matplotlib as mpl
import matplotlib.pyplot as plt

sys.path.append('../')

class HoGPlot:
    """
        Plots a Histogram of Gradients
    """
    def __init__(self, size=[800, 800], name=''):
        """
            Initializes HoG plot
        """
        self.size = size
        self.name = name
        self.bins = 30


    def init_hog_plot (self, ax):
        """
            Initialize axes for plotting
        """
        self.hog_ax = ax

    def compute_hog (self, points, data, bins=30):
        """
        Computes a histogram from a 2d vector
        """
        self.bins = bins

        # clip data and points with gaussian center too far off
        filter_ids = np.argwhere(abs(data) >  5)
        points = np.delete(points, filter_ids, axis=0)
        data = np.delete(data, filter_ids)

        # recreate triangulation using matplotib Delaunay triangulator
        # there is some problem with using Brainvoyager's, must check why returns invalid triangulation
        triangulation = mpl.tri.Triangulation (x=points[:,0], y=points[:,1])
        tri_interp = mpl.tri.LinearTriInterpolator(triangulation, data)

        self.gradient_norm = []
        self.gradient_angle = []
        for p in points:
            grad = tri_interp.gradient(p[0], p[1])            
            self.gradient_norm.append ( math.sqrt(grad[0]*grad[0] + grad[1]*grad[1]) )
            self.gradient_angle.append ( math.atan (grad[1] / grad[0]) )

        # remove nan from arrays
        nan_ids = np.argwhere(np.isnan(self.gradient_angle))
        self.gradient_angle = np.delete(self.gradient_angle, nan_ids)
        self.gradient_norm = np.delete(self.gradient_norm, nan_ids)

        print ('gradients (hog_plot), max, min', np.max(self.gradient_norm), np.min(self.gradient_norm))

        # historgram of gradient angles weighted by gradient norm
        self.hist, self.hist_edges = np.histogram(self.gradient_angle, weights=self.gradient_norm, bins=bins)

    def plot (self):
        """
            Plot HoG
        """
        self.hog_ax.clear()
        theta = np.linspace(0.0, 2 * np.pi, self.bins, endpoint=False)

        self.hog_ax.bar (theta, self.hist, width=0.1)
        

