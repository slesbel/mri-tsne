#
# volume_plot.py
#
# Plots the voxel/vertex selection of a volume/scan
#
# Created by Ricardo Marroquim on 16-02-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import sys
import matplotlib as mpl
from matplotlib.widgets import RadioButtons
from scipy import stats
import numpy as np

sys.path.append('../')
from fMRIVis.datastructs import protocol

class VolumeData():

    def __init__ (self, data=[], st=0):
        """
            Initializes the volume data
        """
        self.data = data
        self.zscore = []
        if len(self.data) > 0:
            self.zscore = stats.zscore(data)
        self.scan_time = st

    def load_scan_data (self, d, st):
        """
            Load the selected scan data from numpy array
        """
        self.data = d
        self.zscore = stats.zscore(d)
        self.scan_time = st


class VolumeTimeCoursePlot():
    """
        Methods for plotting the selected elements of a single scan
    """

    def __init__(self):
        """
        """
        self.volume = VolumeData()
        self.protocol = protocol.Protocol()

    def set_volume_data (self, vol):
        """
            Sets the selected data for plotting
        """
        self.volume = vol

    def set_protocol (self, prt):
        """
            Sets the associate protocol for the experiment
        """
        self.protocol = prt

    def change_type (self, connection_id):
        """
            Change visualization type
        """
        self.plot ()


    def init_plot (self, axis):
        """
            Initializes axes for plotting the data
        """
        self.inspection_ax = axis

    def init_type_button (self, axis):
        """
            Initializes the button to change plot scale (value/zscore)
        """
        self.type_button = RadioButtons(axis, ('value', 'zscore'))
        self.type_button.activecolor = 'skyblue'
        self.type_button.on_clicked(self.change_type)

    def plot (self, vol = []):
        """
            Plot selected data of a volume
            Draws background as corresponding condition of the volume
        """
        if vol:
            self.volume = vol

        self.inspection_ax.clear()

        condition = self.protocol.condition_at_time (self.volume.scan_time)
        if not condition:
            return

        self.inspection_ax.set_facecolor(condition.color)

        thick = 1

        # line color depends on bg luminance, to enhance contrast
        luminance = 0.2126*condition.color[0] + 0.7152*condition.color[1] + 0.0722*condition.color[2]
        if luminance > 0.5:
            color = 'black'
        else:
            color = 'white'

        if self.type_button.value_selected == 'value':
            self.inspection_ax.plot(self.volume.data, linewidth=thick, color=color, zorder=2)
            mean_value = np.mean(self.volume.data)
        elif self.type_button.value_selected == 'zscore':
            self.inspection_ax.plot(self.volume.zscore, linewidth=thick, color=color, zorder=2)
            mean_value = np.mean(self.volume.zscore)

        self.inspection_ax.hlines(y=mean_value, xmin=0, xmax=len(self.volume.data), color=color, zorder=2)

        self.inspection_ax.set_title(str(self.volume.scan_time) + ' ' + condition.name + ' ' + str("{:.3}".format(mean_value)))
        self.inspection_ax.figure.canvas.draw()



