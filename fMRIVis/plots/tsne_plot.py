#
# tsne_plot.py
#
# Plots the TSNE result using matplotlib
#
#  Created by Ricardo Marroquim on 07-04-2018.
#  Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved

import numpy as np
import matplotlib as mpl
from matplotlib.widgets import RadioButtons, Slider, Button
from matplotlib import colors as mcolors
import matplotlib.pyplot as plt

class TsnePlot:
    """
        Plot methods for t-SNE
    """

    def __init__ (self, tsne):
        """
            Initialize t-SNE Plot class
        """

        # associated tsne class instance
        self.tsne = tsne

        self.selection = []
        self.colors = []
        self.selection_color = np.asarray(mcolors.to_rgba(mcolors.cnames["royalblue"]))
        self.colormap = plt.get_cmap('hot')
        self.vmax = 1.0
        self.vmin = 0.0

        self.show_points = True
        self.show_centers = False

        # label annotation
        self.show_annotations = False
        self.annotation_type = 'number'
        self.annotations = []

        #decorations
        self.button_font_size = 9
        self.button_color = 'skyblue'
        self.button_hover = '0.975'

        self.margin_multiplier = 1.10

    def change_perplexity(self, val):
        """
            Change the perplexity parameter for tsne
        """
        self.tsne.perplexity = val

    def create_fake_labels (self, dim):
        # if no labels were loaded, create fake array with zeros
        self.tsne.labels = np.zeros(dim)
        self.selection = np.zeros(shape=[len(self.tsne.labels),4])
        self.colors = self.tsne.labels

    def run_tsne(self, event):
        """
            Execute t-SNE and update the plot
        """
        self.tsne.run()
        self.tsne.compute_cluster_centers()
        self.plot()

    def set_labels(self, labels):
        """
            Sets labels from np array
        """
        self.tsne.labels = np.asarray(labels)
        self.selection = np.zeros(shape=[len(self.tsne.labels),4])
        cl = self.selection_color
        cl[3] = 0.0
        self.selection[:,:] = cl
        self.vmax = self.tsne.labels.max()
        self.vmin = self.tsne.labels.min()

    def load_labels(self, filename):
        """
            Load labels file (may be continuos values)
        """
        # tsne corresponding labels for colormap
        self.tsne_labels = np.load(filename)
        self.vmax = max(-self.tsne_labels.min(), self.tsne_labels.max())
        # initial plot markers colors (all unselected)
        self.selection = np.empty(shape=[len(self.tsne.labels),4])
        cl = self.selection_color
        cl[3] = 0.0
        self.selection[:,:] = cl
        self.colors = self.tsne.labels

    def clear_selection(self):
        self.selection[:,3] = 0.0
        self.plot()

    def toggle_annotation (self, event):
        """
            Toggles annotation flag
        """
        self.show_annotations = not self.show_annotations
        self.plot()

    def toggle_annotation_type (self, event):
        """
            Toggles annotation type name/number
        """
        if self.annotation_type == 'name':
            self.annotation_type = 'number'
        elif self.annotation_type == 'number' and len(self.annotations) > 0:
            self.annotation_type = 'name'
        self.plot()

    def toggle_centers (self, event):
        """
            Toggles show centers flag
        """
        self.show_centers = not self.show_centers
        self.plot()

    def toggle_points (self, event):
        """
            Toggles show points flag
        """
        self.show_points = not self.show_points
        self.plot()


    def init_scatter_plot (self, axis):
        """
            Initialize tsne plot
        """
        self.plot_ax = axis
        self.plot_ax.set_facecolor('darkseagreen')

    def init_perplexity_slider (self, axis, callback=None):
        """
            Initialize slider to change TSNE perplexity value
        """
        self.perplexity_slider = Slider(axis, 'Perplexity', 1, 100, valinit=self.tsne.perplexity)
        if callback:
            self.perplexity_slider.on_changed(callback)
        else:
            self.perplexity_slider.on_changed(self.change_perplexity)

    def init_colorbar (self, axis, text='zmap values', discrete=False):
        """
            Initialize labels colorbar
        """

        if discrete:
            bounds = list(np.arange(1, len(self.colors)+2))
            norm = mpl.colors.BoundaryNorm(bounds, self.colormap.N)

            scatter_colorbar = mpl.colorbar.ColorbarBase(axis, cmap=self.colormap,
                  norm=norm, boundaries=bounds,
                  spacing='proportional',
                  orientation='vertical')

            axis.yaxis.set_major_formatter(mpl.ticker.NullFormatter())
            # ticks positions for labels
            ticks = []
            for t in range(len(self.colors)):
                ticks.append (t*(1.0/len(self.colors))+0.5/len(self.colors)
    )
            axis.yaxis.set_major_locator(mpl.ticker.FixedLocator(ticks))

            color_num = np.arange(1,len(self.colors)+1)
            axis.yaxis.set_major_formatter(mpl.ticker.FixedFormatter(color_num))

        else:
            scatter_colorbar = mpl.colorbar.ColorbarBase(axis, cmap=self.colormap,
                  spacing='proportional',
                  orientation='vertical')
 

        axis.yaxis.set_label_position('left')
        axis.set_ylabel(text, rotation=90)



    def init_clear_button (self, axis, callback=None):
        """
            Initialize clear selection button
        """
        self.clear_selection_button = Button(axis, 'Clear Selection', color=self.button_color, hovercolor=self.button_hover)
        self.clear_selection_button.label.set_fontsize(self.button_font_size)
        if callback:
            self.clear_selection_button.on_clicked(callback)
        else:
            self.clear_selection_button.on_clicked(self.clear_selection)


 
    def init_save_button (self, axis, callback=None):
        """
            Initialize save 2d points button
        """
        self.save_button = Button(axis, 'Save 2D Pts', color=self.button_color, hovercolor=self.button_hover)
        self.save_button.label.set_fontsize(self.button_font_size)
        if callback:
            self.save_button.on_clicked(callback)
        else:
            self.save_button.on_clicked(self.tsne.save_2dpoints)


    def init_run_button (self, axis, callback=None):
        """
            Initialize run tsne button
        """
        self.run_button = Button(axis, 'Run TSNE', color=self.button_color, hovercolor=self.button_hover)
        self.run_button.label.set_fontsize(self.button_font_size)
        if callback:
            self.run_button.on_clicked(callback)
        else:
            self.run_button.on_clicked(self.run_tsne)


    def init_annotate_button (self, axis, callback=None):
        """
            Initialize annotate button on/off
        """
        self.annotate_button = Button(axis, 'Annotate', color=self.button_color, hovercolor=self.button_hover)
        self.annotate_button.label.set_fontsize(self.button_font_size)
        if callback:
            self.annotate_button.on_clicked(callback)
        else:
            self.annotate_button.on_clicked(self.toggle_annotation)

    def init_annotation_type_button (self, axis, callback=None):
        """
            Initialize annotation type button (number or name)
        """
        self.annotation_type_button = Button(axis, 'Number/Name', color=self.button_color, hovercolor=self.button_hover)
        self.annotation_type_button.label.set_fontsize(self.button_font_size)
        if callback:
            self.annotation_type_button.on_clicked(callback)
        else:
            self.annotation_type_button.on_clicked(self.toggle_annotation_type)

    def init_show_centers_button (self, axis, callback=None):
        """
            Initialize show cluster centers buttons
        """
        self.centers_button = Button(axis, 'Centers', color=self.button_color, hovercolor=self.button_hover)
        self.centers_button.label.set_fontsize(self.button_font_size)
        if callback:
            self.centers_button.on_clicked(callback)
        else:
            self.centers_button.on_clicked(self.toggle_centers)

    def init_show_points_button (self, axis, callback=None):
        """
            Initialize show points buttons
        """
        self.points_button = Button(axis, 'Points', color=self.button_color, hovercolor=self.button_hover)
        self.points_button.label.set_fontsize(self.button_font_size)
        if callback:
            self.points_button.on_clicked(callback)
        else:
            self.points_button.on_clicked(self.toggle_points)

    def plot(self):
        """
            Update the tsne scatter plot from the points and labels arrays
        """
        if len(self.tsne.points_2d) == 0:
            return

        # clear the plot axes and set the limits by hand (so when points are turned off the centers do not move)
        self.plot_ax.clear()
        self.plot_ax.autoscale(False)
        self.plot_ax.set_xlim(np.min(self.tsne.points_2d[:,0])*self.margin_multiplier, np.max(self.tsne.points_2d[:,0])*self.margin_multiplier)
        self.plot_ax.set_ylim(np.min(self.tsne.points_2d[:,1])*self.margin_multiplier, np.max(self.tsne.points_2d[:,1])*self.margin_multiplier)

        if self.show_points:

            # draw tsne points
            self.points_2d_scatter = self.plot_ax.scatter(self.tsne.points_2d[:, 0], self.tsne.points_2d[:, 1], s=50, picker=3, c=self.tsne.labels, cmap=self.colormap, vmin=self.vmin, vmax=self.vmax, alpha = 1.0)
            # overlay with selection
            self.selection_2d_scatter = self.plot_ax.scatter(self.tsne.points_2d[:, 0], self.tsne.points_2d[:, 1], s=50, picker=3, color=self.selection, vmin=self.vmin, vmax=self.vmax)


            # annotate labels
            if self.show_annotations:
               for i in range(0, len(self.tsne.points_2d)):
                    if self.annotation_type == 'name':
                        self.plot_ax.annotate(str(self.annotations[i]), (self.tsne.points_2d[i][0], self.tsne.points_2d[i][1]))
                    elif self.annotation_type == 'number':
                        self.plot_ax.annotate(str(self.tsne.labels[i]+1), (self.tsne.points_2d[i][0], self.tsne.points_2d[i][1]))

        if self.show_centers:
            # draw center points
            self.points_2d_scatter = self.plot_ax.scatter(self.tsne.centers[:, 0], self.tsne.centers[:, 1], s=150, picker=3, c=self.tsne.centers_labels, cmap=self.colormap, vmin=self.vmin, vmax=self.vmax, alpha = 1.0)
            if self.show_annotations:
                for i in range(0, len(self.tsne.centers)):
                    self.plot_ax.annotate(str(self.tsne.centers_labels[i]+1), (self.tsne.centers[i][0], self.tsne.centers[i][1]))


        # update the parent canvas
        self.plot_ax.figure.canvas.draw()


