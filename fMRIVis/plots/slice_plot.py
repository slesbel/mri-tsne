#
# slice_plot.py
#
# Plot for volume slices
#
# Created by Ricardo Marroquim on 24-04-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import numpy as np
import nibabel as nib
import matplotlib
from matplotlib.colors import LinearSegmentedColormap
from matplotlib.colors import ListedColormap
from matplotlib import colors as mcolors
from matplotlib import colorbar
import matplotlib.lines as mlines
import matplotlib.pyplot as plt
from matplotlib.widgets import Button, Slider, Cursor
from nipy.labs.viz import cm
import numpy as np
from scipy import ndimage
import copy

class SlicePlot:
    """
        This class plots MRI slices.
        There are three slices defined by the yz, xz, and xy axes (sagittal, coronal, transversal)
    """
    def __init__ (self, vol_data, zmap, mask, mask_regions=[]):
        
        self.slice_ids = [int(vol_data.shape[0]/2.0), int(vol_data.shape[1]/2.0), int(vol_data.shape[2]/2.0)]
        self.slice_data = [] # array to hold the data of the three currently displayed slices
        self.mask_slices = [] # voxel masks for the current slices (it is volume independent)
        self.mask_zmap_slices = [] # voxel masked zmap for the current slices (it is volume independent)
        self.zmap_slices = [] # voxel zmap for the current slices (it is volume independent)

        # volume time course data
        self.vol_data = vol_data

        # glm result map
        self.zmap = zmap

        # voxel mask with regions labels
        self.mask = mask

        print ('vol_data shape', vol_data.shape)
        print ('zmap data shape', self.zmap.shape)
        print ('mask shape', self.mask.shape)

        self.mask_zmap = copy.deepcopy(self.zmap)
        self.mask_zmap[self.mask < 1.0] = 0.0
        self.mask_zmap = np.ma.masked_values(self.mask_zmap, 0.0)

        # mask zmap where there is no voxel value
        # self.zmap[self.vol_data==0.0] = 0.0
        # self.zmap = np.ma.masked_values(self.zmap, 0.0)


        # create a simple colormap for the selection mask with only two colors (unselected=transparent, selected= select_color)
        select_color = mcolors.cnames["steelblue"]       
        mask_colors = [(0.0, 0.0, 0.0, 0.0), select_color]
        self.mask_selection_cm = LinearSegmentedColormap.from_list("mask_selection_cm", mask_colors, N=2)

        # display the mask only for selected regions, set other regions to zero
        if len(mask_regions) > 0:
            self.mask [np.logical_not(np.isin(self.mask, mask_regions))] = 0

        # mask the mask array where it is zero
        self.mask = np.ma.masked_equal(self.mask, 0)

        # set first slices
        self.slice_data.append(self.get_slice(0))
        self.slice_data.append(self.get_slice(1))
        self.slice_data.append(self.get_slice(2))
        self.mask_slices.append(self.get_mask_slice(0))
        self.mask_slices.append(self.get_mask_slice(1))
        self.mask_slices.append(self.get_mask_slice(2))
        self.mask_zmap_slices.append(self.get_mask_zmap_slice(0))
        self.mask_zmap_slices.append(self.get_mask_zmap_slice(1))
        self.mask_zmap_slices.append(self.get_mask_zmap_slice(2))
        self.zmap_slices.append(self.get_zmap_slice(0))
        self.zmap_slices.append(self.get_zmap_slice(1))
        self.zmap_slices.append(self.get_zmap_slice(2))


        #decorations
        self.button_font_size = 9
        self.button_color = 'skyblue'
        self.button_alt_color = 'lightsteelblue'
        self.button_hover = '0.975'

        self.draw_zmap = True
        self.draw_mask = True

        self.overlay_alpha = 1.0

    def init_slices_ax (self, fig):
        """
            Initializes the slices axis for ploting images and colorbars
        """

        self.figure = fig
        self.sag_ax = self.figure.add_axes ([0.03, 0.05, 0.23, 0.8])
        self.cor_ax = self.figure.add_axes ([0.30, 0.05, 0.23, 0.8])
        self.tra_ax = self.figure.add_axes ([0.57, 0.05, 0.23, 0.8])
        self.zmap_cb_ax = self.figure.add_axes ([0.84, 0.05, 0.02, 0.8])
        self.intensity_cb_ax = self.figure.add_axes ([0.89, 0.05, 0.02, 0.8])
        #self.mask_cb_ax = self.figure.add_axes ([0.94, 0.05, 0.02, 0.8])

        # self.zmap_button_ax = self.figure.add_axes ([0.5, 0.9, 0.20, 0.05])
        # self.draw_gaussiano_button = Button(self.zmap_button_ax, 'ZMap Gaussiano', color=self.button_color, hovercolor=self.button_hover)
        # self.draw_gaussiano_button.label.set_fontsize(self.button_font_size)
        # self.draw_gaussiano_button.on_clicked(self.toggle_Gaussiano)

        self.zmap_button_ax = self.figure.add_axes ([0.1, 0.9, 0.15, 0.05])
        self.draw_zmap_button = Button(self.zmap_button_ax, 'ZMap', color=self.button_color, hovercolor=self.button_hover)
        self.draw_zmap_button.label.set_fontsize(self.button_font_size)
        self.draw_zmap_button.on_clicked(self.toggle_zmap)

        self.mask_button_ax = self.figure.add_axes ([0.3, 0.9, 0.15, 0.05])
        self.draw_mask_button = Button(self.mask_button_ax, 'Mask', color=self.button_color, hovercolor=self.button_hover)
        self.draw_mask_button.label.set_fontsize(self.button_font_size)
        self.draw_mask_button.on_clicked(self.toggle_mask)
 
        self.overlay_alpha_ax = self.figure.add_axes([0.2, 0.85, 0.6, 0.02])
        self.overlay_alpha_slider = Slider(self.overlay_alpha_ax, 'Alpha', 0, 1, valinit=self.overlay_alpha)
        self.overlay_alpha_slider.on_changed(self.change_overlay_alpha)
 

        # create colorbars

        self.intensity_cmap = plt.get_cmap('Greys')
        intensity_norm = mcolors.Normalize(vmin=self.vol_data.min(), vmax=self.vol_data.max())
        self.intensity_cb = colorbar.ColorbarBase(self.intensity_cb_ax, cmap=self.intensity_cmap,
                                                spacing='proportional',
                                                norm=intensity_norm,
                                                orientation='vertical')
        # self.intensity_cb.set_label('Intensity')


        print ('ZMAP min max', self.zmap.min(), self.zmap.max())

        self.zmap_cmap = plt.get_cmap('coolwarm')
        zmap_norm = mcolors.Normalize(vmin=-self.zmap.max(), vmax=self.zmap.max())
        self.zmap_cb = colorbar.ColorbarBase(self.zmap_cb_ax, cmap=self.zmap_cmap,
                                            spacing='proportional',
                                            norm=zmap_norm,
                                            orientation='vertical')
        # self.zmap_cb.set_label('Zmap')


        if self.mask.max() > 0.0:
            self.mask_cmap = plt.get_cmap('tab20', self.mask.max())
        else:
            self.mask_cmap = plt.get_cmap('tab20')
        # mask_norm = mcolors.Normalize(vmin=0, vmax=self.mask.max())
        # self.mask_cb = colorbar.ColorbarBase(self.mask_cb_ax, cmap=self.mask_cmap,
        #                                     spacing='proportional',
        #                                     norm=mask_norm,
        #                                     orientation='vertical')
        # self.mask_cb.set_label('Mask')


        # adjust plots spacing
        self.figure.subplots_adjust(wspace=3.5, hspace=0.0)

        self.init_images()
        self.zorder_images()

    def change_overlay_alpha(self, val):
        """
            Change the overlay alpha value
        """
        self.overlay_alpha = val

        self.cor_zmap_im.set_alpha(val)
        self.tra_zmap_im.set_alpha(val)
        self.sag_zmap_im.set_alpha(val)
        self.cor_zmap_mask_im.set_alpha(val)
        self.tra_zmap_mask_im.set_alpha(val)
        self.sag_zmap_mask_im.set_alpha(val)
        self.cor_mask_im.set_alpha(val)
        self.tra_mask_im.set_alpha(val)
        self.sag_mask_im.set_alpha(val)


    def get_slice (self, axes):
        """
            Returns a slice given a pair of axis
            :param axes : axes id (yz, xz, xy)
            :return : slice
        """
        if axes == 0:
            return self.vol_data[self.slice_ids[axes], :, :]
        elif axes == 1:
            return self.vol_data[:, self.slice_ids[axes], :]
        elif axes == 2:
            return self.vol_data[:, :, self.slice_ids[axes]]
 
    def get_mask_slice (self, axes):
        """
            Returns a mask slice given a pair of axis
            :param axes : axes id
            :return : mask slice
        """
        if axes == 0:
            return self.mask[self.slice_ids[axes], :, :]
        elif axes == 1:
            return self.mask[:, self.slice_ids[axes], :]
        elif axes == 2:
            return self.mask[:, :, self.slice_ids[axes]]

    def get_mask_zmap_slice (self, axes):
        """
            Returns a zmap masked slice given a pair of axis
            :param axes : axes id
            :return : zmap mask slice
        """
        if axes == 0:
            return self.mask_zmap[self.slice_ids[axes], :, :]
        elif axes == 1:
            return self.mask_zmap[:, self.slice_ids[axes], :]
        elif axes == 2:
            return self.mask_zmap[:, :, self.slice_ids[axes]]

    def get_zmap_slice (self, axes):
        """
            Returns a zmap slice given a pair of axis
            :param axes : axes id
            :return : zmap slice
        """
        if axes == 0:
            return self.zmap[self.slice_ids[axes], :, :]
        elif axes == 1:
            return self.zmap[:, self.slice_ids[axes], :]
        elif axes == 2:
            return self.zmap[:, :, self.slice_ids[axes]]


    def get_sag_slice (self):
        return np.fliplr(self.slice_data[2]), np.fliplr(self.mask_slices[2]), np.fliplr(self.mask_zmap_slices[2]), np.fliplr(self.zmap_slices[2])
        #return self.slice_data[2], np.flipud(self.mask_slices[2]), np.flipud(self.mask_zmap_slices[2]), np.flipud(self.zmap_slices[2])

    def get_cor_slice (self):
        return np.flipud(np.rot90( self.slice_data[0], k=3)), np.flipud(np.rot90( self.mask_slices[0], k=3)), np.flipud(np.rot90( self.mask_zmap_slices[0], k=3)), np.flipud(np.rot90( self.zmap_slices[0], k=3))

    def get_tra_slice (self):
        return np.rot90(np.flip( self.slice_data[1], axis=0 ), k=1), np.rot90(np.flip( self.mask_slices[1], axis=0 ), k=1), np.rot90(np.flip( self.mask_zmap_slices[1], axis=0), k=1), np.rot90(np.flip( self.zmap_slices[1], axis=0), k=1)
        #return np.rot90( self.slice_data[1], k=1 ), np.rot90( self.mask_slices[1], k=1 ), np.rot90( self.mask_zmap_slices[1], k=1 ), np.rot90( self.zmap_slices[1], k=1 )


    def goto_slices (self, coords):
        """
            Change all three slices to given coords
        """
        self.slice_ids[0:3] = coords
        self.update_slices(0)
        self.update_slices(1)
        self.update_slices(2)

        self.plot()

    def draw_cursor (self, sag_coords, cor_coords, tra_coords):
        """
            Draw cursor position on all axes
        """
        if len(self.sag_ax.lines) > 0:
            self.sag_ax.lines.pop(0)
            self.sag_ax.lines.pop(0)
        if len(self.cor_ax.lines) > 0:
            self.cor_ax.lines.pop(0)
            self.cor_ax.lines.pop(0)
        if len(self.tra_ax.lines) > 0:
            self.tra_ax.lines.pop(0)
            self.tra_ax.lines.pop(0)

        self.sag_ax.add_line(mlines.Line2D([sag_coords[0], sag_coords[0]], [0,255], zorder=5))
        self.sag_ax.add_line(mlines.Line2D([0,255],[sag_coords[1], sag_coords[1]], zorder=5))

        self.cor_ax.add_line(mlines.Line2D([cor_coords[0], cor_coords[0]], [0,255], zorder=5))
        self.cor_ax.add_line(mlines.Line2D([0,255],[cor_coords[1], cor_coords[1]], zorder=5))

        self.tra_ax.add_line(mlines.Line2D([tra_coords[0], tra_coords[0]], [0,255], zorder=5))
        self.tra_ax.add_line(mlines.Line2D([0,255],[tra_coords[1], tra_coords[1]], zorder=5))



    def update_slices (self, axes):
        """
            Update slice and mask for given axes
        """
        self.slice_data[axes] = self.get_slice(axes)
        self.mask_slices[axes] = self.get_mask_slice(axes)
        self.mask_zmap_slices[axes] = self.get_mask_zmap_slice(axes)
        self.zmap_slices[axes] = self.get_zmap_slice(axes)

    def inc_slice_num (self, axes):
        """
            Increment current slice number of given axes by one
        """
        self.slice_ids[axes] += 1
        if self.slice_ids[axes] >= self.vol_data.shape[axes]:
            self.slice_ids[axes] = 0
        self.update_slices(axes)

    def dec_slice_num (self, axes):
        """
            Decrement current slice number of given axes by one
        """
        self.slice_ids[axes] -= 1
        if self.slice_ids[axes] < 0:
            self.slice_ids[axes] =  self.vol_data.shape[axes] - 1
        self.update_slices(axes)


    def voxel_value(self, coord):
        """
            Returns the voxel value at given position for currently selected volume (slice_num[3])
            :param coord: (x, y, z) voxel coordinates
            :returns : voxel value    
        """
        
        # check if click was inside image
        if all([coord[0] >= 0, coord[0] < self.vol_data.shape[0], coord[1] >= 0, coord[1] < self.vol_data.shape[1], coord[2] >= 0, coord[2] < self.vol_data.shape[2]]):
            return self.vol_data[coord[0], coord[1], coord[2]]
        return None


    def init_images (self):
        """
            Initialize all images, from then on we only update and change zorder instead of redrawing everything (for performance reasons)
        """
        from scipy.ndimage import gaussian_filter

        ## SAGITAL ##
        sl, ms, zms, zm = self.get_sag_slice()
        self.sag_ax.set_alpha(0.0)
        self.sag_ax.clear()
        self.sag_im = self.sag_ax.imshow(sl.T, cmap="gray", origin="lower", alpha=1.0, vmax=self.vol_data.max(), vmin=self.vol_data.min(), zorder=1)
        self.sag_zmap_mask_im = self.sag_ax.imshow(zms.T, cmap=self.zmap_cmap, origin="lower", interpolation="none", vmin=-self.zmap.max(), vmax=self.zmap.max(), zorder=2, alpha=self.overlay_alpha)
        self.sag_zmap_im = self.sag_ax.imshow(zm.T, cmap=self.zmap_cmap, origin="lower", interpolation="none", vmin=-self.zmap.max(), vmax=self.zmap.max(), zorder=3, alpha=self.overlay_alpha)
        self.sag_mask_im = self.sag_ax.imshow(ms.T, cmap=self.mask_cmap, origin="lower", interpolation="none", vmin=self.mask.min(), vmax=self.mask.max(), zorder=4, alpha=self.overlay_alpha)


        ## CORONAL ##
        sl, ms, zms, zm = self.get_cor_slice()
        self.cor_ax.set_alpha(0.0)
        self.cor_ax.clear()
        self.cor_ax.invert_xaxis()
        self.cor_im = self.cor_ax.imshow(sl.T, cmap="gray", origin="lower", alpha=1.0, vmax=self.vol_data.max(), vmin=self.vol_data.min(), zorder=1)
        self.cor_zmap_mask_im = self.cor_ax.imshow(zms.T, cmap=self.zmap_cmap, origin="lower", interpolation="none", vmin=-self.zmap.max(), vmax=self.zmap.max(), zorder=2, alpha=self.overlay_alpha)
        self.cor_zmap_im = self.cor_ax.imshow(zm.T, cmap=self.zmap_cmap, origin="lower", interpolation="none", vmin=-self.zmap.max(), vmax=self.zmap.max(), zorder=3, alpha=self.overlay_alpha)
        self.cor_mask_im = self.cor_ax.imshow(ms.T, cmap=self.mask_cmap, origin="lower", interpolation="none", vmin=self.mask.min(), vmax=self.mask.max(), zorder=4, alpha=self.overlay_alpha)

        sl, ms, zms, zm = self.get_tra_slice()
        self.tra_ax.set_alpha(0.0)
        self.tra_ax.clear()
        self.tra_im = self.tra_ax.imshow(sl.T, cmap="gray", origin="lower", alpha=1.0, vmax=self.vol_data.max(), vmin=self.vol_data.min(), zorder=1)
        self.tra_zmap_mask_im = self.tra_ax.imshow(zms.T, cmap=self.zmap_cmap, origin="lower", interpolation="none", vmin=-self.zmap.max(), vmax=self.zmap.max(), zorder=2, alpha=self.overlay_alpha)
        self.tra_zmap_im = self.tra_ax.imshow(zm.T, cmap=self.zmap_cmap, origin="lower", interpolation="none", vmin=-self.zmap.max(), vmax=self.zmap.max(), zorder=3, alpha=self.overlay_alpha)
        self.tra_mask_im = self.tra_ax.imshow(ms.T, cmap=self.mask_cmap, origin="lower", interpolation="none", vmin=self.mask.min(), vmax=self.mask.max(), zorder=4, alpha=self.overlay_alpha)

    def draw_sagittal (self):

        #from scipy.ndimage import gaussian_filter
        """
            Draws the Sagittal slice and masks
        """
        sl, ms, zms, zm = self.get_sag_slice()
        self.sag_ax.set_title('Sagittal\n' + str(self.slice_ids[2]+1) + '/' +  str(self.vol_data.shape[2]))
        #self.sag_im.set_data(sl.T)
        #gaussian_filter(self.sag_im.set_data(sl.T), sigma=5)
        if self.draw_zmap and self.draw_mask:
            self.sag_zmap_mask_im.set_data (zms.T)
        elif self.draw_zmap:
            self.sag_zmap_im.set_data (zm.T)
        elif self.draw_mask:
            self.sag_mask_im.set_data (ms.T)

    def draw_coronal (self):
        """
            Draws the Coronal slice and masks
        """
        sl, ms, zms, zm = self.get_cor_slice()
        self.cor_ax.set_title('Coronal\n' + str(self.slice_ids[0]+1) + '/' +  str(self.vol_data.shape[0]))
        self.cor_im.set_data(sl.T)
        if self.draw_zmap and self.draw_mask:
            self.cor_zmap_mask_im.set_data (zms.T)
        elif self.draw_zmap:
            self.cor_zmap_im.set_data (zm.T)
        elif self.draw_mask:
            self.cor_mask_im.set_data (ms.T)
        
    def draw_transverse (self):
        """
            Draws the Transverse slice and masks
        """
        sl, ms, zms, zm = self.get_tra_slice()
        self.tra_ax.set_title('Transverse\n' + str(self.slice_ids[1]+1) + '/' +  str(self.vol_data.shape[1]))
        self.tra_im.set_data(sl.T)
        if self.draw_zmap and self.draw_mask:
            self.tra_zmap_mask_im.set_data (zms.T)
        if self.draw_zmap:
            self.tra_zmap_im.set_data (zm.T)
        if self.draw_mask:
            self.tra_mask_im.set_data (ms.T)

    def plot (self, axis='all'):



        """
            Draws all three slices
        """
        if axis=='sag' or axis=='all':
            self.draw_sagittal ()
        if axis=='cor' or axis=='all':
            self.draw_coronal ()
        if axis=='tra' or axis=='all':
            self.draw_transverse ()


    def zorder_images (self):

        # send everything back (not visible)
        self.sag_zmap_mask_im.zorder = 0
        self.cor_zmap_mask_im.zorder = 0
        self.tra_zmap_mask_im.zorder = 0
        self.sag_zmap_im.zorder = 0
        self.cor_zmap_im.zorder = 0
        self.tra_zmap_im.zorder = 0
        self.sag_mask_im.zorder = 0
        self.cor_mask_im.zorder = 0
        self.tra_mask_im.zorder = 0
 
        if self.draw_zmap and self.draw_mask:
            self.sag_zmap_mask_im.zorder = 2
            self.cor_zmap_mask_im.zorder = 2
            self.tra_zmap_mask_im.zorder = 2
        elif self.draw_zmap:
            self.sag_zmap_im.zorder = 2
            self.cor_zmap_im.zorder = 2
            self.tra_zmap_im.zorder = 2
        elif self.draw_mask:
            self.sag_mask_im.zorder = 2
            self.cor_mask_im.zorder = 2
            self.tra_mask_im.zorder = 2



    # def toggle_Gaussiano (self, widget):
    #     from scipy.ndimage import gaussian_filter
    #
    #     self.draw_zmap = gaussian_filter(not self.draw_zmap, sigma=5)
    #
    #     if self.draw_zmap and self.draw_mask:
    #         self.draw_gaussiano_button.color = self.button_color
    #     else:
    #         self.draw_gaussiano_button.color = self.button_alt_color
    #     self.zorder_images()
    #
    #
    #     self.plot()


    def toggle_zmap (self, widget):
        self.draw_zmap = not self.draw_zmap

        if self.draw_zmap and self.draw_mask:
            self.draw_zmap_button.color = self.button_color
        else:
            self.draw_zmap_button.color = self.button_alt_color
        self.zorder_images()
        self.plot()

    def toggle_mask (self, widget):
        self.draw_mask = not self.draw_mask

        if self.draw_mask:
            self.draw_mask_button.color = self.button_color
        else:
            self.draw_mask_button.color = self.button_alt_color
        self.zorder_images()
        self.plot()
