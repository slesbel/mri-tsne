#
# conditions.py
#
# Plot conditions over time
# allows selecting scans on conditions plot
#
# Created by Ricardo Marroquim on 14-02-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import sys
import matplotlib as mpl
from matplotlib.widgets import RadioButtons
from scipy import stats
import numpy as np

sys.path.append('../')
from fMRIVis.datastructs import protocol

class ProtocolPlot():
    """
        Methods for plotting the protocol (conditions over time) using matplotlib
    """

    def __init__(self):
        """
        """        
        self.protocol = protocol.Protocol()
        self.selection = []
        self.alpha = 1.0

    def clear_selection (self):
        """
        Clears selected scans
        """
        self.selection = np.zeros(len(self.selection))

    def set_protocol (self, prt):
        """
            Sets the associate protocol for the time course
        """
        self.protocol = prt

    def set_num_scans (self, num):
        """
            Sets the number of different volumes/scans
        """
        self.selection = np.zeros(num)
        print ("protocol plot num scans", self.selection.shape)

    def init_scatter_plot (self, axis):
        """
            Initializes axes for plotting conditions and selected scans
            If there is an associated protocol, create colobar from conditions
        """

        # voxel time course inspection window
        self.inspection_ax = axis


    def init_colorbar (self, axis):
        """
            Initializes the colorbar with conditions colors and names
            Creates the colorbar for labels based on the associated protocol
        """

        if self.protocol.number_conditions > 0:
            labels_colors = []
            labels_names = []
            # create lists with conditions colors and names
            for c in self.protocol.conditions:
                labels_colors.append(c.color)
                labels_names.append(c.name) 

            labels_cmap = mpl.colors.ListedColormap(labels_colors)
            bounds = list(np.arange(1, len(labels_colors)+2))
            norm = mpl.colors.BoundaryNorm(bounds, labels_cmap.N)
            labels_colorbar= mpl.colorbar.ColorbarBase(axis, cmap=labels_cmap, norm=norm,
                      boundaries=bounds,
                      spacing='proportional',
                      orientation='vertical')
            axis.yaxis.set_label_position('left')
            axis.set_ylabel('conditions', rotation=90)
            axis.yaxis.set_major_formatter(mpl.ticker.NullFormatter())

            # ticks positions for labels
            ticks = []
            for t in range(self.protocol.number_conditions):
                ticks.append (t*(1.0/self.protocol.number_conditions)+0.5/self.protocol.number_conditions
    )
            axis.yaxis.set_major_locator(mpl.ticker.FixedLocator(ticks))

            # condition names
            axis.yaxis.set_major_formatter(mpl.ticker.FixedFormatter(labels_names))


    def plot (self, tc = []):
        """
            Plot conditions with selected scans
        """
        self.inspection_ax.clear()

        self.inspection_ax.set_facecolor(self.protocol.bg_color)

        self.plot_conditions()
        self.plot_selection()

        self.inspection_ax.figure.canvas.draw()


    def plot_conditions (self):
        """
            Plot condition (label) stripes
        """
        alpha = 1.0
        darken = 0.8
        #for each condition
        for condition in self.protocol.conditions:
            #plots its intervals with given color
            for interval in condition.intervals:
                color = [x*darken for x in condition.color]
                self.inspection_ax.axvspan(interval[0]-1, interval[1], alpha=self.alpha, color=color, zorder=1)

    def plot_selection (self):
        """
            Plot selected scans
        """
        #for each selection
        count = 0
        for count, s in enumerate(self.selection):
            if s:
                color = [0.8, 0.8, 0.8]
                self.inspection_ax.axvspan(count-1, count+1, alpha=self.alpha, color=color, zorder=2)
