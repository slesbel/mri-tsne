"""
Plots for fMRI data

Modules:

      * ``fit_plot`` - Plot the fit parameters of a surface mesh patch
      * ``hog_plot`` - Histogram of Gradients plot

"""
