#
# fit_analysis_plot.py
#
# Viewer for a flatten surface patch (projected to a plane), as contours
#
# Created by Ricardo Marroquim on 14-03-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import sys
import numpy as np
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.widgets import RadioButtons, Slider, Button
from matplotlib import colors as mcolors
from mpl_toolkits.mplot3d import Axes3D
from scipy import stats, spatial
from scipy.optimize import curve_fit

sys.path.append('../')
from fMRIVis.datastructs import srf
from fMRIVis.models import transformations

class FitAnalysisPlot:
    """
        Plot to analyse fit errors
    """

    def __init__(self, srf, smp, size = [1500, 600], name=''):
        """
            Initializes Surface Viewer class
        """
        self.size = size
        self.name = name
        self.smp = smp

        self.map_chisqr = []
        self.map_redchi = []
        self.map_aic = []
        self.map_bic = []
        self.map_center = []
        self.map_amplitude = []
        self.map_sigma = []

        self.planar_points = []
       
        self.number_contours = 20

        self.error_colormap = plt.get_cmap('coolwarm')
        #decorations
        self.button_font_size = 9
        self.button_color = 'skyblue'
        self.button_hover = '0.975'


        #smooth surface
        #smooth_srf = transformations.laplacian_smooth (srf, iterations=20)

        # original surface points
        self.srf_points = np.zeros(shape=(srf.number_verts,3))
        for i in range (srf.number_verts):
            self.srf_points[i] = [srf.verts[i*3+0], srf.verts[i*3+1], srf.verts[i*3+2]]

        # project surface points to plane
#        self.planar_points = transformations.planar_projection(self.srf_points)
        #self.planar_points = transformations.isomap_projection(self.srf_points)
        #self.planar_points = transformations.stereographic_projection(self.srf_points)

        self.planar_points = self.srf_points
        # instance a matplotlib triangulation for the planar points
        tris = np.reshape(srf.triangles, (-1, 3))
        #tris = np.flip(tris, axis=1)

        print (self.planar_points.shape)

        self.triangulation = mpl.tri.Triangulation (x=self.planar_points[:,0], y=self.planar_points[:,1], triangles=tris)
 
        # kdtree for querying closest point
        self.kdtree = spatial.cKDTree(self.planar_points[:,0:2])

    def init_analysis_ax (self, fig):
        """
            Initializes the analysis axis
        """
        
        self.figure = fig
        self.analysis_ax = []
        self.analysis_ax.append( self.figure.add_subplot (2, 2, 1) )
        self.analysis_ax.append( self.figure.add_subplot (2, 2, 2) )
        self.analysis_ax.append( self.figure.add_subplot (2, 2, 3) )
        self.analysis_ax.append( self.figure.add_subplot (2, 2, 4) )

        for ax in self.analysis_ax:
            ax.set_facecolor('darkseagreen')

    def point_data (self, pt):
        """
            Prints values for specific point on map
        """

        dist, index = self.kdtree.query(pt)
       
        data = []
        for i, m in enumerate(self.smp.maps):
            data.append( m.data[index] )
    
        data = np.asarray (data)
        return data


    def plot (self):
        """
            Plot surface patch as contours
        """

        for ax in self.analysis_ax:
            ax.clear()

        if self.map_chisqr != []:
            self.analysis_ax[0].set_title('Chi-Square fit')
            im = self.analysis_ax[0].tricontourf(self.triangulation, self.map_chisqr, self.number_contours, cmap=self.error_colormap)
            plt.colorbar(im, ax=self.analysis_ax[0])

        if self.map_center != []:
            self.analysis_ax[1].set_title('Gaussian center')
            clipped = np.clip(self.map_center, -10.0, 10.0)
            for i, c in enumerate(self.map_center):
                clipped[i] = math.log(abs(c))*np.sign(c)
            im = self.analysis_ax[1].tricontourf(self.triangulation, clipped, self.number_contours, cmap=self.error_colormap)
            plt.colorbar(im, ax=self.analysis_ax[1])

        if self.map_sigma != []:
            self.analysis_ax[2].set_title('Gaussian sigma')
            clipped = np.clip(self.map_sigma, 0.0, 10.0)
            for i, c in enumerate(self.map_sigma):
                clipped[i] = math.log(abs(c))*np.sign(c)
            im = self.analysis_ax[2].tricontourf(self.triangulation, clipped, self.number_contours, cmap=self.error_colormap)
            plt.colorbar(im, ax=self.analysis_ax[2])

        if self.map_amplitude != []:
            self.analysis_ax[3].set_title('Gaussian amplitude')
            clipped = np.clip(self.map_amplitude, 0.0, 100.0)
            for i, c in enumerate(self.map_amplitude):
                clipped[i] = math.log(abs(c))*np.sign(c)
            im = self.analysis_ax[3].tricontourf(self.triangulation, clipped, self.number_contours, cmap=self.error_colormap, vmin=0.0, vmax=110.0)
            plt.colorbar(im, ax=self.analysis_ax[3])


