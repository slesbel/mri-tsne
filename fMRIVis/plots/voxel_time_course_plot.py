#
# voxel_plot.py
#
# Plots the voxel values (time course) 
#
# Created by Ricardo Marroquim on 16-02-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#
import gi
gi.require_version('Gtk', '3.0')

from gi.repository import Gtk
import sys
import matplotlib as mpl
from matplotlib.widgets import RadioButtons
from scipy import stats
import numpy as np

sys.path.append('../')
from fMRIVis.datastructs import protocol

class VoxelData():

    def __init__ (self, data=[], coord=[0,0,0]):
        """
            Initializes the voxel data
        """
        self.data = data
        self.zscore = []
        if len(self.data) > 0:
            self.zscore = stats.zscore(data)
        self.coord = coord

    def load_voxel_data (self, d, coord):
        """
            Load the selected scan data from numpy array
        """
        self.data = d
        self.zscore = stats.zscore(d)
        self.coord = coord


class VoxelTimeCoursePlot():
    """
        Methods for plotting the selected elements of a single scan
    """

    def __init__(self):
        """
        """
        self.voxel = VoxelData()
        self.protocol = protocol.Protocol()

    def set_voxel_data (self, vox):
        """
            Sets the selected data for plotting
        """
        self.voxel = vox

    def set_protocol (self, prt):
        """
            Sets the associate protocol for the experiment
        """
        self.protocol = prt

    def change_type (self, connection_id):
        """
            Change visualization type
        """
        self.plot ()


    def init_plot (self, axis):
        """
            Initializes axes for plotting the data
        """
        self.inspection_ax = axis

    def init_type_button (self, axis):
        """
            Initializes the button to change plot scale (value/zscore)
        """
        self.type_button = RadioButtons(axis, ('value', 'zscore'))
        self.type_button.activecolor = 'skyblue'
        self.type_button.on_clicked(self.change_type)

    def plot_conditions (self):
        """
            Plot condition (label) stripes
        """
        alpha = 1.0
        darken = 0.8
        #for each condition
        for condition in self.protocol.conditions:
            #plots its intervals with given color
            for interval in condition.intervals:
                color = [x*darken for x in condition.color]
                self.inspection_ax.axvspan(interval[0]-1, interval[1], alpha=alpha, color=color, zorder=1)


    def plot (self, vox = []):
        """
            Plot selected data of a voxel
            Draws background as corresponding condition of the voxel
        """
        if vox:
            self.voxel = vox

        self.inspection_ax.clear()

        thick = 1

#        # line color depends on bg luminance, to enhance contrast
#        luminance = 0.2126*condition.color[0] + 0.7152*condition.color[1] + 0.0722*condition.color[2]
#        if luminance > 0.5:
#            color = 'black'
#        else:
#            color = 'white'
#
        color = 'black'
        if self.type_button.value_selected == 'value':
            self.inspection_ax.plot(self.voxel.data, linewidth=thick, color=color, zorder=2)
        elif self.type_button.value_selected == 'zscore':
            self.inspection_ax.plot(self.voxel.zscore, linewidth=thick, color=color, zorder=2)

        self.plot_conditions()
        self.inspection_ax.set_title(str(self.voxel.coord))
        self.inspection_ax.figure.canvas.draw()


def main():
    Gtk.main()
    return 0


if __name__ == "__main__":
    VoxelData()
    VoxelTimeCoursePlot()
    main()
