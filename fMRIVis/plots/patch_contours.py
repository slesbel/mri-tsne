#
# pacth_viewer.py
#
# Viewer for a flatten surface patch (projected to a plane), as contours
#
# Created by Ricardo Marroquim on 12-03-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import sys
import numpy as np
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.widgets import RadioButtons, Slider, Button
from matplotlib import colors as mcolors
from mpl_toolkits.mplot3d import Axes3D
from scipy import stats, spatial
from scipy.optimize import curve_fit

sys.path.append('../')
from fMRIVis.datastructs import srf
from fMRIVis.models import transformations

class PatchContours:
    """
        Surface Pach Viewer as Contours
    """

    def __init__(self, srf, smp, size = [1500, 600], name='sweet'):
        """
            Initializes Surface Viewer class
        """
        self.size = size
        self.name = name

        # Surface patch representation (geometry)
        self.srf = srf

        self.smp = smp
        self.map_chisqr = []
        self.map_redchi = []
        self.map_aic = []
        self.map_bic = []

        self.planar_points = []
       
        self.number_contours = 10

        self.colormap = plt.get_cmap('hot')
        self.error_colormap = plt.get_cmap('coolwarm')
        #decorations
        self.button_font_size = 9
        self.button_color = 'skyblue'
        self.button_hover = '0.975'

        self.srf_points = np.zeros(shape=(self.srf.number_verts,3))
        for i in range (self.srf.number_verts):
            self.srf_points[i] = [self.srf.verts[i*3+0], self.srf.verts[i*3+1], self.srf.verts[i*3+2]]
        self.planar_points = transformations.planar_projection(self.srf_points)

        tris = np.reshape(self.srf.triangles, (-1, 3))
        self.triangulation = mpl.tri.Triangulation (x=self.planar_points[:,0], y=self.planar_points[:,1], triangles=tris)
 
        # kdtree for querying closest point
        self.kdtree = spatial.cKDTree(self.planar_points[:,0:2])

    def init_analysis_ax (self, ax):
        """
            Initializes the analysis axis
        """
        self.analysis_ax = ax
        for ax in self.analysis_ax:
            ax.set_facecolor('darkseagreen')
 
    def init_colorbar_ax (self, ax):
        """
            Initializes the colobar axis
        """
        self.colorbar_ax = ax
        mpl.colorbar.ColorbarBase(self.colorbar_ax, cmap=self.colormap,
                  spacing='proportional',
                  orientation='vertical')
 

    def init_contours_plot (self, ax):
        """
            Initialize tsne plot
        """
        self.conditions_ax = ax

        for ax in self.conditions_ax:            
            ax.set_facecolor('darkseagreen')

 
    def init_3d_plot (self, ax):
        """
            Initialize tsne plot
        """
        self.plot_3d_ax = ax
        ax.set_facecolor('darkseagreen')


    def point_data (self, pt):
        """
            Prints values for specific point on map
        """

        dist, index = self.kdtree.query(pt)
        print (pt)
        print (self.planar_points[index,:])
        print (self.srf_points[index,:])

        data = []
        for i, m in enumerate(self.smp.maps):
            data.append( m.data[index] )
    
        data = np.asarray (data)
        return data

       

    def plot (self):
        """
            Plot surface patch as contours
        """
        for i, ax in enumerate(self.conditions_ax):
            ax.set_title(self.name + ' ' + str(i+1))
            ax.tricontourf(self.triangulation, self.smp.maps[i].data, self.number_contours, cmap=self.colormap)

        self.plot_analysis()


    def plot_analysis (self):

        # concatenate all maps
        maps = []
        for m in self.smp.maps:
            maps.append (m.data)
        maps = np.asarray(maps)

        # sum maps
        map_sum = np.zeros(shape=self.smp.maps[0].data.shape)
        for m in self.smp.maps:
            map_sum += m.data

        # mean
        map_mean = map_sum / float(self.smp.number_maps)

        # kurtosis
        map_kurtosis = stats.kurtosis(maps, axis=0, fisher=True, bias=True)


        # variance
        map_variance = np.var(maps, axis=0)
       
        # moments
        map_moment_3 = stats.moment(maps, moment=3, axis=0)
        map_moment_4 = stats.moment(maps, moment=4, axis=0)
    
        # std deviation
        map_std = np.std(maps, axis=0)

        # skewness
        map_skewness = stats.skew(maps, axis=0)

        # variation
        map_variation = stats.variation(maps, axis=0)

        # zscore
        map_zscore = stats.zscore(maps, axis=0)

        # Anderson and k-stat
        map_anderson_norm = np.zeros(shape=maps.shape[1])
        map_anderson_expon = np.zeros(shape=maps.shape[1])
        map_anderson_gumbel = np.zeros(shape=maps.shape[1])
        map_anderson_logistic = np.zeros(shape=maps.shape[1])
        map_kstat_1 = np.zeros(shape=maps.shape[1])
        map_kstat_2 = np.zeros(shape=maps.shape[1])
        map_kstat_3 = np.zeros(shape=maps.shape[1])
        map_kstat_4 = np.zeros(shape=maps.shape[1])
        for i in range(maps.shape[1]):
            map_anderson_norm[i] = stats.anderson(maps[:,i], dist='norm')[0]
            map_anderson_expon[i] = stats.anderson(maps[:,i], dist='expon')[0]
            map_anderson_logistic[i] = stats.anderson(maps[:,i], dist='logistic')[0]
            map_anderson_gumbel[i] = stats.anderson(maps[:,i], dist='gumbel')[0]
            map_kstat_1[i] = stats.kstat(maps[:,i], n=1)
            map_kstat_2[i] = stats.kstat(maps[:,i], n=2)
            map_kstat_3[i] = stats.kstat(maps[:,i], n=3)
            map_kstat_4[i] = stats.kstat(maps[:,i], n=4)


        self.analysis_ax[0].set_title('mean')
        self.analysis_ax[0].tricontourf(self.triangulation, map_mean, self.number_contours, cmap=self.colormap)


        self.analysis_ax[1].set_title('standard deviation')
        self.analysis_ax[1].tricontourf(self.triangulation, map_std, self.number_contours, cmap=self.colormap)

        self.analysis_ax[2].set_title('variance')
        self.analysis_ax[2].tricontourf(self.triangulation, map_variance, self.number_contours, cmap=self.colormap)

        self.analysis_ax[3].set_title('skewness')
        self.analysis_ax[3].tricontourf(self.triangulation, map_skewness, self.number_contours, cmap=self.colormap)

        self.analysis_ax[4].set_title('3rd moment')
        self.analysis_ax[4].tricontourf(self.triangulation, map_moment_3, self.number_contours, cmap=self.colormap)


        self.analysis_ax[5].set_title('4th moment')
        self.analysis_ax[5].tricontourf(self.triangulation, map_moment_4, self.number_contours, cmap=self.colormap)

#        self.analysis_ax[6].set_title('kurtosis')
#        self.analysis_ax[6].tricontourf(self.triangulation, map_kurtosis, self.number_contours, cmap=self.colormap)

#        self.analysis_ax[6].set_title('Anderson Norm')
#        self.analysis_ax[6].tricontourf(self.triangulation, map_anderson_norm, self.number_contours, cmap=self.colormap)
#
##        self.analysis_ax[7].set_title('2nd k-statistic')
##        self.analysis_ax[7].tricontourf(self.triangulation, map_kstat_2, self.number_contours, cmap=self.colormap)
#
#        self.analysis_ax[7].set_title('3rd k-statistic')
#        self.analysis_ax[7].tricontourf(self.triangulation, map_kstat_3, self.number_contours, cmap=self.colormap)
#
#        self.analysis_ax[8].set_title('4th k-statistic')
#        self.analysis_ax[8].tricontourf(self.triangulation, map_kstat_4, self.number_contours, cmap=self.colormap)
#
        if self.map_chisqr != []:
            self.analysis_ax[6].set_title('chi-square fit')
            self.analysis_ax[6].tricontourf(self.triangulation, self.map_chisqr, self.number_contours, cmap=self.error_colormap)

        self.analysis_ax[7].set_title('reduced chi-square fit')
        if self.map_redchi != []:
            self.analysis_ax[7].tricontourf(self.triangulation, self.map_redchi, self.number_contours, cmap=self.error_colormap)

        if self.map_aic != []:
            self.analysis_ax[8].set_title('Akaike Info Criterion')
            self.analysis_ax[8].tricontourf(self.triangulation, self.map_aic, self.number_contours, cmap=self.error_colormap)

        if self.map_aic != []:
            self.analysis_ax[9].set_title('Bayesian Info Criterion')
            self.analysis_ax[9].tricontourf(self.triangulation, self.map_bic, self.number_contours, cmap=self.error_colormap)


