#
# fit_plot.py
#
# Visualize diverse fitting models for given sample data
#
# Created by Ricardo Marroquim on 13-03-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import sys
import numpy as np
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.widgets import RadioButtons, Slider, Button
from matplotlib import colors as mcolors
from mpl_toolkits.mplot3d import Axes3D
from scipy import stats, spatial
from scipy.optimize import curve_fit
from scipy.stats import norm
from lmfit.models import GaussianModel

sys.path.append('../')
from fMRIVis.datastructs import srf
from fMRIVis.vis import glutils

class SampleData:
    """
        Data values and statistical values for sample
    """

    def __init__ (self, data=[]):

        self.data = data
        self.base = []

        self.base_size = 50

        self.mean = 0.0
        self.kurtosis = 0.0
        self.variance = 0.0
        self.std = 0.0
        self.skewness = 0.0
        self.variation = 0.0
        self.anderson_norm = 0.0
        self.anderson_expon = 0.0
        self.anderson_gumbel = 0.0
        self.anderson_logistic = 0.0
        self.kstat = [0.0, 0.0, 0.0, 0.0]
        self.moment = [0.0, 0.0, 0.0, 0.0]

        self.gauss_model = GaussianModel()

        self.compute_stats()

    def compute_stats (self, data=[]):
        if len(data) > 0:
            self.data = data

        if len(self.data) == 0:
            return

        self.mean = np.mean(self.data)
        self.kurtosis = stats.kurtosis(self.data, fisher=True, bias=True)
        self.variance = stats.tvar(self.data)
        self.std = stats.tstd(self.data)
        self.skewness = stats.skew(self.data)
        self.variation = stats.variation(self.data)
        self.anderson_norm = stats.anderson(self.data, dist='norm')
        self.anderson_expon = stats.anderson(self.data, dist='expon')
        self.anderson_logistic = stats.anderson(self.data, dist='logistic')
        self.anderson_gumbel = stats.anderson(self.data, dist='gumbel')

        for i in range(1,5):
            self.moment = stats.moment(self.data, moment=i)
            self.kstat = stats.kstat(self.data, n=i)

        
    def fit_data (self):
        self.base = np.linspace(1, len(self.data), self.base_size)
        self.data_x = np.arange(1,len(self.data)+1)
        self.data = self.data - self.mean
        self.data = self.data/self.std
        self.data = self.data - np.min(self.data)

        init_params = self.gauss_model.guess(self.data, x=self.data_x)

        self.gauss_fit = self.gauss_model.fit (data=self.data, params=init_params, x=self.data_x)

        best_params = self.gauss_fit.params
        self.gauss_init_fit = self.gauss_model.eval (init_params, x=self.base)
        self.gauss_final_fit = self.gauss_model.eval (best_params, x=self.base)

        self.gauss_fit_mu, self.gauss_fit_std = norm.fit(self.data)
        self.gauss_data = norm.pdf(self.base, self.gauss_fit_mu, self.gauss_fit_std)
        self.gauss_data = norm.pdf(self.base)
        
class FitPlot:
    """
        Plots fitting models for sample data
    """

    def __init__(self, sample1=SampleData(), sample2=SampleData(), size=[1500, 600], name=''):
        """
            Initializes Fit plot class
        """
        self.size = size
        self.name = name

        self.sample1 = sample1
        self.sample2 = sample2

        self.colormap = plt.get_cmap('hot')
        #decorations
        self.button_font_size = 9
        self.button_color = 'skyblue'
        self.button_hover = '0.975'


    def init_fit_plot (self, ax):
        """
            Initialize axes for plotting
        """
        self.fit_ax = ax

        for ax in self.fit_ax:            
            ax.set_facecolor('darkseagreen')

    def set_data (self, data1, data2=[]):
        self.sample1 = SampleData(data1)
        self.sample1.fit_data()

        if data2 != []:
            self.sample2 = SampleData(data2)
            self.sample2.fit_data()

    def plot (self):
        """
            Plot fit models
        """

        for i, ax in enumerate(self.fit_ax):
            ax.clear()
            if i == 0:
                ax.set_title('Sweet sample')
                ax.plot(self.sample1.data_x, self.sample1.data, 'bo')
                #ax.plot(self.sample1.base, self.sample1.gauss_init_fit, 'k--')
                ax.plot(self.sample1.base, self.sample1.gauss_final_fit, 'r--')
            if i == 1 and len(self.sample2.data) > 0:
                ax.set_title('Bitter sample')
                ax.plot(self.sample2.data_x, self.sample2.data, 'bo')
                #ax.plot(self.sample2.base, self.sample2.gauss_init_fit, 'k--')
                ax.plot(self.sample2.base, self.sample2.gauss_final_fit, 'r--')


