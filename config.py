#
# config.py
#
# Reads and handles config file
#
# Created by Ricardo Marroquim on 26-03-2018
# Copyright (c) 2018 Federal University of Rio de Janeiro. All rights reserved
#

import sys
import os
import configparser

class MriConfig:
    """
    Class to handle config files
    """

    def __init__ (self, filename=None):
        """
        Init for config class
        """
        self.config = configparser.ConfigParser()
        self.subject = ''
        self.data_path = ''
        self.taste = ''
        self.condition = ''
        self.resolution = '' # high/low
        self.zmap_threshold = 0
        self.min_cluster_vol = 0
        self.num_pre = 0
        self.num_center = 0
        self.mask_regions = []
        self.mapname = ''

        if filename is not None:
            self.read_file(filename)

    def read_file (self, filename):
        """
        Reads a config file
        """
        self.config_file = filename
        if not os.path.isfile(self.config_file):
            print ("Misssing config file")
            return False

        self.config.read(self.config_file)
        self.data_path = self.config['Paths']['data']
        self.subject = self.config['DEFAULT']['subject']
        self.mask_regions = []
        self.taste = self.config['DEFAULT']['taste']
        self.condition = self.config['DEFAULT']['condition']
        self.resolution = self.config['DEFAULT']['resolution']
        self.zmap_threshold = float(self.config['DEFAULT']['zmap_threshold'])
        self.min_cluster_vol = int(self.config['DEFAULT']['min_cluster_vol'])
        self.num_pre = int(self.config['DEFAULT']['num_pre'])
        self.num_center = int(self.config['DEFAULT']['num_center'])
        
        return True

    def parse_arguments (self, args):
        """
            Parse arguments
        """
        self.arg_list = self.config['DEFAULT']

        for arg in args:
            argname = arg.split('=')[0]
            
            if argname in self.arg_list:
                argvalue = arg.split('=')[1]
                if argname == 'regions':
                    self.mask_regions = argvalue
                if argname == 'subject':
                    self.subject = argvalue
                elif argname == 'taste':
                    self.taste = argvalue
                elif argname == 'condition':
                    self.condition = argvalue
                elif argname == 'resolution':
                    self.resolution = argvalue
                elif argname == 'zmap_threshold':
                    self.zmap_threshold = float(argvalue)
                elif argname == 'min_cluster_vol':
                    self.min_cluster_vol = int(argvalue)
                elif argname == 'num_pre':
                    self.num_pre = int(argvalue)
                elif argname == 'num_center':
                    self.num_center = int(argvalue)
                elif argname == 'mask_regions':
                    self.mask_regions = [int(x) for x in argvalue.split(',')]


    def subject_data_path (self, subject=None):
        """
            Returns the data path of a subject
        """
        return self.data_path + subject + '/'

    def fmri_va_path (self):
        """
            Returns the fMRI Visual Analytics Lib path
        """
        return self.config['Paths']['fmri_visual_analytics']

    def mask_file (self, resolution='low'):
        """
        Composes a MASK filename
        """

        if resolution == 'low':
            return (self.data_path + self.config['Filenames']['mask_lowres'])
        elif resolution == 'high':
            return (self.data_path + self.config['Filenames']['mask_highres'])

    def voi_file (self):
        """
        Composes a VOI filename
        """

        return (self.data_path + self.config['Filenames']['voi'])

    def contrasts_file (self, taste='sweet'):
        """
            Returns the contrasts filename
        """
        return self.data_path + self.config['Filenames']['contrasts'].replace('TASTENAME', taste)

    def sdm_file (self, taste='sweet', subject=None):
        """
            Returns the SDM (design matrix) filename
        """
        if subject is None: 
            subject = self.subject
 
        return self.data_path + subject + '/' + self.config['Filenames']['sdm'].replace('SUBJECTNAME', subject).replace('TASTENAME', taste[0:3])


    def vmr_file (self, taste='sweet', subject=None):
        """
        Composes a VMR filename for given taste
        """    
        if subject is None: 
            subject = self.subject
        return (self.data_path + subject + '/' + self.config['Filenames']['vmr']).replace('SUBJECTNAME', subject).replace('TASTENAME', taste[0:3])

    def vtc_file (self, taste='sweet', subject=None):
        """
        Composes a VTC filename for given taste
        """    
        if subject is None: 
            subject = self.subject
        return (self.data_path + subject + '/' + self.config['Filenames']['vtc']).replace('SUBJECTNAME', subject).replace('TASTENAME', taste[0:3])


    def zmap_file (self, taste='sweet', subject=None, contrast='c1_sweet', highres=False):
        """
        Composes a ZMAP filename for given taste
        """    
        if subject is None: 
            subject = self.subject
        if highres:
            return (self.data_path + subject + '/' + self.config['Filenames']['zmap_highres']).replace('SUBJECTNAME', subject).replace('TASTENAME', taste[0:3]).replace('CONTRASTNAME', contrast)
        else:
            return (self.data_path + subject + '/' + self.config['Filenames']['zmap']).replace('SUBJECTNAME', subject).replace('TASTENAME', taste[0:3]).replace('CONTRASTNAME', contrast)

    def zmap_mask_file (self, taste='sweet', subject=None, contrast='c1_sweet', highres=False):
        """
        Composes a ZMAP binary mask filename for given taste
        """    
        if subject is None: 
            subject = self.subject
        if highres:
            return (self.data_path + subject + '/' + self.config['Filenames']['zmap_mask_highres']).replace('SUBJECTNAME', subject).replace('TASTENAME', taste[0:3]).replace('CONTRASTNAME', contrast)
        else:
            return (self.data_path + subject + '/' + self.config['Filenames']['zmap_mask']).replace('SUBJECTNAME', subject).replace('TASTENAME', taste[0:3]).replace('CONTRASTNAME', contrast)

    def srf_D80K_file (self, subject=None):
        """
        Composes a SRF D80K filename
        """
        if subject is None: 
            subject = self.subject
        return (self.data_path + subject + '/' + self.config['Filenames']['srf_D80K']).replace('SUBJECTNAME', subject)

    def srf_SPH_file (self, subject=None):
        """
        Composes a SRF SPH filename
        """
        if subject is None: 
            subject = self.subject
        return (self.data_path + subject + '/' + self.config['Filenames']['srf_SPH']).replace('SUBJECTNAME', subject)

    def poi_file (self, subject=None):
        """
        Composes a POI filename
        """       
        if subject is None: 
            subject = self.subject
        return (self.data_path + subject + '/' + self.config['Filenames']['poi']).replace('SUBJECTNAME', subject)

    def smp_file (self, taste='sweet', subject=None):
        """
        Composes a SMP filename for given taste
        """       
        if subject is None: 
            subject = self.subject
        return (self.data_path + subject + '/' + self.config['Filenames']['smp']).replace('SUBJECTNAME', subject).replace('TASTENAME', taste)

    def ssm_D80K2SPH_file (self, subject=None):
        """
        Composes a SSM from subject D80K to subject SPH filename
        """       
        if subject is None: 
            subject = self.subject
        return (self.data_path + subject + '/' + self.config['Filenames']['ssm_D80K2SPH']).replace('SUBJECTNAME', subject)


    def mtc_file (self, taste='sweet', subject=None):
        """
        Composes a MTC filename for given taste
        """    
        if subject is None: 
            subject = self.subject
        return (self.data_path + subject + '/' + self.config['Filenames']['mtc']).replace('SUBJECTNAME', subject).replace('TASTENAME', taste[0:3])

    def protocol_file (self, taste='sweet', subject=None):
        """
        Composes a PTR (Protocol) filename for given taste
        """    
        if subject is None: 
            subject = self.subject
        return (self.data_path + subject + '/' + self.config['Filenames']['protocol']).replace('SUBJECTNAME', subject).replace('TASTENAME', taste.capitalize())

    def protocol_sub_file (self, taste='sweet', subject=None):
        """
        Composes a PTR (Protocol) filename for given taste
        """    
        if subject is None: 
            subject = self.subject
        return (self.data_path + subject + '/' + self.config['Filenames']['protocol_sub']).replace('SUBJECTNAME', subject).replace('TASTENAME', taste.capitalize())



    def patch_srf_D80K_file (self, subject=None):
        """
        Composes a patch Surface file for D80K mesh
        """
        if subject is None: 
            subject = self.subject
        return (self.data_path + subject + '/' + self.config['Filenames']['srf_D80K_patch']).replace('SUBJECTNAME', subject)

    def patch_srf_SPH_file (self, subject=None):
        """
        Composes a patch Surface file for SPH mesh
        """
        if subject is None: 
            subject = self.subject
        return (self.data_path + subject + '/' + self.config['Filenames']['srf_SPH_patch']).replace('SUBJECTNAME', subject)
 
    def map_file (self, mapname, taste='sweet', subject=None):
        """
        Composes a statistical map filename for given map and taste
        """
        if subject is None: 
            subject = self.subject
        return (self.data_path + subject + '/' + self.config['Filenames']['map']).replace('SUBJECTNAME', subject).replace('TASTENAME', taste).replace('MAPNAME', mapname)

    def mask_lowres (self, mapname, taste='sweet', subject=None):

        if subject is None:
            subject = self.subject
        return (self.data_path + subject + '/' + self.config['Filenames']['mask_lowres']).replace('SUBJECTNAME',subject).replace('TASTENAME',taste[0:3])

    def mask_highres (self, mapname, taste='sweet', subject=None):

        if subject is None:
            subject = self.subject
        return (self.data_path + subject + '/' + self.config['Filenames']['mask_highres']).replace('SUBJECTNAME',subject).replace('TASTENAME',taste[0:3])


    def zmap_mask_highres (self, mapname, taste='sweet', subject=None):
        """
        Composes a statistical map filename for given map and taste
        """
        if subject is None:
            subject = self.subject
        return (self.data_path + subject + '/' + self.config['Filenames']['zmap_mask_highres']).replace('SUBJECTNAME', subject).replace('TASTENAME', taste).replace('MAPNAME', mapname)


